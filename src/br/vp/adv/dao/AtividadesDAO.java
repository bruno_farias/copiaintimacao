package br.vp.adv.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import br.vp.adv.vo.Atividade;

public class AtividadesDAO {
	
	private static final Logger log = Logger.getLogger(AtividadesDAO.class);

	public static ArrayList<Atividade> buscaPublicacoes(int uf, boolean primeiroGrau, boolean segundoGrau) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		ArrayList<Atividade> atividades = new ArrayList<Atividade>();
		
		try {
			String cmdSQL = "SELECT ap.codigo, ap.tipo, ap.pasta, prc.PrcNroPrc, tip.TipJusCodCls, ap.codigoPublicacao,"
					+ "	CONVERT(VARCHAR, pub.dataPublicacao, 103) as dataPublicacao, cad.PesNom as cliente, "
					+ " (SELECT COUNT(*) FROM AnxAti WHERE CodAti = ap.codigo AND PstAnx = ap.pasta ) as 'qtdAnexosAtividade', "
					+ " ap.dataCriacao, ap.status, ap1.codigo, ap1.tipo, ap1.status"
					+ "	FROM atividadePasta as ap"
					+ "	LEFT JOIN PrcCad as prc on prc.PrcNroPst = ap.pasta"
					+ "	LEFT JOIN CadPes1 as cad on cad.PesCod = prc.PrcCli"
					+ "	LEFT JOIN PrcClsAst as cls on cls.PrcNroPst = prc.PrcNroPst"
					+ "	LEFT JOIN TipJus as tip on tip.TipJusCodAst = cls.PrcCodTabAst"
					+ "	LEFT JOIN publicacao as pub on pub.codigo = ap.codigoPublicacao"
					+ "	LEFT JOIN logCopiaIntimacao as lg on lg.codigoAtividade = ap.codigo AND lg.status IN (1,3,4,5)"
					+ " LEFT JOIN atividadePasta as ap1 on ap1.atividadePai = ap.codigo AND ap1.tipo = 787"
					+ "	WHERE ap.tipo = 220 /*AND ap.status = 0*/ AND prc.PrcTipPrc = 'V' AND ap.pasta != 0"
					+ "	AND ap.dataCriacao >= '2021-09-01' AND lg.data IS NULL AND prc.prcUf = ?"
					+ "	AND prc.PrcCli NOT IN (40493, 1025181, 40576, 1025180) AND tip.TipJusCodCls IS NOT NULL"
					+ "	AND ap.codigoPublicacao IS NOT NULL AND pub.dataPublicacao IS NOT NULL"
					+ " AND cad.PesGpeEco NOT IN (41,4)"
					+ " AND DATEDIFF(DAY, ap.dataCriacao, CURRENT_TIMESTAMP) <= 7" //atividades criadas até 7 dias atrás 
					+ " AND pub.conteudoPublicacao NOT LIKE '%INTIMAÇÃO ELETRÔNICA REPROCESSADA EM AUDITORIA%'";
			
					if(primeiroGrau && segundoGrau) {
						cmdSQL += "	AND TipJusCodCls IN (18,19,20,21,22,24,26,28,30,32,23,25,27,29,31,33,34)";
					} else if(primeiroGrau) {
						cmdSQL += "	AND TipJusCodCls IN (18,19,20,21,22,24,26,28,30,32)";
					} else if(segundoGrau) {
						cmdSQL += "	AND TipJusCodCls IN (23,25,27,29,31,33,34)";
					}
					
					cmdSQL += "	AND (SELECT COUNT(*) FROM AnxAti WHERE CodAti = ap.codigo AND PstAnx = ap.pasta) = 0" /*nenhum arquivo na atividade*/
							+ " AND (ap.status IN (0,1) OR (ap.status = 3 AND ap1.codigo IS NOT NULL AND ap1.status IN (0,1)))" /*Publicacao em andamento/reativada OU Publicacao finalizada e filha agendar prazo em andamento/reativada*/
							+ " AND ((pub.origem IS NULL) OR (pub.origem NOT IN ('TJMG', 'TJSC', 'TJPR', 'TJSC', 'PJEAPI', 'JPEMG')))"
							+ " ORDER BY CASE WHEN ap.dataFatalProcessual IS NULL THEN 1 ELSE 0 END DESC, ap.dataPrevistaEncerramento, ap.codigo";
			
			log.debug(cmdSQL);
			conn = DataSource.getConnection();
			
			ps = conn.prepareStatement(cmdSQL);
			
			ps.setInt(1, uf);
			
			ps.executeQuery();
			
			rs = ps.getResultSet();
			
			while(rs.next()) {
				Atividade atividade = new Atividade();
				atividade.setCodigo(rs.getInt("codigo"));
				atividade.setTipo(rs.getInt("tipo"));
				atividade.setPasta(rs.getInt("pasta"));
				String prcNroPrc = rs.getString("PrcNroPrc");
				if(prcNroPrc != null) {
					prcNroPrc = prcNroPrc.trim();
				}
				atividade.setNumeroProcesso(prcNroPrc);
				atividade.setClientePasta(rs.getString("cliente"));
				atividade.setDataPublicacao(rs.getString("dataPublicacao"));
				
				int tipJusCodCls = rs.getInt("TipJusCodCls");
				String instancia = null;

				if(tipJusCodCls != -1) {
					if(tipJusCodCls == 18 || tipJusCodCls == 19 || tipJusCodCls == 20 || tipJusCodCls == 21 || tipJusCodCls == 22 || tipJusCodCls == 24 || tipJusCodCls == 26 ||
							tipJusCodCls == 28 || tipJusCodCls == 30 || tipJusCodCls == 32 ) {
						instancia = "1º Grau";
					} else if(tipJusCodCls == 23 || tipJusCodCls == 25 || tipJusCodCls == 27 || tipJusCodCls == 29 || tipJusCodCls == 31 || tipJusCodCls == 33 || tipJusCodCls == 34) {
						instancia = "2º Grau";
					}
				}
				
				atividade.setGrauProcesso(instancia);
				
				atividades.add(atividade);;
			}
			
			return atividades;
		} catch (Exception e) {
			throw e;
		} finally{
			if(ps !=null){
				ps.close();
			}
			if(rs !=null){
				rs.close();
			}
			if(conn != null){
				conn.close();
			}
		}
	}

	public static ArrayList<Integer> verificarMultiplasAtividades(int codigo, int pasta) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		ArrayList<Integer> atividades = new ArrayList<Integer>();
		
		try {
			String cmdSQL = "SELECT codigo"
					+ " FROM atividadePasta"
					+ " WHERE pasta = ? AND tipo = 220 AND status = 0 AND codigo != ?";
			
					
			conn = DataSource.getConnection();
			
			ps = conn.prepareStatement(cmdSQL);
			
			ps.setInt(1, pasta);
			ps.setInt(2, codigo);
			
			ps.executeQuery();
			
			rs = ps.getResultSet();
			
			while(rs.next()) {
				atividades.add(rs.getInt("codigo"));
			}
			
			return atividades;
		} catch (Exception e) {
			throw e;
		} finally{
			if(ps !=null){
				ps.close();
			}
			if(rs !=null){
				rs.close();
			}
			if(conn != null){
				conn.close();
			}
		}
	}

	public static ArrayList<Atividade> buscaPublicacoesErroTribunal(String tribunal) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		ArrayList<Atividade> atividades = new ArrayList<Atividade>();
		
		try {
			String cmdSQL = "SELECT DISTINCT lg.codigoAtividade,lg.pasta,lg.numeroProcesso,lg.mensagem,lg.instancia,lg.status,lg.urlDocumento,lg.dataPublicacao,"
					+ " lg.referencia,lg.tribunal,prc.PrcNroPrc,tip.TipJusCodCls,CONVERT(VARCHAR, pub.dataPublicacao, 103) as dataPublicacaoPub,cad.PesNom as cliente"
					+ " FROM logCopiaIntimacao as lg"
					+ " LEFT JOIN atividadePasta as ap on ap.codigo = lg.codigoAtividade"
					+ " LEFT JOIN PrcCad as prc on prc.PrcNroPst = lg.pasta"
					+ " LEFT JOIN CadPes1 as cad on cad.PesCod = prc.PrcCli"
					+ " LEFT JOIN PrcClsAst as cls on cls.PrcNroPst = prc.PrcNroPst"
					+ " LEFT JOIN TipJus as tip on tip.TipJusCodAst = cls.PrcCodTabAst"
					+ " LEFT JOIN publicacao as pub on pub.codigo = ap.codigoPublicacao"
					+ " LEFT JOIN logCopiaIntimacao as lg1 on lg1.codigoAtividade = lg.codigoAtividade AND lg1.status IN (1,3,4,5) "
					+ " WHERE lg.status = 2 AND lg.tribunal = '"+tribunal+"' AND lg.referencia IS NULL AND lg1.data IS NULL AND DATEDIFF(day, pub.dataPublicacao, CURRENT_TIMESTAMP) <= 14 AND ap.status = 0 AND DATEDIFF(hour, ap.dataCriacao, CURRENT_TIMESTAMP) >= 2";
			
					
			conn = DataSource.getConnection();
			
			ps = conn.prepareStatement(cmdSQL);
			
			
			ps.executeQuery();
			
			rs = ps.getResultSet();
			
			while(rs.next()) {
				Atividade atividade = new Atividade();
				atividade.setCodigo(rs.getInt("codigoAtividade"));
				atividade.setTipo(220);
				atividade.setPasta(rs.getInt("pasta"));
				String prcNroPrc = rs.getString("PrcNroPrc");
				if(prcNroPrc != null) {
					prcNroPrc = prcNroPrc.trim();
				}
				atividade.setNumeroProcesso(prcNroPrc);
				atividade.setClientePasta(rs.getString("cliente"));
				atividade.setDataPublicacao(rs.getString("dataPublicacaoPub"));
				
				int tipJusCodCls = rs.getInt("TipJusCodCls");
				String instancia = null;

				if(tipJusCodCls != -1) {
					if(tipJusCodCls == 18 || tipJusCodCls == 19 || tipJusCodCls == 20 || tipJusCodCls == 21 || tipJusCodCls == 22 || tipJusCodCls == 24 || tipJusCodCls == 26 ||
							tipJusCodCls == 28 || tipJusCodCls == 30 || tipJusCodCls == 32 ) {
						instancia = "1º Grau";
					} else if(tipJusCodCls == 23 || tipJusCodCls == 25 || tipJusCodCls == 27 || tipJusCodCls == 29 || tipJusCodCls == 31 || tipJusCodCls == 33 || tipJusCodCls == 34) {
						instancia = "2º Grau";
					}
				}
				
				atividade.setGrauProcesso(instancia);
				
				atividades.add(atividade);;
			}
			
			return atividades;
		} catch (Exception e) {
			throw e;
		} finally{
			if(ps !=null){
				ps.close();
			}
			if(rs !=null){
				rs.close();
			}
			if(conn != null){
				conn.close();
			}
		}
	}
}
