package br.vp.adv.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import br.vp.adv.constants.CopiaIntimacaoConstants;
import br.vp.adv.vo.AnexoLS;
import br.vp.adv.vo.Atividade;
import br.vp.adv.vo.LogCopiaIntimacao;

public class CopiaIntimacaoDAO {
	
	private static final Logger log = Logger.getLogger(CopiaIntimacaoDAO.class);

	public static void insereLogCopiaIntimacao(LogCopiaIntimacao logCI) throws Exception {
		Connection cnn = null;
		PreparedStatement ps = null;
		String cmdSQL;	
		
		try{
			cmdSQL = "INSERT INTO logCopiaIntimacao (codigoAtividade,pasta,data,numeroProcesso,mensagem,instancia,status,urlDocumento,dataPublicacao,referencia,tribunal,anexosLS) VALUES (?,?,CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?,?)";
			
			cnn = DataSource.getConnection();
			ps = cnn.prepareStatement(cmdSQL);
			
			ps.setInt(1, logCI.getCodigoAtividade());
			ps.setInt(2, logCI.getPasta());
			ps.setString(3, logCI.getNumeroProcesso());
			ps.setString(4, logCI.getMensagem());
			ps.setString(5, logCI.getInstancia());
			ps.setInt(6, logCI.getStatus());
			ps.setString(7, logCI.getUrl());
			ps.setString(8, logCI.getDataPublicacaoAtividade());
			ps.setString(9, logCI.getReferencia());
			ps.setString(10, logCI.getTribunal());
			ps.setString(11, logCI.getAnexosLS());
			
			ps.executeUpdate();
			
		}catch(Exception e){
			throw e;
		}
		finally{
			if(ps != null){
				ps.close();
			}
			if(cnn != null){
				cnn.close();
			}
		}
		
	}

	public static int verificaTentativasFrustadas(String numeroProcesso, String instancia, int codigoAtividade) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dataAtual = sdf.format(new Date());
		
		try {
			String cmdSQL = "SELECT COUNT(*) AS contagem"
					+ "  FROM logCopiaIntimacao"
					+ "  WHERE numeroProcesso = ?"
					+ "  AND instancia = ?"
					+ "  AND data >= ?"
					+ "  AND status = ?"
					+ "  AND codigoAtividade = ?";
			
					
			conn = DataSource.getConnection();
			
			ps = conn.prepareStatement(cmdSQL);
			
			ps.setString(1, numeroProcesso);
			ps.setString(2, instancia);
			ps.setString(3, dataAtual);
			ps.setInt(4, CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS);
			ps.setInt(5, codigoAtividade);
			
			ps.executeQuery();
			
			rs = ps.getResultSet();
			
			if(rs.next()) {
				return rs.getInt("contagem");
			}
			
			return 0;
		} catch (Exception e) {
			throw e;
		} finally{
			if(ps !=null){
				ps.close();
			}
			if(rs !=null){
				rs.close();
			}
			if(conn != null){
				conn.close();
			}
		}
	}

	public static boolean tentativaFrustradaTempo(String numeroProcesso, String instancia, int codigoAtividade) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		LocalDateTime agora = LocalDateTime.now();
		LocalDateTime horasAtras = agora.minusHours(CopiaIntimacaoConstants.INTERVALO_ENTRE_TENTATIVAS);
		Date dataHrsAtras = Date.from(horasAtras.atZone(ZoneId.systemDefault()).toInstant());
		String dataQuery = sdf.format(dataHrsAtras);
		
		try {
			String cmdSQL = "SELECT COUNT(*) AS contagem"
					+ "  FROM logCopiaIntimacao"
					+ "  WHERE numeroProcesso = ?"
					+ "  AND instancia = ?"
					+ "  AND data >= ?"
					+ "  AND status = ?"
					+ "  AND codigoAtividade = ?";
			
					
			conn = DataSource.getConnection();
			
			ps = conn.prepareStatement(cmdSQL);
			
			ps.setString(1, numeroProcesso);
			ps.setString(2, instancia);
			ps.setString(3, dataQuery);
			ps.setInt(4, CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS);
			ps.setInt(5, codigoAtividade);
			
			ps.executeQuery();
			
			rs = ps.getResultSet();
			
			if(rs.next()) {
				if(rs.getInt("contagem") > 0) {
					return true;
				}
				return false;
			}
			
			return false;
		} catch (Exception e) {
			throw e;
		} finally{
			if(ps !=null){
				ps.close();
			}
			if(rs !=null){
				rs.close();
			}
			if(conn != null){
				conn.close();
			}
		}
	}

	public static int obterIdAnexoPasta(int pasta) throws Exception {
		Connection cnn = null;
		Statement sta = null;
		String cmdSQL;	
		ResultSet rs = null;
		try{
			cnn = DataSource.getConnection();
			sta = cnn.createStatement();
			cmdSQL = "SELECT MAX(IdAnx) AS IdAnx FROM PrcAnx WHERE PstAnx = " + pasta;
			rs = sta.executeQuery(cmdSQL);
			if(rs.next()){
				int idAnx = rs.getInt("IdAnx");
				idAnx = idAnx + 1;
				return idAnx;
			}else{
				return -1;
			}
			
		}catch(Exception e){
			throw e;
		}
		finally{
			if(sta !=null){
				sta.close();
			}
			if(rs !=null){
				rs.close();
			}
			if(cnn != null){
				cnn.close();
			}
		}
	}

	public static int incluirAnexoPasta(AnexoLS anexoLS) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String cmdSQL = "INSERT INTO PrcAnx (PstAnx, IdAnx, TipAnx, NomAnx, PatAnx, DtaIncAnx, UsuIncAnx, IdFornecedor) VALUES(?, ?, ?, ?, ?, CURRENT_TIMESTAMP, ?, ?)";    
			
			conn = DataSource.getConnection();
			ps = conn.prepareStatement(cmdSQL);
			
			ps.setInt(1, anexoLS.getPasta());
			ps.setInt(2, anexoLS.getIdAnx());
			ps.setInt(3, anexoLS.getTipAnx());
			ps.setString(4, anexoLS.getNomeAnexo());
			ps.setString(5, anexoLS.getUrl());
			ps.setInt(6, anexoLS.getCodUsuIncluir());
			ps.setString(7, null);
			
			ps.executeUpdate();
			
			return anexoLS.getIdAnx();
			
		} catch (Exception e) {
			throw e;
		} finally{
			if(ps !=null){
				ps.close();
			}
			if(rs !=null){
				rs.close();
			}
			if(conn != null){
				conn.close();
			}
		}
	}

	public static boolean incluirAnexoAtividade(AnexoLS anexoLS) throws Exception {
		Connection conexao = null;         
        PreparedStatement ps = null;  
        String sql = "INSERT INTO AnxAti (IdAnx, PstAnx, CodAti) VALUES(?, ?, ?)";    
        
        try {  
        	conexao = DataSource.getConnection();
        	
      	  	ps = conexao.prepareStatement(sql);  
           
      	  	ps.setInt(1, anexoLS.getIdAnx()); 
      	  	ps.setInt(2, anexoLS.getPasta());
      	  	ps.setInt(3, anexoLS.getCodigoAtividade());   
      	  	
      	  	ps.executeUpdate();
            
      	  	return true;
        } catch (Exception e) {  
      	  throw e;
        } finally{
        	if(conexao !=null){
        		conexao.close();
        	}
        	if(ps !=null){
        		ps.close();
        	}
        }
	}

	public static ArrayList<String> buscaSequenciasProibidas(String atividades) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		ArrayList<String> seqs = new ArrayList<String>();
		
		try {
			String cmdSQL = "SELECT referencia FROM logCopiaIntimacao WHERE codigoAtividade IN ("+atividades+")";
					
			conn = DataSource.getConnection();
			
			ps = conn.prepareStatement(cmdSQL);
			
			ps.executeQuery();
			
			rs = ps.getResultSet();
			
			while(rs.next()) {
				String ref = rs.getString("referencia");
				if(ref != null) {
					ref = ref.replace("Sequência: ", "");
				}
				seqs.add(ref);
			}
			
			return seqs;
		} catch (Exception e) {
			throw e;
		} finally{
			if(ps !=null){
				ps.close();
			}
			if(rs !=null){
				rs.close();
			}
			if(conn != null){
				conn.close();
			}
		}
	}

	public static int verificaPublicacaoMesmaData(Atividade atividade) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String cmdSQL = "SELECT * FROM logCopiaIntimacao WHERE pasta = ? AND dataPublicacao = ? AND status IN (1,5)";
					
			conn = DataSource.getConnection();
			
			ps = conn.prepareStatement(cmdSQL);
			
			ps.setInt(1, atividade.getPasta());
			ps.setString(2, atividade.getDataPublicacao());
			
			ps.executeQuery();
			
			rs = ps.getResultSet();
			
			if(rs.next()) {
				return rs.getInt("codigoAtividade");
			}
			
			return -1;
		} catch (Exception e) {
			throw e;
		} finally{
			if(ps !=null){
				ps.close();
			}
			if(rs !=null){
				rs.close();
			}
			if(conn != null){
				conn.close();
			}
		}
	}

	public static void deleteLogsErroPeriodo(String timestampInicio, String timestampFinal) throws Exception {
		Connection cnn = null;
		PreparedStatement ps = null;
		String cmdSQL;	
		
		try{
			cmdSQL = "DELETE FROM logCopiaIntimacao WHERE data BETWEEN '"+timestampInicio+"' AND '"+timestampFinal+"' AND status = 2";
			log.debug(cmdSQL);
			
			cnn = DataSource.getConnection();
			ps = cnn.prepareStatement(cmdSQL);
			
			int linhasExcluidas = ps.executeUpdate();
			
			log.debug("Excluiu [" + linhasExcluidas + "] linhas!");
			
		}catch(Exception e){
			throw e;
		}
		finally{
			if(ps != null){
				ps.close();
			}
			if(cnn != null){
				cnn.close();
			}
		}
	}

	public static int verificaTentativasFrustadasTribunal(String numeroProcesso, String instancia, int codigoAtividade, String tribunal) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dataAtual = sdf.format(new Date());
		
		try {
			String cmdSQL = "SELECT COUNT(*) AS contagem"
					+ "  FROM logCopiaIntimacao"
					+ "  WHERE numeroProcesso = ?"
					+ "  AND instancia = ?"
					+ "  AND data >= ?"
					+ "  AND status = ?"
					+ "  AND codigoAtividade = ?"
					+ "  AND tribunal = ?";
			
					
			conn = DataSource.getConnection();
			
			ps = conn.prepareStatement(cmdSQL);
			
			ps.setString(1, numeroProcesso);
			ps.setString(2, instancia);
			ps.setString(3, dataAtual);
			ps.setInt(4, CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS);
			ps.setInt(5, codigoAtividade);
			ps.setString(6, tribunal);
			
			ps.executeQuery();
			
			rs = ps.getResultSet();
			
			if(rs.next()) {
				return rs.getInt("contagem");
			}
			
			return 0;
		} catch (Exception e) {
			throw e;
		} finally{
			if(ps !=null){
				ps.close();
			}
			if(rs !=null){
				rs.close();
			}
			if(conn != null){
				conn.close();
			}
		}
	}

	public static boolean tentativaFrustradaTempoTribunal(String numeroProcesso, String instancia, int codigoAtividade, String tribunal) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		LocalDateTime agora = LocalDateTime.now();
		LocalDateTime horasAtras = agora.minusHours(CopiaIntimacaoConstants.INTERVALO_ENTRE_TENTATIVAS);
		Date dataHrsAtras = Date.from(horasAtras.atZone(ZoneId.systemDefault()).toInstant());
		String dataQuery = sdf.format(dataHrsAtras);
		
		try {
			String cmdSQL = "SELECT COUNT(*) AS contagem"
					+ "  FROM logCopiaIntimacao"
					+ "  WHERE numeroProcesso = ?"
					+ "  AND instancia = ?"
					+ "  AND data >= ?"
					+ "  AND status = ?"
					+ "  AND codigoAtividade = ?"
					+ "  AND tribunal = ?";
			
					
			conn = DataSource.getConnection();
			
			ps = conn.prepareStatement(cmdSQL);
			
			ps.setString(1, numeroProcesso);
			ps.setString(2, instancia);
			ps.setString(3, dataQuery);
			ps.setInt(4, CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS);
			ps.setInt(5, codigoAtividade);
			ps.setString(6, tribunal);
			
			ps.executeQuery();
			
			rs = ps.getResultSet();
			
			if(rs.next()) {
				if(rs.getInt("contagem") > 0) {
					return true;
				}
				return false;
			}
			
			return false;
		} catch (Exception e) {
			throw e;
		} finally{
			if(ps !=null){
				ps.close();
			}
			if(rs !=null){
				rs.close();
			}
			if(conn != null){
				conn.close();
			}
		}
	}

	public static int obterQuantidadeFalhasTribunal(Atividade at, String tribunal) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		
		try {
			String cmdSQL = "SELECT COUNT(*) AS quantidadeFalhas"
					+ " FROM logCopiaIntimacao"
					+ " WHERE codigoAtividade = ? AND status = 2 AND referencia IS NULL AND tribunal = ?";
			
					
			conn = DataSource.getConnection();
			
			ps = conn.prepareStatement(cmdSQL);
			
			ps.setInt(1, at.getCodigo());
			ps.setString(2, tribunal);
			
			ps.executeQuery();
			
			rs = ps.getResultSet();
			
			if(rs.next()) {
				return rs.getInt("quantidadeFalhas");
			}
			
			return -1;
			
		} catch (Exception e) {
			throw e;
		} finally{
			if(ps !=null){
				ps.close();
			}
			if(rs !=null){
				rs.close();
			}
			if(conn != null){
				conn.close();
			}
		}
	}

	public static boolean temArquivoAnexadoAtividade(int codigo, int pasta) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String cmdSQL = "SELECT * FROM AnxAti WHERE CodAti = ? AND PstAnx = ?";
					
			conn = DataSource.getConnection();
			
			ps = conn.prepareStatement(cmdSQL);
			
			ps.setInt(1, codigo);
			ps.setInt(2, pasta);
			
			ps.executeQuery();
			
			rs = ps.getResultSet();
			
			if(rs.next()) {
				return true;
			}
			
			return false;
			
		} catch (Exception e) {
			throw e;
		} finally{
			if(ps !=null){
				ps.close();
			}
			if(rs !=null){
				rs.close();
			}
			if(conn != null){
				conn.close();
			}
		}
	}

	public static ArrayList<Atividade> temAtividadePublicacaoMesmaData(Atividade at) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		ArrayList<Atividade> atividades = new ArrayList<Atividade>();
		
		try {
			String cmdSQL = "SELECT ap.codigo"
					+ " ,ap.pasta"
					+ " ,ap.codigoPublicacao"
					+ " ,pub.lote"
					+ "	,CONVERT(VARCHAR, pub.dataPublicacao, 103) as dataPub"
					+ " FROM atividadePasta as ap"
					+ " LEFT JOIN publicacao as pub on pub.codigo = ap.codigoPublicacao"
					+ " WHERE ap.pasta = ? AND ap.tipo = 220 AND ap.status = 0 AND CONVERT(VARCHAR, pub.dataPublicacao, 103) = ?";
					
			conn = DataSource.getConnection();
			
			ps = conn.prepareStatement(cmdSQL);
			
			ps.setInt(1, at.getPasta());
			ps.setString(2, at.getDataPublicacao());
			
			ps.executeQuery();
			
			rs = ps.getResultSet();
			
			while(rs.next()) {
				Atividade atividade = new Atividade();
				atividade.setCodigo(rs.getInt("codigo"));
				atividade.setTipo(220);
				atividade.setPasta(rs.getInt("pasta"));
				atividade.setDataPublicacao(rs.getString("dataPub"));
				
				atividades.add(atividade);
			}
			
			return atividades;
			
		} catch (Exception e) {
			throw e;
		} finally{
			if(ps !=null){
				ps.close();
			}
			if(rs !=null){
				rs.close();
			}
			if(conn != null){
				conn.close();
			}
		}
	}
}
