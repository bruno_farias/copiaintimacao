package br.vp.adv.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DataSource {
	private static HikariConfig config = new HikariConfig();
    private static HikariDataSource ds;
    
    private static Logger log = Logger.getLogger(DataSource.class);

    static {
    	try {
    		config.setJdbcUrl( "jdbc:jtds:sqlserver://191.252.2.89/VP-LEGALSOFT" );
//          config.setJdbcUrl( "jdbc:jtds:sqlserver://192.168.152.3/VP-LEGALSOFT" );
    		config.setUsername( "rbsvplegalsoft" );
    		config.setPassword( "sRV!WvP63o#RoB0sVp" );
    		config.addDataSourceProperty( "cachePrepStmts" , "true" );
    		config.addDataSourceProperty( "prepStmtCacheSize" , "250" );
    		config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
    		config.setConnectionTestQuery("select 1");
    		config.setIdleTimeout(900000); //conexão pode ficar até 15 minutos em idle
    		config.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");
    		config.setConnectionTimeout(60000);
	    	config.setMaximumPoolSize(500);
	    	config.setMinimumIdle(1);
    		ds = new HikariDataSource( config );
    	} catch (Exception e) {
			log.error("Erro ao instanciar DataSource!", e);
		}
    }

    private DataSource() {}

    public static Connection getConnection() throws SQLException {
    	try {
    		return ds.getConnection();
    	} catch (Exception e) {
			log.error("Erro no getConnection!", e);
			return null;
		}
    }
}
