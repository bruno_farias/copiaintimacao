package br.vp.adv.main;

import org.apache.log4j.Logger;

import br.vp.adv.service.PJEMG;
import br.vp.adv.utils.Utils;

public class Main {
	public static final Logger log = Logger.getLogger(Main.class);
	
	public static void main(String[] args) {
		//CopiaIntimacaoBO.baixarCopiasPROJUDI();
		PJEMG.baixarCopiasPJE();
		Utils.limpezaArquivos();
	}
}