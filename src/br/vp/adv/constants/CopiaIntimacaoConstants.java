package br.vp.adv.constants;

public class CopiaIntimacaoConstants {
	public static String AMBIENTE_BD = "PROD";
	
	/**PROJUDI */
	public static String URL_PROJUDI = "https://projudi.tjpr.jus.br";
	
	public static final String LOGIN_PROJUDI_LUCAS = "07557791932";
	public static final String SENHA_PROJUDI_LUCAS = "Solus@123";
	public static final String KEY_2FA_PROJUDI_LUCAS = "NJCDORCIIM2U23KFMVSDCN3WNF5G6Q3T";
	
	public static final String LOGIN_PROJUDI_JAIME = "83722076820";
	public static final String SENHA_PROJUDI_JAIME = "prvp2023";
	public static final String KEY_2FA_PROJUDI_JAIME = "IVVXQVZRMZLXC6SLMJ2U6T3OGY4UMQ3F";
	
	/**PJE */
	public static final String URL_PJE = "https://pje.tjmg.jus.br";
	public static final String URL_PJE_BUSCA = "https://pje.tjmg.jus.br/pje/Processo/ConsultaProcesso/listView.seam";
	
	public static final String LOGIN_PJE_JAIME = "83722076820";
	public static final String SENHA_PJE_JAIME = "Vanzin@2024";
	
	/**PJE RECURSAL MG*/
	public static final String URL_PJE_RECURSAL_MG = "https://pjerecursal.tjmg.jus.br";
	public static final String URL_PJE_RECURSAL_BUSCA = "https://pjerecursal.tjmg.jus.br/pje/Processo/ConsultaProcesso/listView.seam";
	
	public static final String LOGIN_PJE_RECURSAL_JAIME = "83722076820";
	public static final String SENHA_PJE_RECURSAL_JAIME = "Vanzin@2023*";
	
	/**JPEMG*/
	public static final String URL_JPE_MG = "https://pe.tjmg.jus.br/rupe/portaljus/intranet/principal.rupe";
	public static final String LOGIN_JPE_JAIME = "04332621921";
	public static final String SENHA_JPE_JAIME = "Vanzin#2019";
	
	/**EPROCSC*/
	public static final String URL_EPROCSC1G = "https://eproc1g.tjsc.jus.br/eproc/";
	public static final String LOGIN_EPROCSC1G_JAIME = "PR020835";
	public static final String SENHA_EPROCSC1G_JAIME = "Vanzin@2024@";
	public static final String USUARIO_SIGLA = "SC017282";
	
	public static final String PATH_DOCUMENTOS = "C:\\Users\\robo.5\\Documents\\Arquivos Projetos\\copiaIntimacao\\Documentos";
	public static final String PATH_DOCUMENTOS_TEMP = "C:\\Users\\robo.5\\Documents\\Arquivos Projetos\\copiaIntimacao\\Temp";
	//public static final String PATH_DOCUMENTOS = "C:\\Users\\bruno.farias\\Documents\\Arquivos Projetos\\copiaIntimacao\\Documentos";
	//public static final String PATH_DOCUMENTOS_TEMP = "C:\\Users\\bruno.farias\\Documents\\Arquivos Projetos\\copiaIntimacao\\Temp";
	
	//contantes para status na tabela logCopiaIntimacao
	public static final int SUCESSO_BUSCA_DOCUMENTOS = 1;
	public static final int ERRO_BUSCA_DOCUMENTOS = 2;
	public static final int DOCUMENTOS_ATIVIDADE_MESMO_DIA = 3;
	public static final int NAO_GEROU_DOCUMENTO = 4;
	public static final int MULTIPLOS_ANEXOS = 5;
	
	//quantidade máxima de tentativas que um processo pode ter por dia
	public static final int TENTATIVAS_POR_DIA = 10;
	//intervalo entre as tentativas (em horas)
	public static final int INTERVALO_ENTRE_TENTATIVAS = 1;
	
	public static final int LIMITE_POR_EXECUCAO = 10;
	
	public static final int QTD_FALHAS_MG = 3;
	
	//quantidade de dias que um documento/print fica armazenado
	public static final int DIAS_DOCUMENTO_ARMAZENADO = 14;
	public static final int DIAS_PRINT_ARMAZENADO = 7;
	
	//tipo de anexo INTIMAÇÃO - DOWNLOAD AUTOMÁTICO
	public static final int ANEXO_INT_DOWN_AUT_HOM = 380; //HOMOLOGAÇÃO
	public static final int ANEXO_INT_DOWN_AUT_PROD = 382; //PRODUÇÃO
	
	//wasabi
	public static final String BUCKET_NAME = "lsv3";
	public static final String SERVICE_ENDPOINT = "s3.us-east-2.wasabisys.com";
	public static final String REGION = "us-east-2";
	public static final String ACCESS_KEY = "162NB0T2UDL3U3WOBTZF";
	public static final String SECRET_KEY = "ovTtvv25kT70h1lWWMWV4wA917e95PfcHJkyPeu2";

	public static final String TWOCAPTCHA_API_KEY = "711477b97738d11219285e7f0a642bb4";

}
