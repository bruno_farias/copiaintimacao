package br.vp.adv.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import br.vp.adv.scheduler.CopiaIntimacaoScheduler;

@WebListener
public class CopiaListener implements ServletContextListener {
	public static final Logger log = Logger.getLogger(CopiaListener.class);
	public void contextInitialized(ServletContextEvent sce) {
		log.debug("Context initialized!");
		CopiaIntimacaoScheduler.schedule();
	}
	public void contextDestroyed(ServletContextEvent sce) {
		log.debug("Context destroyed!");
		CopiaIntimacaoScheduler.parar();
	}
}
