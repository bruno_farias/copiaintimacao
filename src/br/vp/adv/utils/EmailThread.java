package br.vp.adv.utils;

import java.io.File;
import java.util.ArrayList;

public class EmailThread implements Runnable {
	private String titulo;
	private String mensagem;
	private String[] emails;
	private File relatorio;
	private ArrayList<File> anexos;
	
	public EmailThread(String titulo, String mensagem, String[] emails, File relatorio, ArrayList<File> anexos) {
		super();
		this.titulo = titulo;
		this.mensagem = mensagem;
		this.emails = emails;
		this.relatorio = relatorio;
		this.anexos = anexos;
	}

	@Override
	public void run() {
		Utils.enviarEmail(this.titulo, this.mensagem, this.emails, this.relatorio, this.anexos);
	}
}
