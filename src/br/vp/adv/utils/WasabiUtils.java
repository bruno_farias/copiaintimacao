package br.vp.adv.utils;

import java.io.File;

import org.apache.log4j.Logger;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;

import br.vp.adv.bo.CopiaIntimacaoBO;
import br.vp.adv.constants.CopiaIntimacaoConstants;
import br.vp.adv.dao.CopiaIntimacaoDAO;
import br.vp.adv.vo.AnexoLS;
import br.vp.adv.vo.LogCopiaIntimacao;

public class WasabiUtils {
	public static final Logger log = Logger.getLogger(WasabiUtils.class);

	//constantes úteis para subir arquivos para o wasabi
	private static final AmazonS3 AMAZON_S3_CLIENT = AmazonS3ClientBuilder.standard().withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(
				CopiaIntimacaoConstants.SERVICE_ENDPOINT, CopiaIntimacaoConstants.REGION)).withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(
						CopiaIntimacaoConstants.ACCESS_KEY, CopiaIntimacaoConstants.SECRET_KEY))).build();
	
	public static boolean anexarArquivoLS(int prcNroPst, int codigoAtividade, String nomeArquivo, File arquivo, LogCopiaIntimacao logCI, boolean anexaAtividade) {
		try {
			log.debug("Subindo arquivo ["+nomeArquivo+"] para WASABI...");
			
			String caminho = "";
			if(!CopiaIntimacaoConstants.AMBIENTE_BD.equals("PROD")) {
				caminho += "homologacao/";
			}
			
			caminho += "anexosv3/"+prcNroPst+"/"+codigoAtividade+"/";
			System.out.println(caminho);
			//System.exit(0);
			String pathCompleto = subirWasabi(arquivo, nomeArquivo, caminho);
			
			if(pathCompleto != null) {
				log.debug("URL = [" + pathCompleto + "]");

				if(logCI.getUrl() != null) {
					logCI.setUrl(logCI.getUrl()+"--"+pathCompleto);
				} else {
					logCI.setUrl(pathCompleto);
				}
				
				if(logCI.getUrl() != null && logCI.getUrl().length() > 200) {
					logCI.setUrl(logCI.getUrl().substring(0, 200));
				}
				
				
				int codAnx = -1;
				if(CopiaIntimacaoConstants.AMBIENTE_BD.equals("PROD")) {
					codAnx = CopiaIntimacaoConstants.ANEXO_INT_DOWN_AUT_PROD;
				} else {
					codAnx = CopiaIntimacaoConstants.ANEXO_INT_DOWN_AUT_HOM;
				}
				
				//anexa o comprovante ao LS e na atividade 'Publicação' com usuario ADMINISTRADOR
				if(!anexarArquivoLS(nomeArquivo, prcNroPst, codigoAtividade, codAnx, 999999, pathCompleto, anexaAtividade, logCI)){
					log.debug("Falha ao anexar arquivo na pasta [" + prcNroPst + "]!");
					return false;
				}
			} else {
				log.error("Erro ao subir arquivo para WASABI! pathCompleto = null");
				return false;
			}
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Erro ao subir anexo ["+nomeArquivo+"] da pasta [" + prcNroPst + "] para o WASABI", e);
			return false;
		}
	}

	private static boolean anexarArquivoLS(String nomeArquivo, int prcNroPst, int codigoAtividade, int codAnx, int codUsu, String pathCompleto, boolean anexaAtividade, LogCopiaIntimacao logCI) {
		log.debug("Inserindo [" + nomeArquivo + "] da pasta [" + prcNroPst + "] no LS...");
		
		try {
			AnexoLS anexoLS = new AnexoLS();
			
			//anexoLS.setCodigoAtividade(atvValProtocoloGerada);
			anexoLS.setNomeAnexo(nomeArquivo);
			anexoLS.setPasta(prcNroPst);
			anexoLS.setTipAnx(codAnx);
			anexoLS.setCodUsuIncluir(codUsu);
			anexoLS.setUrl(pathCompleto);
			anexoLS.setCodigoAtividade(codigoAtividade);

			//obtem o id do anexo mais recente da pasta e adiciona 1
			int idNovoAnexo = CopiaIntimacaoBO.obterIdAnexoPasta(anexoLS.getPasta());
			if(idNovoAnexo == 0 || idNovoAnexo == -1) {
				log.error("Erro ao obter o id do novo anexo da pasta [" + prcNroPst + "]!");
				return false;
			} else {
				log.debug("O novo anexo terá ID [" + idNovoAnexo + "]");
			}
			
			anexoLS.setIdAnx(idNovoAnexo);
			
			//inclui o anexo na pasta do LS
			int codigoAnexo = CopiaIntimacaoBO.incluirAnexoPasta(anexoLS);
			if(codigoAnexo == 0 || codigoAnexo == -1) {
				log.error("Erro ao incluir anexo " + anexoLS + "!");
				return false;
			} 
			
			log.debug("Anexo inserido com sucesso na pasta [" + anexoLS.getPasta() + "]!");
			
			if(logCI.getAnexosLS() == null) {
				logCI.setAnexosLS(String.valueOf(codigoAnexo));
			} else {
				logCI.setMultiplosAnexos(true);
				logCI.setAnexosLS(logCI.getAnexosLS().concat(",").concat(String.valueOf(codigoAnexo)));
			}
			
			log.debug("anexaAtividade: ["+anexaAtividade+"]");
			
			if(anexaAtividade) {
				log.debug("Anexo inserido com sucesso na pasta [" + anexoLS.getPasta() + "]! Inserindo na atividade [" + codigoAtividade + "]...");
				
				if(logCI.isMultiplosAnexos()) {
					log.debug("JÁ ANEXOU ARQUIVO NESSA ATIVIDADE! NÃO VAI ANEXAR OUTROS...");
				} else {
					//adiciona o anexo a atividade 'Publicação'
					if(!incluirAnexoAtividade(anexoLS)) {
						log.error("Erro ao incluir o arquivo na atividade " + codigoAtividade);
						return false;
					}
					
					log.debug("Sucesso ao anexar arquivo na atividade!");
				}
			}
			
			
			return true;
			
		} catch (Exception e) {
			log.error("Erro ao anexar arquivo ao LS [ " + nomeArquivo + "] - [" + prcNroPst + "]!", e);
			return false;
		}
	}

	private static boolean incluirAnexoAtividade(AnexoLS anexoLS) {
		log.debug("Incluindo o anexo de ID [" + anexoLS.getIdAnx() + "] na atividade [" + anexoLS.getCodigoAtividade() + "]");
		try {
			return CopiaIntimacaoDAO.incluirAnexoAtividade(anexoLS);
		} catch (Exception e) {
			log.error("Erro ao inserir anexo [" + anexoLS + "] na atividade [" + anexoLS.getCodigoAtividade() + "]!", e);
			return false;
		}
	}

	private static String subirWasabi(File arquivo, String nomePDF, String caminho) {
		if(arquivo.exists()) {
			return enviarArquivoPublico(arquivo, nomePDF, caminho);
		} else {
			log.error("O arquivo [" + nomePDF + "] não existe!");
			return null;
		}
	}

	private static String enviarArquivoPublico(File arquivo, String nomePDF, String caminho) {
		log.debug("Enviando o arquivo [" + arquivo.getName() + "] - caminho: [" + caminho + "] para o WASABI...");

		try {
			//String completePath = "anexosv3/" + caminho + filename;
			String completePath = caminho + nomePDF;

			/*
			if (DocumentosConstants.AMBIENTE_EXECUCAO.equals("HOM")) {
				completePath = "homologacao/" + completePath;
			}
			*/
			  
			log.debug("[enviarArquivoPublico] - completePath: ["+completePath + "]");
			PutObjectRequest request = new PutObjectRequest(CopiaIntimacaoConstants.BUCKET_NAME, completePath, arquivo);
	    	request.withCannedAcl(CannedAccessControlList.PublicRead);
	    	AMAZON_S3_CLIENT.putObject(request);
	    	return AMAZON_S3_CLIENT.getUrl(CopiaIntimacaoConstants.BUCKET_NAME, completePath).toString(); 
		} catch (Exception e) {
			log.error("Erro ao enviar arquivo para o WASABI!", e);
			return null;
		}
	}
}
