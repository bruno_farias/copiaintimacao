package br.vp.adv.utils;

import java.io.File;
import java.net.URL;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.jboss.aerogear.security.otp.Totp;

import br.vp.adv.constants.CopiaIntimacaoConstants;
import br.vp.adv.vo.Atividade;
import br.vp.adv.vo.PartesNroProcessoPJE;

public class Utils {
	public static final Logger log = Logger.getLogger(Utils.class);
	
	public static final String EMAIL_REMETENTE = "notificacoestivp5@gmail.com";
	public static final String SENHA_REMETENTE = "vhrhubbihlkskgsa";
	public static final String[] arrayEmailBrunoFarias = {"bruno.farias@vp.adv.br"};
	public static final String[] arrayEmailBrunoFariasBrunoNassar = {"bruno.farias@vp.adv.br", "bruno.nassar@vp.adv.br"};
	
	public static String geradorCod2FAPROJUDI(String key) {
		try {
			Totp totp = new Totp(key);
			
			return totp.now();
		} catch (Exception e) {
			log.error("Falha ao gerar código de autenticação em 2 fatores PROJUDI!", e);
			return null;
		}
	}
	
	public static File concatenarArquivos(ArrayList<File> arquivosDoProcesso, Atividade atividade) {
		try {
			if(arquivosDoProcesso.size() == 0) {
				return null;
			}
			
			if(arquivosDoProcesso.size() == 1) {
				return arquivosDoProcesso.get(0);
			}
			
			log.debug("Compilando [" + arquivosDoProcesso.size() + "] da atividade [" + atividade.getCodigo() + "]...");
			PDFMergerUtility ut = new PDFMergerUtility();
			for(File pdf: arquivosDoProcesso) {
				ut.addSource(pdf);
			}
			
			Random gerador = new Random();
			//String pathCompilado = CopiaIntimacaoConstants.PATH_DOCUMENTOS+"/"+atividade.getNumeroProcesso() + "/" + gerador.nextInt() + "-" + atividade.getCodigo() + "compilado.pdf";
			String pathCompilado = CopiaIntimacaoConstants.PATH_DOCUMENTOS+"/"+atividade.getCodigo() + "/" + gerador.nextInt() + "-" + atividade.getCodigo() + "compilado.pdf";
			ut.setDestinationFileName(pathCompilado);
			ut.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());
			
			
			File retorno = new File(pathCompilado);
			log.debug("Arquivos compilados com sucesso - [" + retorno.getName() + "]!");
			return retorno;
		} catch (Exception e) {
			log.error("Erro ao compilar pdfs da atividade [" + atividade.getCodigo() + "]", e);
			return null;
		}
	}
	
	public static String formataPadrao(String nome) {
		if(nome == null) {
			return null;
		}
		
		nome = nome.trim();
		nome = nome.toUpperCase();
		nome = Normalizer.normalize(nome, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
		nome = nome.replace(" S/A", "").replace(" S/ A", "").replace(" S A", "").replace(" S.A", "").replace(" S.A.", "");
		nome = nome.replace(".", "").replace(",", "").replace(":", "").replace(";", "");
		
		if(nome.endsWith(" SA")) {
			nome = nome.replace(" SA", "");
		}
		nome = nome.trim();
				
		return nome;
	}
	
	public static ArrayList<String> sinonimosCliente(String nomeCliente){
		if(nomeCliente == null) {
			return null;
		}
		
		//nome no projudi
		String nomeClienteFormatado = formataPadrao(nomeCliente);
				
		ArrayList<String> possibilidades = new ArrayList<String>();
		possibilidades.add(nomeClienteFormatado);
		
		
		//vanzin
		possibilidades.add(formataPadrao("VANZIN & PENTEADO ADVOGADOS"));
		
		if(nomeClienteFormatado.equals("PORTO SEGURO CIA DE SEGUROS GERAIS")) {
			possibilidades.add("PORTO SEGURO COMPANHIA DE SEGUROS GERAIS");
		}
		
		if(nomeClienteFormatado.equals("AUTO - MAPFRE SEGUROS GERAIS")) {
			possibilidades.add("MAPFRE SEGUROS GERAIS");
			possibilidades.add("MAPFRE VERA CRUZ SEGURADORA");
			possibilidades.add("MAFRE SEGUROS EM GERAIS");
		}
		
		if(nomeClienteFormatado.equals("MAPFRE SEGUROS GERAIS")) {
			possibilidades.add("MAPFRE VERA CRUZ SEGURADORA");
			possibilidades.add("MAPFRE BRASIL SEGUROS");
		}
		
		if(nomeClienteFormatado.equals("BANCO BRADESCO")) {
			possibilidades.add("BANCO LOSANGO - BANCO MULTIPLO");
			possibilidades.add("BANCO BRADESCO AGENCIA 5831");
			possibilidades.add("BANCO BRADESCARD");
			possibilidades.add("BANCO BRADESCO CARTOES");
			possibilidades.add("BANCO FINASA");
		}
		
		if(nomeClienteFormatado.equals("MAPFRE VIDA")) {
			possibilidades.add("MAPFRE SEGUROS GERAIS");
			possibilidades.add("VIDA SEGURADORA");
		}
		
		if(nomeClienteFormatado.equals("AZUL SEGUROS")) {
			possibilidades.add("AZUL COMPANHIA DE SEGUROS GERAIS");
		}
		
		if(nomeClienteFormatado.equals("BANCO BRADESCO")) {
			possibilidades.add("BRADESCO");
			possibilidades.add("BRADESCO ADMINISTRADORA DE CONSORCIOS LTDA");
			possibilidades.add("BRADESCO VIDA E PREVIDENCIA");
			possibilidades.add("BANCO LOSANGO");
			possibilidades.add("BANCO BRADESCO FINANCIAMENTOS");
			possibilidades.add("BRADESCO SEGUROS");
			possibilidades.add("FUNDO DE INVESTIMENTO EM DIREITOS CREDITORIOS NAO PADRONIZADOS NPL II");
			possibilidades.add("BANCO BRADESCO BBI");
			possibilidades.add("BANCO NEXT");
			possibilidades.add("BANCO BRADESCO S A");
			possibilidades.add("BP PROMOTORA DE VENDAS LTDA");
			possibilidades.add("BRADESCO LEASING");
		}
		
		if(nomeClienteFormatado.equals("LOSANGO PROMOCOES DE VENDAS LTDA")) {
			possibilidades.add("BANCO LOSANGO - BANCO MULTIPLO");
			possibilidades.add("BANCO LOSANGO");
		}
		
		if(nomeClienteFormatado.equals("BANCO LOSANGO - BANCO MULTIPLO")) {
			possibilidades.add("BANCO LOSANGO");
		}
		
		if(nomeClienteFormatado.equals("BRADESCO SEGUROS")) {
			possibilidades.add("BRADESCO VIDA E PREVIDENCIA");
		}
		
		if(nomeClienteFormatado.equals("BANCO BRADESCO - MIGRACAO EXTENSAO") || nomeClienteFormatado.equals("BANCO BRADESCO CARTOES")) {
			possibilidades.add("BANCO BRADESCO");
		}
		
		if(nomeClienteFormatado.equals("BRADESCO PROMOTORA")) {
			possibilidades.add("BP PROMOTORA DE VENDAS LTDA");
		}
		
		if(nomeClienteFormatado.equals("BANCO BRADESCO - MIGRACAO EXTENSAO")) {
			possibilidades.add("BANCO BRADESCO FINANCIAMENTOS");
		}
		
		if(nomeClienteFormatado.equals("BRASILVEICULOS COMPANHIA DE SEGUROS")) {
			possibilidades.add("MAPFRE SEGUROS GERAIS");
		}
		
		if(nomeClienteFormatado.equals("BRASILSEG COMPANHIA DE SEGUROS")) {
			possibilidades.add("ALIANCA DO BRASIL SEGUROS");
			possibilidades.add("BANCO DO BRASIL");
		}
		
		if(nomeClienteFormatado.equals("FUNDO DE INVESTIMENTOS EM DIREITOS CREDITORIOS NAO PADRONIZADOS NPL 2")) {
			possibilidades.add("FUNDO DE INVESTIMENTO EM DIREITOS CREDITORIOS NAO PADRONIZADOS NPL II");
		}
		
		if(nomeClienteFormatado.equals("HSBC BANK BRASIL - BANCO MULTIPLO")) {
			possibilidades.add("HSBC BANK BRASIL");
			possibilidades.add("KIRTON BANK");
			possibilidades.add("BANCO BRADESCO");
			possibilidades.add("BANCO HSBC BAMERINDUS");
		}
		
		if(nomeClienteFormatado.equals("ITAU SEGUROS DE AUTO E RESIDENCIA")) {
			possibilidades.add("ITAU SEGUROS AUTO E RESIDENCIA");
			possibilidades.add("ITAU UNIBANCO");
			possibilidades.add("ITAU SEGUROS");
		}
		
		if(nomeClienteFormatado.equals("ITAU SEGUROS")) {
			possibilidades.add("ITAU SEGUROS DE AUTO E RESIDENCIA");
		}
		
		if(nomeClienteFormatado.equals("BANCO BRADESCO FINANCIAMENTOS")) {
			possibilidades.add("BANCO FINASA");
			possibilidades.add("BRADESCO LEASING");
		}
		
		if(nomeClienteFormatado.equals("MAPFRE SEGUROS GERAIS")) {
			possibilidades.add("COOPERATIVA DE CREDITO POUPANCA E INVESTIMENTO VALE DO PIQUIRI ABCD SICREDI VALE DO PIQUIRI ABCD PR/SP");
			possibilidades.add("MAPFRE SEGUROS");
		}
		
		if(nomeClienteFormatado.equals("INSTITU MAPFRE SEGUROS GERAIS")) {
			possibilidades.add("MAPFRE SEGUROS GERAIS");
		}
		
		if(nomeClienteFormatado.equals("COOPERATIVA DE CREDITO POUPANCA E INVESTIMENTO VALE DO PIQUIRI ABCD  SICREDI VALE DO PIQUIRI ABCD PR/SP")) {
			possibilidades.add("COOPERATIVA DE CREDITO POUPANCA E INVESTIMENTO VALE DO PIQUIRI ABCD SICREDI VALE DO PIQUIRI ABCD PR/SP");
		}
		
		if(nomeClienteFormatado.equals("CORRETORA DE SEGUROS SICREDI LTDA")) {
			possibilidades.add("COOPERATIVA DE CREDITO POUPANCA E INVESTIMENTO VALE DO PIQUIRI ABCD SICREDI VALE DO PIQUIRI ABCD PR/SP");
			possibilidades.add("MAPFRE SEGUROS GERAIS");
		}
		
		if(nomeClienteFormatado.equals("BANCO BRADESCO FINANCIAMENTOS")) {
			possibilidades.add("VANZIN PENTEADO & ANGHINONI SOCIEDADE DE ADVOGADOS");
		}
		
		
		return possibilidades;
	}

	public static boolean nomeParteVerificado(String nomeParte, Atividade atividade) {
		try {
			String nomeParteFormatado = formataPadrao(nomeParte);
			ArrayList<String> possibilidades = sinonimosCliente(atividade.getClientePasta());
			for(String possibilidade: possibilidades) {
				log.debug("Comparando cliente da pasta (modificado) [" + possibilidade + "] com nome da parte no movimento [" + nomeParteFormatado + "]");
				if(possibilidade.equals(nomeParteFormatado)) {
					log.debug("Cliente compatível!");
					return true;
				}
			}
			return false;
		} catch (Exception e) {
			log.error("Verificar se o cliente da atividade [" + atividade.getClientePasta() + "] está é compatível com o da descrição do movimento [" + nomeParte + "]", e);
			return false;
		}
	}

	public static PartesNroProcessoPJE separaNumeroProcessoPJE(String numeroProcesso) {
		try {
			String numeroProcessoFormatado = numeroProcesso.replace("-", ".").replace(" ", "").trim();
			String[] partesNumero = numeroProcessoFormatado.split("\\.");
			
			if(partesNumero.length == 6) {
				
				if(partesNumero[0] == null || partesNumero[1] == null || partesNumero[2] == null || partesNumero[3] == null ||
						partesNumero[4] == null || partesNumero[5] == null ) {
					log.error("Alguma parte do número do processo é null!");
					return null;
				}
				
				PartesNroProcessoPJE npp = new PartesNroProcessoPJE();
				npp.setNumeroSequencial(partesNumero[0]);
				npp.setNumeroDigitoVerificador(partesNumero[1]);
				npp.setAno(partesNumero[2]);
				npp.setRamoJustica(partesNumero[3]);
				npp.setRespectivoTribunal(partesNumero[4]);
				npp.setNumeroOrgaoJustica(partesNumero[5]);
				
				log.debug(npp);
				
				return npp;
			} else {
				log.debug("O numero do processo não possui 6 partes!");
				return null;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Erro ao dividir numero do processo em partes: " + numeroProcesso, e);
			return null;
		}
	}
	
	public static void limparPastaTemp() {
		try {
			File temp = new File(CopiaIntimacaoConstants.PATH_DOCUMENTOS_TEMP);
			File[] conteudoTemp = temp.listFiles();
			for(File arquivo: conteudoTemp) {
				String nomeArquivo = arquivo.getName();
				if(arquivo.delete()) {
					log.debug("Sucesso ao excluir arquivo [" + nomeArquivo + "] da pasta TEMP!");
				} else {
					log.warn("FALHA ao excluir arquivo [" + nomeArquivo + "] da pasta TEMP!");
				}
			}
		} catch (Exception e) {
			log.error("Erro ao limpar a pasta TEMP!", e);
		}
	}
	
	public static void limpezaArquivos() {
		try {
			log.debug("Realizando limpeza de arquivos [" + CopiaIntimacaoConstants.PATH_DOCUMENTOS_TEMP + "]...");
			
			limparPastaTemp();
			
			log.debug("Realizando limpeza de arquivos [" + CopiaIntimacaoConstants.PATH_DOCUMENTOS + "]...");
			
			File raiz = new File(CopiaIntimacaoConstants.PATH_DOCUMENTOS);
			LocalDateTime agora = LocalDateTime.now();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			
			File[] conteudo = raiz.listFiles();
			for(File arquivo: conteudo) {
				if(arquivo.isDirectory()) {
					//log.debug("[" + arquivo.getName() + "] FOLDER");
					
					File docs = new File(arquivo.getPath());
					File[] documentos = docs.listFiles();
					
					for(File doc: documentos) {
						Date dataUltimaMod = new Date(doc.lastModified());
						LocalDateTime ldtUltimaMod = dataUltimaMod.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
						
						int diasDoc = 30;
						if(doc.getName().contains(".pdf")) {
							diasDoc = CopiaIntimacaoConstants.DIAS_DOCUMENTO_ARMAZENADO;
						} else if(doc.getName().contains(".png")) {
							diasDoc = CopiaIntimacaoConstants.DIAS_PRINT_ARMAZENADO;
						}
						
						if(ldtUltimaMod.until(agora, ChronoUnit.DAYS) >= diasDoc) {
							if(doc.delete()) {
								log.debug("Sucesso ao excluir [" + doc.getName() + "] - [" + sdf.format(dataUltimaMod) + "]");
							} else {
								log.warn("FALHA ao excluir [" + doc.getName() + "] - [" + sdf.format(dataUltimaMod) + "]");
							}
						}
					}
					
					if(docs.listFiles().length == 0) {
						log.debug("Excluindo folder [" + docs.getName() + "]...");
						if(docs.delete()) {
							log.debug("Sucesso ao excluir [" + docs.getName() + "]");
						} else {
							log.warn("FALHA ao excluir [" + docs.getName() + "]");
						}
					}
				} else {
					log.debug("[" + arquivo.getName() + "]");
				}
				
			}
			log.debug("Final da limpeza de arquivos!");
		} catch (Exception e) {
			log.error("Erro na limpeza de arquivos: ", e);
			e.printStackTrace();
		}
	}
	
	public static void enviarEmail(String titulo, String mensagem, String[] emails, File relatorio, ArrayList<File> anexos) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date data = new Date();
		
		int tentativa = 1;
		int maxTentativas = 50;
		
		while(tentativa <= maxTentativas) {
			log.debug("ENVIANDO EMAIL... Tentativa " + tentativa + " de " + maxTentativas + " - Assunto: ["+titulo+"]");
			try {
				String host = "smtp.gmail.com";
				Properties props = new Properties();
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host", host);
				props.put("mail.smtp.port", 587);

				Session session = Session.getInstance(props, new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(EMAIL_REMETENTE, SENHA_REMETENTE);
					}
				});

				Message email = new MimeMessage(session);
				email.setFrom(new InternetAddress(EMAIL_REMETENTE, "Notificações TI VP"));

				String destinatarios = String.join(",", emails);
				email.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatarios)); 
				
				String emailBruno = "bruno.farias@vp.adv.br";
//				String emailLucas = "lucas.farias@vp.adv.br";
				if (!Arrays.asList(emails).contains(emailBruno)) {
					email.addRecipient(Message.RecipientType.CC, new InternetAddress(emailBruno));
				}
//				if (!Arrays.asList(emails).contains(emailLucas)) {
//					email.addRecipient(Message.RecipientType.CC, new InternetAddress(emailLucas));
//				}
				
				email.setSubject(titulo + " - " + sdf.format(data));
				
				BodyPart messageBodyPart = new MimeBodyPart();
			
				URL urlLogo = new URL("https://s3.us-east-2.wasabisys.com/lsv3/anexosv3/logo.png");
				String tmpdir = System.getProperty("java.io.tmpdir");
				File tempFile = File.createTempFile(tmpdir, "logo.png");

				// copia input stream para arquivo temporário
				FileUtils.copyURLToFile(urlLogo, tempFile);

				StringBuffer msg = new StringBuffer();
				msg.append("<html style=\"width: 100% !important;\">");
				msg.append("<body style=\"width: 100% !important;\">");
				msg.append("<img class=\"center\" src=cid:abc12345>");
				msg.append("<hr>");
				msg.append("<h1>Caro usuário(a), </h1>");
				if (relatorio != null) {
					msg.append("<div><p>" + mensagem + "<br><br>Verifique o relatório em anexo!</p></div>");
				} else {
					msg.append("<div><p>" + mensagem + "</p></div>");
				}
				
				msg.append("</body></html>");
				
				messageBodyPart.setContent(msg.toString(), "text/html; charset=\"UTF-8\"");
				
				MimeBodyPart imagePart = new MimeBodyPart();
				DataSource fds = new FileDataSource(tempFile);
				imagePart.setDataHandler(new DataHandler(fds));
				imagePart.setHeader("Content-ID", "<abc12345>");
				
				Multipart multipart = new MimeMultipart("related");
				multipart.addBodyPart(messageBodyPart);
				multipart.addBodyPart(imagePart);
				
				if(relatorio != null || anexos != null) {
					if(relatorio != null && relatorio.exists()) {
						MimeBodyPart attachmentPart = new MimeBodyPart();
						attachmentPart.attachFile(relatorio);
						attachmentPart.setFileName(MimeUtility.encodeText(relatorio.getName(), "UTF-8", null));
						multipart.addBodyPart(attachmentPart);
					}
					
					if(anexos != null) {
						for(File anexo: anexos) {
							if(anexo == null || !anexo.exists()) {
								log.warn("Anexo é null! Ñ vai add!");
								continue;
							}
							MimeBodyPart attachmentPart = new MimeBodyPart();
							attachmentPart.attachFile(anexo);
							attachmentPart.setFileName(MimeUtility.encodeText(anexo.getName(), "UTF-8", null));
							multipart.addBodyPart(attachmentPart);
						}
					}
				}
				
				email.setContent(multipart);
				
				Transport.send(email);

				tempFile.delete();

		    	log.debug("Email enviado! Título: ["+email.getSubject()+"]");
		    	
		    	return;
				
			} catch (Exception e) {
				log.error("Erro", e);
				if(tentativa >= maxTentativas) {
					log.error("");
					log.error("");
					log.error("");
					log.error("");
					log.error("");
					log.error("");
					log.error("Erro ao enviar email!", e);
					log.error("");
					log.error("");
					log.error("");
					log.error("");
					log.error("");
					log.error("");
				} else {
					log.warn("FALHA ao enviar email na tentativa " + tentativa);
				}
			}
			
			tentativa++;
			
			sleepComLog(10);
		}
	}
	
	public static void sleepComLog(int segundos) {
		try {
			log.debug("Sleep de [" + segundos + "]...");
			Thread.sleep(segundos*1000);
		} catch (Exception e) {
			log.error(e);
		}
	}
	
	public static void enviaEmailThread(String titulo, String mensagem, String[] emails, File relatorio, ArrayList<File> anexos) {
		try {
			EmailThread email = new EmailThread(titulo, mensagem, emails, relatorio, anexos);
			Thread th = new Thread(email);
			log.debug("Inciando thread de envio de email: " + th.getName() + " - Assunto: ["+titulo+"]");
			th.start();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Erro ao enviar email por thread!", e);
		}
	}
}
