package br.vp.adv.scheduler;

import org.apache.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import br.vp.adv.job.CopiaIntimacaoJob;
import br.vp.adv.job.LimpezaTabelaJob;

public class CopiaIntimacaoScheduler {
	public static final Logger log = Logger.getLogger(CopiaIntimacaoScheduler.class);
	public static Scheduler scheduler;
	
	public static void schedule() {
		log.debug("");
		log.debug("");
		log.debug("Iniciando schedule!");
		SchedulerFactory sf = new StdSchedulerFactory();
		
		try {
			scheduler = sf.getScheduler();
			
			JobDetail job = JobBuilder.newJob(CopiaIntimacaoJob.class).withIdentity("copiaIntimacaoJob", "grupo01").build();
			log.debug("Job copiaIntimacaoJob instanciado!");
			
			Trigger trigger = TriggerBuilder.newTrigger().withIdentity("copiaIntimacaoTrigger", "grupo01")
					//.withSchedule(CronScheduleBuilder.cronSchedule("0 0/15 22-7 ? * MON-SAT")).build(); //a cada 15 min, das 22h até as 7h de segunda a sábado sem execução concorrente
					.withSchedule(CronScheduleBuilder.cronSchedule("0 0/5 * ? * MON-SAT")).build(); 
			
			scheduler.scheduleJob(job, trigger);
			
			JobDetail jobLimpeza = JobBuilder.newJob(LimpezaTabelaJob.class).withIdentity("LimpezaTabelaJob", "grupo01").build();
			log.debug("Job LimpezaTabelaJob instanciado!");
			
			Trigger triggerLimpeza = TriggerBuilder.newTrigger().withIdentity("LimpezaTabelaTrigger", "grupo01")
					.withSchedule(CronScheduleBuilder.cronSchedule("0 0 22 * * ?")).build(); //
		
			scheduler.scheduleJob(jobLimpeza, triggerLimpeza);
			
			log.debug("Próxima execução copiaIntimacao: ["+trigger.getNextFireTime()+"]");
			log.debug("Próxima execução LimpezaTabelaJob: ["+triggerLimpeza.getNextFireTime()+"]");
			
			scheduler.start();
			log.debug("Schedule iniciado!");
		} catch (Exception e) {
			log.error("Falha ao fazer o schedule!", e);
		}
	}
	
	public static void parar() {
		try {
			scheduler.shutdown();
			log.debug("Scheduler parado com sucesso!");
		} catch (Exception e) {
			log.error("Falhar ao parar scheduler!", e);
		}
	}
}
