package br.vp.adv.bo;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import br.vp.adv.dao.AtividadesDAO;
import br.vp.adv.vo.Atividade;

public class AtividadesBO {
	private static final Logger log = Logger.getLogger(AtividadesBO.class);

	public static ArrayList<Atividade> buscaPublicacoes(int uf, boolean primeiroGrau, boolean segundoGrau) {
		try {
			return AtividadesDAO.buscaPublicacoes(uf, primeiroGrau, segundoGrau);
		} catch (Exception e) {
			log.error("Erro ao buscar publicações em andamento...", e);
			e.printStackTrace();
			return null;
		}
	}

	public static ArrayList<Integer> temMultiplasAtividades(int codigo, int pasta) {
		try {
			return AtividadesDAO.verificarMultiplasAtividades(codigo, pasta);
		} catch (Exception e) {
			log.error("Erro ao verificar se existe mais de uma publicação em andamento na pasta [" + pasta + "] além da de código [" + codigo + "]", e);
			return null;
		}
	}

	public static ArrayList<Atividade> buscaPublicacoesErroTribunal(String tribunal) {
		try {
			return AtividadesDAO.buscaPublicacoesErroTribunal(tribunal);
		} catch (Exception e) {
			log.error("Erro ao buscar publicacoes com erro no PJE!", e);
			return null;
		}
	}
	
	
}
