package br.vp.adv.bo;

import java.io.File;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.vp.adv.constants.CopiaIntimacaoConstants;
import br.vp.adv.utils.Utils;
import br.vp.adv.utils.WasabiUtils;
import br.vp.adv.vo.Atividade;
import br.vp.adv.vo.LogCopiaIntimacao;
import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumBO {
	private static final Logger log = Logger.getLogger(SeleniumBO.class);
	
	static String frameAtual = null;
	
	public static void preencheCampo(WebElement campo, String valor) {
		campo.sendKeys(valor);
	}
	
	public static WebDriver abrirNavegador() {
		log.debug("Abrindo chrome...");
		
		WebDriver driver = null;
		try {
			//não aparece o navegador
			ChromeOptions options = new ChromeOptions();
			//options.addArguments("--headless");
			
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.prompt_for_download", false);
			prefs.put("plugins.always_open_pdf_externally", true);
			prefs.put("profile.default_content_settings.popups", 0);
			prefs.put("download.default_directory", CopiaIntimacaoConstants.PATH_DOCUMENTOS_TEMP);
			prefs.put("savefile.default_directory", CopiaIntimacaoConstants.PATH_DOCUMENTOS_TEMP);
			prefs.put("plugins.plugins_disabled", "PDFCreator");
			prefs.put("printing.print_preview_sticky_settings.appState", "{\"recentDestinations\": [{\"id\": \"Save as PDF\", \"origin\": \"local\", \"account\": \"\"}], \"selectedDestinationId\": \"Save as PDF\", \"version\": 2}");
			options.setExperimentalOption("prefs", prefs);
			
			options.addArguments("--kiosk-printing");
			options.addArguments("--lang=pt-br");
			//options.addArguments("---printing");
			
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver(options);
			
			//monitor 2
			driver.manage().window().setPosition(new Point(2700, 500));
			driver.manage().window().maximize();
			
			log.debug("Excluindo cookies ...");
			driver.manage().deleteAllCookies();
			
			limparCacheChrome(driver);
		} catch (Exception e) {
			log.error("Erro ao abrir navegador!", e);
		}
		
		log.debug("Chrome aberto");
		return driver;
	}
	
	private static void limparCacheChrome(WebDriver driver) {
		try {
			log.debug("Limpando cache do chrome...");
			
			driver.get("chrome://settings/clearBrowserData");
			
			WebDriverWait wdw = new WebDriverWait(driver, 10);
			
			log.debug(wdw.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/head/title"))).getText());
			
			log.debug("Clicando aba 'Avançado'...");
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("document.querySelector(\"body > settings-ui\").shadowRoot.querySelector(\"#main\").shadowRoot.querySelector(\"settings-basic-page\").shadowRoot."
					+ "querySelector(\"#basicPage > settings-section:nth-child(11) > settings-privacy-page\").shadowRoot.querySelector(\"settings-clear-browsing-data-dialog\")."
					+ "shadowRoot.querySelector(\"#clearBrowsingDataDialog > div:nth-child(2) > cr-tabs\").shadowRoot.querySelector(\"div.tab.selected\")");
			log.debug("Clicou na aba 'Avançado'!");
			
			WebElement weSelectPeriodo = (WebElement) js.executeScript("return document.querySelector(\"body > settings-ui\").shadowRoot.querySelector(\"#main\").shadowRoot.querySelector(\"settings-basic-page\").shadowRoot.querySelector(\"#basicPage > settings-section:nth-child(11) > settings-privacy-page\").shadowRoot.querySelector(\"settings-clear-browsing-data-dialog\").shadowRoot.querySelector(\"#clearFrom\").shadowRoot.querySelector(\"#dropdownMenu\")");
			Select selectPeriodo = new Select(weSelectPeriodo);
			log.debug("Opções select:");
			for(WebElement option: selectPeriodo.getOptions()) {
				log.debug(option.getText());
			}
			
			try {
				selectPeriodo.selectByVisibleText("Todo o período");
			} catch (Exception estop) {
				log.debug("Selecionando 'All time'...");
				selectPeriodo.selectByVisibleText("All time");
			}
			
			@SuppressWarnings("unchecked")
			List<WebElement> listaCheckboxs = (List<WebElement>) js.executeScript("return document.querySelector(\"body > settings-ui\").shadowRoot.querySelector(\"#main\").shadowRoot.querySelector(\"settings-basic-page\").shadowRoot.querySelector(\"#basicPage > settings-section:nth-child(11) > settings-privacy-page\").shadowRoot.querySelector(\"settings-clear-browsing-data-dialog\").shadowRoot.querySelector(\"#advanced-tab\").querySelectorAll(\"settings-checkbox\")");
			log.debug("Encontrou ["+listaCheckboxs.size()+"] checkbox pra preencher...");
			for(WebElement itemLista: listaCheckboxs) {
				log.debug(itemLista.getText().replace("\r\n", "--").replace("\n", "--"));
				WebElement checkbox = (WebElement) js.executeScript("return arguments[0].shadowRoot.querySelector(\"#checkbox\").shadowRoot.querySelector(\"#checkbox\")", itemLista);
				String marcado = checkbox.getAttribute("aria-checked");
				log.debug("marcado: ["+marcado+"]");
				
				if(!marcado.equals("true")) {
					log.debug("Marcando checkbox...");
					js.executeScript("arguments[0].click();", checkbox);
					log.debug("Marcou!");
				}
			}
			
			log.debug("Clicando em 'Remover dados'...");
			js.executeScript("document.querySelector(\"body > settings-ui\").shadowRoot.querySelector(\"#main\").shadowRoot.querySelector(\"settings-basic-page\").shadowRoot.querySelector(\"#basicPage > settings-section:nth-child(11) > settings-privacy-page\").shadowRoot.querySelector(\"settings-clear-browsing-data-dialog\").shadowRoot.querySelector(\"#clearButton\").click();");
			log.debug("Clicou em 'Remover dados'!");
		} catch (Exception e) {
			log.error("Erro ao limpar cache do chrome!", e);
		}
	}

	public static boolean loginProjudi(WebDriver driver) {
		log.debug("Fazendo login PROJUDIPR...");
		try {
			WebDriverWait wdw = new WebDriverWait(driver, 30);
			
			driver.get(CopiaIntimacaoConstants.URL_PROJUDI);
			log.debug("Abriu projudi!");
			
			//vai pra frame mainFrame
			wdw.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//*[@id=\"mainFrame\"]")));
			log.debug("Mudou para frame mainFrame!");
			frameAtual = "mainFrame";
			
			//localiza  acesso ao sistema Advogados,Partes
			WebElement botaoAdvPartes = wdw.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/ul/li[2]/span[2]")));
			botaoAdvPartes.click();
			
			WebElement campoLogin = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"username\"]")));
			WebElement campoSenha = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"password\"]")));
			
			preencheCampo(campoLogin, CopiaIntimacaoConstants.LOGIN_PROJUDI_JAIME);
			preencheCampo(campoSenha, CopiaIntimacaoConstants.SENHA_PROJUDI_JAIME);
			String codigoUnico = Utils.geradorCod2FAPROJUDI(CopiaIntimacaoConstants.KEY_2FA_PROJUDI_JAIME);
			
			log.debug("Preencheu campos de login");
			
			WebElement btEntrar = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"kc-login\"]")));
			btEntrar.click();
			log.debug("Clicou no botão entrar");
			
			//2FA PROJUDI 09/21
			WebElement campoCodigoUnico = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"totp\"]")));
			preencheCampo(campoCodigoUnico, codigoUnico);
			log.debug("Preencheu campo código unico com o código [" + codigoUnico + "]");		
			
			btEntrar = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"kc-login\"]")));
			btEntrar.click();
			log.debug("Clicou no botão entrar");
			
			//espera aparecer botão BUSCA->PROCESSOS 1º GRAU
			wdw.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div/nav/ul/li[8]/a")));
			log.debug("Encontrou botão busca processos");
			
			return true;
		} catch (Exception e) {
			log.error("Erro ao realizar login no PROJUDI!", e);
			return false;
		}
	}
	
	
	
	public static String buscaProcessoPROJUDIPR(WebDriver driver, Atividade atividade) {
		try {
			String numeroProcesso = atividade.getNumeroProcesso();
			String instancia = atividade.getGrauProcesso();
			int pasta = atividade.getPasta();
			
			LogCopiaIntimacao logCI = new LogCopiaIntimacao();
			logCI.setCodigoAtividade(atividade.getCodigo());
			logCI.setPasta(pasta);
			logCI.setNumeroProcesso(numeroProcesso);
			logCI.setInstancia(instancia);
			logCI.setDataPublicacaoAtividade(atividade.getDataPublicacao());
			logCI.setTribunal("PROJUDIPR");
			
			WebDriverWait wdw = new WebDriverWait(driver, 30);
			WebDriverWait wdw1 = new WebDriverWait(driver, 5);
			WebDriverWait wdw2 = new WebDriverWait(driver, 10);
			
			try {
				//volta para mainFrame
				driver.switchTo().defaultContent();
				wdw.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//*[@id=\"mainFrame\"]")));
				log.debug("Mudou para mainFrame");
				frameAtual = "mainFrame";
			} catch (Exception e2) {
			}
			
			if(instancia == null || instancia == "") {
				log.error("Instancia do pasta " + pasta + " não é setada: " + instancia);
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Instância da pasta não é setada!");
				return null;
			}
			
			log.debug("Instancia da pasta " + pasta + ": " + instancia);
			
			if(!abreProcesso(driver, wdw, wdw1, wdw2, numeroProcesso, instancia, logCI)) {
				return null;
			}
			
			if(!buscaIntimacoes(driver, wdw1, atividade, logCI, false)) {
				return null;
			}
			
			return "Sucesso!";
		} catch (Exception e) {
			log.error("Erro ao buscar processo " + atividade.getNumeroProcesso() + " no PROJUDIPR!", e);
			return null;
		}
	}
	
	private static boolean buscaIntimacoes(WebDriver driver, WebDriverWait wdw1, Atividade atividade, LogCopiaIntimacao logCI, boolean outraInstancia) {
		try {
			int codigoAtividade = atividade.getCodigo();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date dataCompPubAtividade = sdf.parse(atividade.getDataPublicacao());
			
			fechaToastNotify(driver, wdw1);
			
			//verifica se já teve uma publicação com mesma data e mesma pasta
			log.debug("Verificando se já teve uma publicação da pasta [" + atividade.getPasta() + "] da data [" + atividade.getDataPublicacao() + "] com status sucesso...");
			int pubMesmoDocs = CopiaIntimacaoBO.verificaPublicacaoMesmaData(atividade);
			if(pubMesmoDocs > 0) {
				log.debug("Já teve uma atividade de publicação com a data [" + atividade.getDataPublicacao() + "] com sucesso: ["+pubMesmoDocs+"]! Não vai rodar essa atividade!");
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.DOCUMENTOS_ATIVIDADE_MESMO_DIA, logCI, "A atividade ["+pubMesmoDocs+"] é do mesmo dia e tem status sucesso! Não roda para evitar duplicidade de documentos!");
				return true;
			}
			
			//pega quantidade de movimentações 
			int quantidadeMovimentacoes = pegaQuantidadeMovimentacoes(wdw1, wdw1, atividade.getNumeroProcesso(), atividade.getGrauProcesso(), logCI);
			if(quantidadeMovimentacoes == -1) {
				log.error("Falha ao pegar a quantidade de movimentações!");
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao pegar a quantidade de movimentações!");
				return false;
			}
			
			
			//pega movimentações
			List<WebElement> movimentacoes = null;
			
			try {
				movimentacoes = wdw1.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr")));
			} catch (Exception nem) {
				log.debug("Não encontrou as movimentações com xpath normal, buscando com outro...");
				movimentacoes = wdw1.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("/html/body/div[1]/div[2]/form/fieldset/table[4]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr")));
				log.debug("Encontrou as movimentações com xpath secundário!");
			}
			
			if(movimentacoes == null || movimentacoes.size() == 0) {
				log.error("Nenhuma movimentação encontrada!");
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Nenhuma movimentação encontrada!");
				return false;
			} 
			
			boolean procuraOutraInstancia = false;
			//boolean encontrouCopias = false;
			ArrayList<String> sequenciasParaBaixarDocumentos = new ArrayList<String>();
			
			//log.debug("Encontrou [" + (int) (movimentacoes.size() / 2) + "] movimentações no [" + atividade.getGrauProcesso() + "]");
			log.debug("Movimentos:");
			
			int soma = 1;
			//for(int i = 0; i < (((int) (movimentacoes.size() / 2)) - 2); i++) {
			for(int i = 0; i < quantidadeMovimentacoes; i++) {
				
				fechaToastNotify(driver, wdw1);
				
				//int indTr = ((i*2)+1); //linhas ímpares
				
				//a partir de 07/03/21 passou a ser a cada 3 linhas (ex: 1, 4, 7, 10, ...)
				int indTr = ((i*2)+soma);
				soma++;
				
				log.debug("indTr = [" + indTr + "]");
				
				String xpathNomeMovimento =        "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[4]/b";
				String xpathDataMovimento =		   "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[3]";
				String xpathNomeMovimentoStrike =  "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[4]/strike/b";
				String xpathDescricaoMovimento =   "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[4]";
				String xpathSeqMovimento =         "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[2]";
				String xpathMovPor =         	   "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[5]";
				
				//secundário (caso o primário falhe)              
				String xpathNomeMovimento2 =       "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[4]/b";
				String xpathDataMovimento2 =	   "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[3]";
				String xpathNomeMovimentoStrike2 = "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[4]/strike/b";
				String xpathDescricaoMovimento2 =  "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[4]";
				String xpathSeqMovimento2 =        "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[2]";
				String xpathMovPor2 =        	   "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[5]";
				
				String xpathNomeMovimento3 =       "/html/body/div[1]/div[2]/form/fieldset/table[4]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[4]/b";
				String xpathDataMovimento3 =	   "/html/body/div[1]/div[2]/form/fieldset/table[4]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[3]";
				String xpathNomeMovimentoStrike3 = "/html/body/div[1]/div[2]/form/fieldset/table[4]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[4]/strike/b";
				String xpathDescricaoMovimento3 =  "/html/body/div[1]/div[2]/form/fieldset/table[4]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[4]";
				String xpathSeqMovimento3 =        "/html/body/div[1]/div[2]/form/fieldset/table[4]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[2]";
				String xpathMovPor3 =              "/html/body/div[1]/div[2]/form/fieldset/table[4]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[5]";
				
				WebElement nomeMov = null;
				WebElement dataMov = null;
				WebElement descricaoMov = null;
				WebElement seqMov = null;
				
				//busca seq do movimento com xpaths primário e secundário
				try {
					seqMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathSeqMovimento)));
				} catch (Exception esm) {
					log.debug("Não encontrou seq. do movimento com xpath padrão... Buscando com alternativos");
					try {
						seqMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathSeqMovimento2))); 
					} catch (Exception esm1) {
						log.debug("Falha ao encontrar seq. do movimento com xpath2, bucando com o 3...");
						try {
							seqMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathSeqMovimento3)));
						} catch (Exception esm2) {
							log.error("Erro ao encontrar seq. do movimento com xpath primário, secundário e terciário!", esm2);
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a seq. de algum movimento!");
							return false;
						}
						
					}
				}
				
				int sequenciaMovimento = -1;
				try{
					sequenciaMovimento = Integer.parseInt(seqMov.getText().trim());
				} catch (Exception csm) {
					log.debug("Não conseguiu converter encontrou seq. do movimento no xpath primário! Buscando com secundário...");
					try {
						sequenciaMovimento = Integer.parseInt(wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathSeqMovimento2))).getText().trim());
						log.debug("Sucesso ao encontrar seq. do movimento com xpath secundário!");
					} catch (Exception csm1) {
						try {
							sequenciaMovimento = Integer.parseInt(wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathSeqMovimento3))).getText().trim());
							log.debug("Sucesso ao encontrar seq. do movimento com xpath secundário!");
						} catch (Exception csm2) {
							log.error("Erro ao encontrar seq. do movimento com xpaths!", csm2);
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a seq. de algum movimento!");
							return false;
						}
						
					}
				}
				
				String dataHoraMovimento = "";
				String dataMovimento = "";
				try {
					dataMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathDataMovimento)));
					dataHoraMovimento = dataMov.getText().trim();
					if(dataHoraMovimento.equals("")) {
						throw new Exception("data do movimento vazia...");
					}
				} catch (Exception edm) {
					log.debug("Não encontrou data do movimento com xpath primário... Buscando com secundário...");
					try {
						dataMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathDataMovimento2)));
						dataHoraMovimento = dataMov.getText().trim();
						log.debug("Encontrou data do movimento com xpath secundário!");
					} catch (Exception edm1) {
						try {
							dataMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathDataMovimento3)));
							dataHoraMovimento = dataMov.getText().trim();
							log.debug("Encontrou data do movimento com xpath terciário!");
						} catch (Exception edm2) {
							log.error("Erro ao pegar data do movimento!", edm2);
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a data de algum movimento!");
							return false;
						}
						
					}
				}
				
				if(dataMov == null || dataHoraMovimento.equals("")) {
					log.error("Erro ao pegar data do movimento!");
					Thread.sleep(15000);
					CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a data de algum movimento!");
					return false;
				}
				
				
				log.debug("dataHoraMovimento: [" + dataHoraMovimento + "]");
				
				//PEGA APENAS A DATA DO MOVIMENTO NO FORMATO dd/mm/yyyy
				String[] dataHora = dataHoraMovimento.split(" ");
				if(dataHora.length < 2) {
					log.error("Erro ao pegar data do movimento!");
					CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a data de algum movimento!");
					return false;
				}
				
				dataMovimento = dataHora[0];
				
				String nomeMovimento = "";
				
				try {
					nomeMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathNomeMovimento)));
					nomeMovimento = nomeMov.getText();
				} catch (Exception enm) {
					log.debug("Não encontrou nome do movimento com xpath padrão... Buscando com alternativo...");
					try {
						nomeMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathNomeMovimento2)));
						nomeMovimento = nomeMov.getText();
					} catch (Exception enm1) {
						//log.debug("Não encontrou nome do movimento com xpath secundário... Buscando com xpath STRIKE primário...");
						log.debug("Não encontrou nome do movimento com xpath secundário, buscando com terciário...");
						try {
							nomeMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathNomeMovimento3)));
							nomeMovimento = nomeMov.getText();
						} catch (Exception enm2) {
							log.debug("Não encontrou nome do movimento com xpath teriário, buscando com xpath STRIKE primário...");
							try {
								nomeMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathNomeMovimentoStrike)));
								nomeMovimento = nomeMov.getText();
								log.debug("["+sequenciaMovimento+"] - " + nomeMovimento);
								log.debug("Movimento com STRIKE, não considerando...");
								continue;
							} catch (Exception enms1) {
								log.debug("Não encontrou nome do movimento com xpath STRIKE primário... Buscando com alternativo...");
								try {
									nomeMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathNomeMovimentoStrike2)));
									nomeMovimento = nomeMov.getText();
									log.debug("["+sequenciaMovimento+"] - " + nomeMovimento);
									log.debug("Movimento com STRIKE, não considerando...");
									continue;
								} catch (Exception enms2) {
									try {
										nomeMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathNomeMovimentoStrike3)));
										nomeMovimento = nomeMov.getText();
										log.debug("["+sequenciaMovimento+"] - " + nomeMovimento);
										log.debug("Movimento com STRIKE, não considerando...");
										continue;
									} catch (Exception enms3) {
										log.error("Erro ao encontrar seq. do movimento com xpath primário, secundário e terciário!", enms3);
										CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar o nome de algum movimento!");
										return false;
									}
									
								}
							}
						}
					}
				}
					
				String descricaoMovimento = "";
				try {
					descricaoMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathDescricaoMovimento)));
					descricaoMovimento = descricaoMov.getText().trim();
					if(descricaoMovimento.equals("")) {
						throw new Exception("descricao do movimento vazia...");
					}
				} catch (Exception edm) {
					log.debug("Erro ao encontrar descrição do movimento com o xpath primário, buscando com secundário...");
					try {
						descricaoMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathDescricaoMovimento2)));
						descricaoMovimento = descricaoMov.getText().trim();
					} catch (Exception edm1) {
						log.debug("Erro ao encontrar descrição do movimento com o xpath secundário, buscando com terciário...");
						try {
							descricaoMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathDescricaoMovimento3)));
							descricaoMovimento = descricaoMov.getText().trim();
						} catch (Exception edm2) {
							log.error("Erro ao encontrar descrição do movimento com xpaths primário e secundário!", edm2);
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a descrição de algum movimento!");
							return false;
						}
					}
				}
				
				descricaoMovimento = descricaoMovimento.replace("\n", "").trim();
				
				String movimentadoPor = "";
				try {
					log.debug("Procurando movimentadoPor com xpath ["+xpathMovPor+"]");
					movimentadoPor = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathMovPor))).getText();
					if(movimentadoPor != null) {
						movimentadoPor = movimentadoPor.trim();
					}
					if(movimentadoPor.equals("")) {
						throw new Exception("Campo 'movimentadoPor' é vazio!");
					}
				} catch (Exception emp) {
					log.debug("Erro ao encontrar movimentado por do movimento com o xpath primário, buscando com secundário...");
					try {
						log.debug("Procurando movimentadoPor com xpath ["+xpathMovPor2+"]");
						movimentadoPor = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathMovPor2))).getText();
						if(movimentadoPor != null) {
							movimentadoPor = movimentadoPor.trim();
						}
						if(movimentadoPor.equals("")) {
							throw new Exception("Campo 'movimentadoPor' é vazio!");
						}
					} catch (Exception edm1) {
						log.debug("Erro ao encontrar movimentado por do movimento com o xpath secundário, buscando com terciário...");
						try {
							log.debug("Procurando movimentadoPor com xpath ["+xpathMovPor3+"]");
							movimentadoPor = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathMovPor3))).getText();
							if(movimentadoPor != null) {
								movimentadoPor = movimentadoPor.trim();
							}
							if(movimentadoPor.equals("")) {
								throw new Exception("Campo 'movimentadoPor' é vazio!");
							}
						} catch (Exception edm2) {
							log.error("Erro ao encontrar movimentado por do movimento com xpaths primário e secundário!", edm2);
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar o movimentado por de algum movimento!");
							return false;
						}
					}
				}
				
				if(movimentadoPor != null) {
					movimentadoPor = movimentadoPor.trim();
				}
				
				log.debug("movimentadoPor: ["+movimentadoPor+"]");
				
				fechaToastNotify(driver, wdw1);
					
				if(nomeMovimento.trim().equals("CONFIRMADA A INTIMAÇÃO ELETRÔNICA") || nomeMovimento.trim().equals("EXPEDIÇÃO DE INTIMAÇÃO")) {
					log.debug("[" + sequenciaMovimento + "] - [" + dataMovimento + "] - [" + nomeMovimento + "] - [" + descricaoMovimento + "] - [" + movimentadoPor + "]");
					log.debug("Encontrou uma confirmação/expedição de intimação! Verificando a data...");
					
					log.debug("Data movimento: [" + dataMovimento + "] - data atividade: [" + atividade.getDataPublicacao() + "]");
					
					if(!dataMovimento.equals(atividade.getDataPublicacao())) {
						
						log.debug("Não é a mesma data, buscando a data da confirmação na descrição do movimento...");
						if(!descricaoMovimento.equals("")) {
							int indexDe = descricaoMovimento.indexOf(" em ");
							if(indexDe != -1) {
								String dataDescricao = descricaoMovimento.substring(indexDe);
								dataDescricao = dataDescricao.trim().replace("em", "").trim();
								log.debug("data descrição (filtrando): [" + dataDescricao + "]");
								
								int indexPrimeiroEspacoPosData = dataDescricao.indexOf(" ");
								if(indexPrimeiroEspacoPosData != -1) {
									dataDescricao = dataDescricao.substring(0, indexPrimeiroEspacoPosData).trim();
									log.debug("data descrição (filtrando): [" + dataDescricao + "]");
									
									log.debug("Data na descrição do movimento: [" + dataDescricao + "]");
									
									if(!dataDescricao.equals(atividade.getDataPublicacao())) {
										log.debug("A data da descrição [" + dataDescricao +"] não é a mesma da atividade [" + atividade.getDataPublicacao() + "]! Continuando para próximo movimento...");
										
										Date dataMovimentoComparacao = sdf.parse(dataMovimento);
										//para a busca caso estiver estiver olhando para uma movimentacao com data anterior a data da publicação na atividade
										if(dataMovimentoComparacao.before(dataCompPubAtividade)) {
											log.error("Data do movimento na descrição ["+dataMovimento+"] é anterior à data da publicação na atividade ["+atividade.getDataPublicacao()+"]... Finalizando busca...");
											
											if(sequenciasParaBaixarDocumentos.size() == 0) {
												procuraOutraInstancia = true;
												log.debug("Vai procurar em outra instância!");
											}
											
											break;
											
											//return "outrainstancia";
										}
										
										continue;
									}
									
								} else {
									log.debug("Falha ao encontrar data na descrição do movimento...");
									continue;
									//break;
								}
							} else {
								log.debug("Falha ao encontrar data na descrição do movimento...");
								continue;
								//break;
							}
						} else {
							log.error("Erro ao encontrar descrição do movimento!");
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a descrição de algum movimento!");
							return false;
						}
					}
					
					
					//A PARTIR DE 03/2022 A BUSCA É FEITA PELO 'JAIME OLIVEIRA PENTEADO' NA COLUNA 'MOVIMENTADO POR'
					log.debug("É a mesma data, verificando movimentado por...");
					if(!movimentadoPor.equals("")) {
						if(!movimentadoPor.toUpperCase().contains("JAIME OLIVEIRA PENTEADO")) {
							log.debug("O movimentador ñ é o Jaime! : ["+movimentadoPor+"]! Pulando...");
							continue;
						}
						log.debug("É o Jaime!");
					} else {
						log.error("Erro ao encontrar movimentado por do movimento!");
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar o movimentado por de algum movimento!");
						return false;
					}
					
					/*
					log.debug("É a mesma data, verificando cliente...");
					fechaToastNotify(driver, wdw1);
					
					//verifica se tem o nome do cliente nessa descrição de movimento
					String nomeParte = null;
					String[] iniciosNomeParte = {"defensor de ", "Para Perito "};
					if(!descricaoMovimento.equals("")) {
						int indexDefensor = descricaoMovimento.indexOf(iniciosNomeParte[0]);
						
						int indINP = 1;
						
						while(indINP < iniciosNomeParte.length && indexDefensor == -1) {
							indexDefensor = descricaoMovimento.indexOf(iniciosNomeParte[indINP]);
							log.debug("Encontrou [" + iniciosNomeParte[indINP] + "] ...");
							indINP++;
						}
						
						
						if(indexDefensor == -1) {
							log.error("Falha ao encontrar parte do movimento na descrição!");
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a parte na descrição de algum movimento!");
							return false;
						}
						
						String stringDefensor = descricaoMovimento.substring(indexDefensor);
						for(int ir = 0; ir < iniciosNomeParte.length; ir++) {
							stringDefensor = stringDefensor.replace(iniciosNomeParte[ir], "");
						}
						
						
						String[] palavrasFimNomeParte = {" com ", " em ", " representado(a) ", " - "};
						
						int indP = 0, indexFimNomeParte = -1;
						
						while(indP < palavrasFimNomeParte.length) {
							int indexAtualPalavra =  stringDefensor.indexOf(palavrasFimNomeParte[indP]);
							if(indexAtualPalavra != -1 && indexFimNomeParte == -1) {
								indexFimNomeParte = indexAtualPalavra;
							} else if(indexAtualPalavra != -1 && indexAtualPalavra < indexFimNomeParte) {
								indexFimNomeParte = indexAtualPalavra;
							}
							
							indP++;
						}
						
						if(indexFimNomeParte == -1) {
							log.error("Não encontrou o final do nome da parte!");
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a parte na descrição de algum movimento!");
							return false;
						}
						
						stringDefensor = stringDefensor.substring(0, indexFimNomeParte);
						stringDefensor = stringDefensor.replace("(", "").replace(")", "").trim();
						
						nomeParte = stringDefensor;
						log.debug("Parte movimento: [" + nomeParte + "]");
						
						if(!Utils.nomeParteVerificado(nomeParte, atividade)) {
							log.error("Não encontrou o cliente na descrição!");
							continue;
						}
					} else {
						log.error("Erro ao encontrar descrição do movimento!");
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a descrição de algum movimento!");
						return false;
					}
					*/
					
					log.debug("Encontrou intimação correta! Pegando descrição...");
					descricaoMovimento = descricaoMovimento.replace("CONFIRMADA A INTIMAÇÃO ELETRÔNICA", "").replace("EXPEDIÇÃO DE INTIMAÇÃO", "").trim();
					log.debug("Encontrou a última intimação na sequência ["+sequenciaMovimento+"]: " + descricaoMovimento);	
					
					//busca sequencia que essa confirmação se refere
					int indexReferente = descricaoMovimento.indexOf("*Referente");
					if(indexReferente == -1) {
						indexReferente = descricaoMovimento.indexOf("Referente ao evento");
						if(indexReferente == -1) {
							log.error("Erro ao buscar a referência da confirmação de intimação...");
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a referência da confirmação de intimação!");
							return false;
						}
					}
						
					String referenciaCompleta = descricaoMovimento.substring(indexReferente);
						
					int indexParentesesInicio = referenciaCompleta.indexOf("(");
					int indexParentesesFinal = referenciaCompleta.indexOf(")");
						
					if(indexParentesesInicio == -1 || indexParentesesFinal == -1) {
						log.error("Erro ao buscar a referência da confirmação de intimação...");
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a referência da confirmação de intimação!");
						return false;
					}
						
					String sequenciaCompleta = referenciaCompleta.substring(indexParentesesInicio, indexParentesesFinal);
					log.debug("Sequência completa: [" + sequenciaCompleta + "]");
						
					String idSeq = sequenciaCompleta.replace("(", "").replace(")", "").replace("seq.","").trim();
					log.debug("idSeq: [" + idSeq + "]");
					
					boolean proibida = false;
					/*
					//verifica se essa seq não é proibida
					
					if(sequenciasProibidas != null && sequenciasProibidas.size() > 0) {
						for(String sequencia: sequenciasProibidas) {
							log.debug("Verificando a sequência encontrada [" + sequenciaMovimento + "] x [" + sequencia + "]!");
							if(sequencia != null && sequencia.trim().equals(String.valueOf(sequenciaMovimento))) {
								log.debug("Essa sequência é proibida, procurando outra...");
								proibida = true;
							}
						}
					}
					*/
						
					if(!proibida) {
						//logCI.setReferencia(String.valueOf(sequenciaMovimento));
						if(logCI.getReferencia() != null) {
							logCI.setReferencia(logCI.getReferencia() + "/" + sequenciaMovimento + "->" + idSeq);
						} else {
							logCI.setReferencia(sequenciaMovimento + "->" + idSeq);
						}
						//encontrouCopias = true;
						
						if(!sequenciasParaBaixarDocumentos.contains(idSeq)) {
							//adiciona documento para baixar
							sequenciasParaBaixarDocumentos.add(idSeq);
							log.debug("Adicionou sequencia [" + idSeq + "] para baixar!");
						} else {
							log.debug("A sequencia [" + idSeq + "] já está na fila para download!");
						}
					}
				} else {
					log.debug("[" + sequenciaMovimento + "] - [" + dataMovimento + "] - [" + nomeMovimento + "]!");
					
					//para a busca se passou mais de 14 dias da data da publicação da atividade
					LocalDate ldAtividade = dataCompPubAtividade.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					Date dataMovimentoComparacao = sdf.parse(dataMovimento);
					LocalDate ldMovimento = dataMovimentoComparacao.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					
					if(ldMovimento.until(ldAtividade, ChronoUnit.DAYS) >= 14) {
						log.error("A data do movimento [" + ldMovimento + "] é muito antiga para a data da atividade [" + ldAtividade + "]! Parando busca...");
						
						if(sequenciasParaBaixarDocumentos.size() == 0) {
							procuraOutraInstancia = true;
							log.debug("Vai procurar em outra instância!");
						}
						break;
					}
				}
			}
			
			if(procuraOutraInstancia && !outraInstancia) {
				log.debug("[INÍCIO PROCURA EM OUTRA INSTÂNCIA]+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
				fechaToastNotify(driver, wdw1);
				
				if(atividade.getGrauProcesso().equals("1º Grau")) {
					log.debug("A instância é 1º grau, vai procurar nos recursos...");
					WebElement btVisRecursos = null;
					try {
						btVisRecursos =  wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Clique aqui para visualizar os recursos relacionados")));
					} catch (Exception e) {
						log.error("Falha ao buscar recursos!", e);
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao procurar recursos!");
						return false;
					}
					
					if(btVisRecursos.getText().trim().equals("Clique aqui para visualizar os recursos relacionados")) {
						log.debug("Encontrou o botão para visualizar os recursos relacionados...");
						
						String instancia = atividade.getGrauProcesso();
						String numeroProcesso = atividade.getNumeroProcesso();
						
						btVisRecursos.click();
						log.debug("Clicou no botão mostrar recursos...");
						
						//pega todas as linhas e procura a 'Árvore Processual'
						List<WebElement> linhasRecurso = wdw1.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("/html/body/div[1]/div[2]/form/fieldset/table[1]/tbody/tr")));
						log.debug("Obteve [" + linhasRecurso.size() + "] linhas de dados do processo... Procurando 'Árvore Processual'");
						
						ArrayList<String> nomesRecursos = new ArrayList<String>();
						
						for(WebElement linha: linhasRecurso) {
							if(linha.getText().trim().contains("Árvore Processual")){
								log.debug(linha.getText());
								List<WebElement> links = linha.findElements(By.tagName("a"));
								
								for(WebElement link: links) {
									String nomeLink = link.getText().trim();
									log.debug("Link: [" + nomeLink + "]");
									if(nomeLink.contains("Recurso: ")) {
										nomesRecursos.add(nomeLink);
									}
								}
							}
						}
						
						for(String nomeRecurso: nomesRecursos) {
							WebElement linkRecurso = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(nomeRecurso)));
							log.debug("Encontrou recurso: ["+nomeRecurso+"]! Clicando...");
							
							linkRecurso.click();
							
							log.debug("Esperando abrir o recurso...");
							Thread.sleep(3000);
							
							WebElement tituloMudouRecurso = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div[2]/form/h3")));
							
							if(!tituloMudouRecurso.getText().trim().contains("Recurso")) {
								log.error("Falha ao abrir recurso! Não encontrou 'Recurso' no título!");
								return false;
							}
							
							log.debug("Abriu recurso ["+nomeRecurso+"]! Pegando botões...");
							
							List<WebElement> botoesRecurso = wdw1.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[1]/td/div/ul/li")));
							log.debug("Encontrou [" + botoesRecurso.size() + "] botões! Procurando movimentações...");
							
							boolean btMov = false;
							for(WebElement botao: botoesRecurso) {
								if(botao.getText().trim().equals("Movimentações")) {
									log.debug("Encontrou botão movimentações! Clicando...");
									btMov = true;
									botao.click();
								}
							}
							
							if(!btMov) {
								log.error("Falha ao encontrar botão movimentações...");
								return false;
							}
							
							Thread.sleep(3000);
							fechaToastNotify(driver, wdw1);
							return buscaIntimacoes(driver, wdw1, atividade, logCI, true);
						}
						
						
					}
				} else {
					log.debug("A instância é 2º grau, vai procurar no processo...");
					fechaToastNotify(driver, wdw1);
					String instancia = atividade.getGrauProcesso();
					String numeroProcesso = atividade.getNumeroProcesso();
					
					log.debug("Procurando 'Árvore Processual'...");
					
					//pega todas as linhas e procura a 'Árvore Processual'
					List<WebElement> linhasRecurso = wdw1.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("/html/body/div[1]/div[2]/form/fieldset/table[1]/tbody/tr")));
					log.debug("Obteve [" + linhasRecurso.size() + "] linhas de dados do processo... Procurando 'Árvore Processual'");
					
					boolean entrouNoLink = false;
					for(WebElement linha: linhasRecurso) {
						if(linha.getText().trim().contains("Árvore Processual")){
							log.debug(linha.getText());
							List<WebElement> links = linha.findElements(By.tagName("a"));
							
							for(WebElement link: links) {
								log.debug("Link: [" + link.getText().trim() + "]");
								if(link.getText().trim().contains("Processo: ")) {
									log.debug("Encontrou processo! Clicando...");
									
									link.click();
									entrouNoLink = true;
									break;
								}
							}
							
							if(entrouNoLink) {
								break;
							}
						}
					}
					
					log.debug("Esperando abrir o processo...");
					Thread.sleep(3000);
					
					WebElement tituloMudouProcesso = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div[2]/form/h3")));
					
					if(!tituloMudouProcesso.getText().trim().contains("Processo")) {
						log.error("Falha ao abrir recurso! Não encontrou 'Processo' no título!");
						return false;
					}
					
					log.debug("Abriu processo!");
					fechaToastNotify(driver, wdw1);
					return buscaIntimacoes(driver, wdw1, atividade, logCI, true);
				}
				return false;
			}
			
			log.debug("Sequências para baixar: " + sequenciasParaBaixarDocumentos);
			
			
			if(sequenciasParaBaixarDocumentos.size() == 0) {
				log.error("Não encontrou nenhuma intimação para baixar!");
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Não encontrou intimação para baixar!");
				return false;
			}
			
			//baixa todas as sequencias
			ArrayList<File> arquivosAnexar = new ArrayList<File>();
			int idDoc = 1;
			for(String sequenciaBaixar: sequenciasParaBaixarDocumentos) {
				log.debug("Baixando sequência [" + sequenciaBaixar + "]");
				
				//for(int i = 0; i < ((int) (movimentacoes.size() / 2) - 2); i++) {
				int soma2 = 1;
				for(int i = 0; i < quantidadeMovimentacoes; i++) {
					fechaToastNotify(driver, wdw1);
					
					//int indTr = ((i*2)+1); //linhas ímpares
					
					//a partir de 07/03/21 passou a ser a cada 3 linhas (ex: 1, 4, 7, 10, ...)
					int indTr = ((i*2)+soma2);
					soma2++;
					
					log.debug("indTr = [" + indTr + "]");
					
					String xpathAbrirDocumentos    = "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[1]/a";
					String xpathSeqMovimento       = "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[2]";
					String xpathSeqMovimento2       = " /html/body/div[1]/div[2]/form/fieldset/table[4]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[2]";
					String xpathDescricaoMovimento = "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[4]";
					
					String strMovAtual = null;
					try {
						strMovAtual = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathSeqMovimento))).getText().trim();
					} catch (Exception esma) {
						strMovAtual = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathSeqMovimento2))).getText().trim();
					}
					
					
					if(strMovAtual.equals(sequenciaBaixar)) {
						WebElement descricaoMovimentoReferencia = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathDescricaoMovimento)));
						log.debug("Encontrou movimento de referência de id [" + sequenciaBaixar + "] - [" + descricaoMovimentoReferencia.getText().replace("\n", "") + "]");
						
						WebElement botaoExpandir = null;
						try {
							botaoExpandir = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathAbrirDocumentos)));
						} catch (Exception e) {
							log.error("Não encontrou botão expandir na sequência ["+sequenciaBaixar+"]!");
							
							if(sequenciasParaBaixarDocumentos.size() == 1) {
								CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Não encontrou botão expandir na seq. [" + sequenciaBaixar + "]");
								return false;
							}
							continue;
						}
						
						botaoExpandir.click();
						log.debug("Clicou botão expandir da sequência [" + sequenciaBaixar + "]");
						
						Thread.sleep(5000);
						
						String xpathTrDocsMovimentacao = "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+(indTr+1)+"]";
						
						//verifica se abriu a tr abaixo
						WebElement trDocsMovimentacao = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathTrDocsMovimentacao)));
						log.debug("Encontrou a tr de documentos da movimentação: [" + trDocsMovimentacao.getText() + "]");
						
						//procura a tabela dentro dessa tr
						WebElement tabelaDocumentosMovimento = null;
						
						List<WebElement> linhas = null;
						
						//pega link para baixar documento
						try {
							tabelaDocumentosMovimento = trDocsMovimentacao.findElement(By.tagName("table"));
							log.debug("Encontrou tabela de documentos: [" + tabelaDocumentosMovimento.getText() + "]");
							linhas = tabelaDocumentosMovimento.findElements(By.tagName("a"));
							log.debug("Encontrou [" + linhas.size() + "] links dentro da tabela...");
							
							if(linhas.size() == 0) {
								log.debug("Não encontrou links dentro da tabela, procurando na tr...");
								try {
									linhas = trDocsMovimentacao.findElements(By.tagName("a"));
									log.debug("Encontrou [" + linhas.size() + "] links dentro da tr...");
								} catch (Exception e) {
								}
							}
						} catch (Exception e) {
							log.debug("Buscando links com xpath alternativo...");
							try {
								linhas = trDocsMovimentacao.findElements(By.tagName("a"));
								log.debug("Encontrou [" + linhas.size() + "] links dentro da tr...");
							} catch (Exception e1) {
								log.error("Erro ao buscar tabela de documentos!");
								CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao encontrar tabela de documentos!");
								e.printStackTrace();
								return false;
							}
						}
						
						/*
						if(linhas == null) {
							log.error("Erro ao pegar documentos do movimento [" + idSeq + "]");
							return null;
						}
						*/
						
						log.debug("Encontrou [" + linhas.size() + "] documento(s) para o movimento [" + sequenciaBaixar + "]");
						
						if(linhas.size() == 0) {
							log.error("Nenhum documento encontrado!");
							if(tabelaDocumentosMovimento != null) {
								CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, tabelaDocumentosMovimento.getText());
							} else {
								CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao encontrar documentos: tabela E/OU links não encontrados");
							}
							
							return false;
						}
						
						//String pathFolderNroProcesso = CopiaIntimacaoConstants.PATH_DOCUMENTOS+"/"+numeroProcesso;
						String pathFolderCodigoAtividade = CopiaIntimacaoConstants.PATH_DOCUMENTOS+"/"+codigoAtividade;					
						try {
							((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", tabelaDocumentosMovimento);
							File print = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
							SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
							String nomePrint = "printDocumentos---" + sdf1.format(new Date());
							String pathArquivoInteiro = pathFolderCodigoAtividade + "/"+nomePrint+".png";
							log.debug("Print salvo em [" + pathArquivoInteiro + "]");
							FileUtils.copyFile(print, new File(pathArquivoInteiro));
						} catch (Exception e) {
							log.error("Erro ao tirar print dos documentos...");
						}
						
						ArrayList<File> arquivosDoProcesso = new ArrayList<File>();
						
						//Random gerador = new Random();
						
						for(int j = 0; j < linhas.size(); j++) {
							fechaToastNotify(driver, wdw1);
							log.debug("Tratando anexo [" + (j+1) + "] de [" + linhas.size() + "]");
							WebElement linkAtual = linhas.get(j);
							String nomeArquivoLink = linkAtual.getText().trim();
							log.debug("Nome do arquivo no link: [" + nomeArquivoLink + "]");
							
							String classes = linkAtual.getAttribute("class");
							log.debug("Class link: [" + classes + "]");
							boolean temClassLink = false;
							for(String c: classes.split(" ")) {
								if(c.equals("link")) {
									temClassLink = true;
								}
							}
							
							if(!temClassLink) {
								log.error("Link não tem classe link, pulando...");
								continue;
							}
							
							//boolean adicionaAnexo = true;
							
							if(!nomeArquivoLink.endsWith(".pdf")) {
								log.debug("Algum arquivo não é do formato .pdf!");
								
								if(nomeArquivoLink.endsWith(".webm")) {
									log.debug("É vídeo! Pulando anexo...");
									continue;
								}
								
								CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro no formato de algum arquivo!");
								return false;
							}
							
							String nomeArquivo = atividade.getCodigo() + "-" + (j+1)+ "-arquivo-"+idDoc+"-"+atividade.getNumeroProcesso().replace(".", "-")+".pdf";
							idDoc++;
							log.debug("Nome do arquivo: [" + nomeArquivo + "]");
							
							String pathCompletoArquivo = pathFolderCodigoAtividade + "/" + nomeArquivo;
							
							//verifica se existe um folder pra esse processo
							File folderAtividade = new File(pathFolderCodigoAtividade);
							if(!folderAtividade.exists() || !folderAtividade.isDirectory()) {
								folderAtividade.mkdir();
							}
							
							String linkArquivo = linkAtual.getAttribute("href");
							
							log.debug("Link: [" + linkArquivo + "]");
							
							//CLICA PARA BAIXAR DOCUMENTO
							linkAtual.click();
							
							log.debug("Esperando 10s baixar o documento...");
							Thread.sleep(10000);
							
							log.debug("Tempo passado!");
							
							//pega o ultimo arquivo na pasta
							File[] arquivosTemp = new File(CopiaIntimacaoConstants.PATH_DOCUMENTOS_TEMP).listFiles();
							File ultimoArquivoMod = null;
							Date ultimoMod = null;
							for(int indArqTempo = 0; indArqTempo < arquivosTemp.length; indArqTempo++) {
								Date dataUltimaMod = new Date(arquivosTemp[indArqTempo].lastModified());
								if(ultimoMod == null || dataUltimaMod.after(ultimoMod)) {
									ultimoArquivoMod = arquivosTemp[indArqTempo];
									ultimoMod = dataUltimaMod;
								}
							}
							
							SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
							if(ultimoArquivoMod == null || ultimoMod == null) {
								log.error("Erro ao pegar arquivo baixado!");
								CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao pegar algum arquivo baixado!");
								return false;
							} else {
								log.debug("Último arquivo modificado: ["+ultimoArquivoMod.getName()+"] - [" + sdf2.format(ultimoMod) + "]");
								log.debug("Tamanho: [" + ultimoArquivoMod.length() + "]");
								
								File arquivoDownload = new File(pathCompletoArquivo);
								
								log.debug("Copiando arquivo para [" + pathCompletoArquivo + "]");
								//copia pra essa pasta
								FileUtils.copyFile(ultimoArquivoMod, arquivoDownload);
								log.debug("Copiou arquivo para [" + pathCompletoArquivo + "] com sucesso!");
								
								arquivosDoProcesso.add(arquivoDownload);
								
								//excluir arquivo
								if(ultimoArquivoMod.delete()) {
									log.debug("Sucesso ao excluir arquivo ["+ultimoArquivoMod.getName() + "] da pasta TEMP!");
								} else {
									log.warn("FALHA ao excluir arquivo ["+ultimoArquivoMod.getName() + "] da pasta TEMP!");
								}
							}
						}
						
						//caso tenha mais de 1 pdf, compila todos em 1 só
						File arquivoAnexarPasta = Utils.concatenarArquivos(arquivosDoProcesso, atividade);
						if(arquivoAnexarPasta == null || !arquivoAnexarPasta.exists()) {
							log.error("Erro no arquivo!");
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro em algum no arquivo!");
							return false;
						} 
						
						arquivosAnexar.add(arquivoAnexarPasta);
						log.debug("Adicionou ["+ arquivoAnexarPasta.getName() + "] na fila de arquivos para adicionar na pasta [" + atividade.getPasta() + "]");
						
						break;
					}
				}
			}
			
			log.debug("Obteve [" + arquivosAnexar.size() + "] arquivos para anexar na pasta [" + atividade.getPasta() + "]");
			
			if(arquivosAnexar.size() > 0) {
				for(File arquivo: arquivosAnexar) {
					if(!WasabiUtils.anexarArquivoLS(atividade.getPasta(), atividade.getCodigo(), arquivo.getName(), arquivo, logCI, true)) {
						log.error("Erro ao subir [" + arquivo.getName() + "] para o LS!");
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao anexar arquivo no LS!");
						return false;
					}
				}
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.SUCESSO_BUSCA_DOCUMENTOS, logCI, "Sucesso ao baixar documentos!");
			} else {
				log.error("Nenhum arquivo para anexar na pasta!");
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Nenhum arquivo para anexar na pasta!");
				return false;
			}
			
			return true;
		} catch (Exception e) {
			log.error("Falha ao buscar intimações para a atividade [" + atividade.getCodigo() + "]!", e);
			CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, e.getMessage());
			return false;
		}
	}

	

	

	

	private static File baixaArquivos(WebDriver driver, String idSeq, WebDriverWait wdw, WebDriverWait wdw1, List<WebElement> movimentacoes, String numeroProcesso,
			LogCopiaIntimacao logCI, Atividade atividade) {
		try {
			int codigoAtividade = atividade.getCodigo();
			
			int soma = 1;
			for(int i = 0; i < ((int) (movimentacoes.size() / 2)); i++) {
				
				//int indTr = ((i*2)+1); //linhas ímpares
				
				//a partir de 07/03/21 passou a ser a cada 3 linhas (ex: 1, 4, 7, 10, ...)
				int indTr = ((i*2)+soma);
				soma++;
				
				log.debug("indTr = [" + indTr + "]");
				
				String xpathAbrirDocumentos    = "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[1]/a";
				String xpathSeqMovimento       = "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[2]";
				String xpathDescricaoMovimento = "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[4]";
				
				String strMovAtual = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathSeqMovimento))).getText().trim();
				
				if(strMovAtual.equals(idSeq)) {
					WebElement descricaoMovimentoReferencia = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathDescricaoMovimento)));
					log.debug("Encontrou movimento de referência de id [" + idSeq + "] - [" + descricaoMovimentoReferencia.getText().replace("\n", "") + "]");
					
					WebElement botaoExpandir = null;
					try {
						botaoExpandir = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathAbrirDocumentos)));
					} catch (Exception e) {
						log.error("Não encontrou botão expandir nessa sequência!");
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Não encontrou botão expandir na seq. referenciada! [" + idSeq + "]");
						return null;
					}
					
					botaoExpandir.click();
					log.debug("Clicou botão expandir da sequência [" + idSeq + "]");
					
					Thread.sleep(5000);
					
					String xpathTrDocsMovimentacao = "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+(indTr+1)+"]";
					
					//verifica se abriu a tr abaixo
					WebElement trDocsMovimentacao = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathTrDocsMovimentacao)));
					log.debug("Encontrou a tr de documentos da movimentação: [" + trDocsMovimentacao.getText() + "]");
					
					//procura a tabela dentro dessa tr
					WebElement tabelaDocumentosMovimento = null;
					
					List<WebElement> linhas = null;
					
					try {
						tabelaDocumentosMovimento = trDocsMovimentacao.findElement(By.tagName("table"));
						log.debug("Encontrou tabela de documentos: [" + tabelaDocumentosMovimento.getText() + "]");
						linhas = tabelaDocumentosMovimento.findElements(By.tagName("a"));
						log.debug("Encontrou [" + linhas.size() + "] links dentro da tabela...");
						
						if(linhas.size() == 0) {
							log.debug("Não encontrou links dentro da tabela, procurando na tr...");
							try {
								linhas = trDocsMovimentacao.findElements(By.tagName("a"));
								log.debug("Encontrou [" + linhas.size() + "] links dentro da tr...");
							} catch (Exception e) {
							}
						}
					} catch (Exception e) {
						log.debug("Buscando links com xpath alternativo...");
						try {
							linhas = trDocsMovimentacao.findElements(By.tagName("a"));
							log.debug("Encontrou [" + linhas.size() + "] links dentro da tr...");
						} catch (Exception e1) {
							log.error("Erro ao buscar tabela de documentos!");
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao encontrar tabela de documentos!");
							e.printStackTrace();
							return null;
						}
					}
					
					/*
					if(linhas == null) {
						log.error("Erro ao pegar documentos do movimento [" + idSeq + "]");
						return null;
					}
					*/
					
					log.debug("Encontrou [" + linhas.size() + "] documento(s) para o movimento [" + idSeq + "]");
					
					if(linhas.size() == 0) {
						log.error("Nenhum documento encontrado!");
						if(tabelaDocumentosMovimento != null) {
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, tabelaDocumentosMovimento.getText());
						} else {
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao encontrar documentos: tabela E/OU links não encontrados");
						}
						
						return null;
					}
					
					//String pathFolderNroProcesso = CopiaIntimacaoConstants.PATH_DOCUMENTOS+"/"+numeroProcesso;
					String pathFolderCodigoAtividade = CopiaIntimacaoConstants.PATH_DOCUMENTOS+"/"+codigoAtividade;					
					try {
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", tabelaDocumentosMovimento);
						File print = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
						String nomePrint = "printDocumentos---" + sdf.format(new Date());
						String pathArquivoInteiro = pathFolderCodigoAtividade + "/"+nomePrint+".png";
						log.debug("Print salvo em [" + pathArquivoInteiro + "]");
						FileUtils.copyFile(print, new File(pathArquivoInteiro));
					} catch (Exception e) {
						log.error("Erro ao tirar print dos documentos...");
					}
					
					ArrayList<File> arquivosDoProcesso = new ArrayList<File>();
					
					//Random gerador = new Random();
					
					for(int j = 0; j < linhas.size(); j++) {
						log.debug("Tratando anexo [" + (j+1) + "] de [" + linhas.size() + "]");
						WebElement linkAtual = linhas.get(j);
						String nomeArquivoLink = linkAtual.getText().trim();
						log.debug("Nome do arquivo no link: [" + nomeArquivoLink + "]");
						
						String classes = linkAtual.getAttribute("class");
						log.debug("Class link: [" + classes + "]");
						boolean temClassLink = false;
						for(String c: classes.split(" ")) {
							if(c.equals("link")) {
								temClassLink = true;
							}
						}
						
						if(!temClassLink) {
							log.error("Link não tem classe link, pulando...");
							continue;
						}
						
						//boolean adicionaAnexo = true;
						
						if(!nomeArquivoLink.endsWith(".pdf")) {
							log.debug("Algum arquivo não é do formato .pdf!");
							
							if(nomeArquivoLink.endsWith(".webm")) {
								log.debug("É vídeo! Pulando anexo...");
								continue;
							}
							
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro no formato de algum arquivo!");
							return null;
						}
						
						//String nomeArquivo = gerador.nextInt()+"-arquivo-"+numeroProcesso.replace(".", "-")+".pdf";
						String nomeArquivo = atividade.getCodigo() + "-" + (j+1)+ "-arquivo-"+numeroProcesso.replace(".", "-")+".pdf";
						log.debug("Nome do arquivo: [" + nomeArquivo + "]");
						
						
						String pathCompletoArquivo = pathFolderCodigoAtividade + "/" + nomeArquivo;
						
						//verifica se existe um folder pra esse processo
						File folderAtividade = new File(pathFolderCodigoAtividade);
						if(!folderAtividade.exists() || !folderAtividade.isDirectory()) {
							folderAtividade.mkdir();
						}
						
						String linkArquivo = linkAtual.getAttribute("href");
						
						log.debug("Link: [" + linkArquivo + "]");
						
						//CLICA PARA BAIXAR DOCUMENTO
						linkAtual.click();
						
						Thread.sleep(7000);
						
						log.debug("Baixou arquivo!");
						
						//pega o ultimo arquivo na pasta
						File[] arquivosTemp = new File(CopiaIntimacaoConstants.PATH_DOCUMENTOS_TEMP).listFiles();
						File ultimoArquivoMod = null;
						Date ultimoMod = null;
						for(int indArqTempo = 0; indArqTempo < arquivosTemp.length; indArqTempo++) {
							Date dataUltimaMod = new Date(arquivosTemp[indArqTempo].lastModified());
							if(ultimoMod == null || dataUltimaMod.after(ultimoMod)) {
								ultimoArquivoMod = arquivosTemp[indArqTempo];
								ultimoMod = dataUltimaMod;
							}
						}
						
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
						if(ultimoArquivoMod == null || ultimoMod == null) {
							log.error("Erro ao pegar arquivo baixado!");
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao pegar algum arquivo baixado!");
							return null;
						} else {
							log.debug("Último arquivo modificado: ["+ultimoArquivoMod.getName()+"] - [" + sdf.format(ultimoMod) + "]");
							log.debug("Tamanho: [" + ultimoArquivoMod.length() + "]");
							
							File arquivoDownload = new File(pathCompletoArquivo);
							
							log.debug("Copiando arquivo para [" + pathCompletoArquivo + "]");
							//copia pra essa pasta
							FileUtils.copyFile(ultimoArquivoMod, arquivoDownload);
							log.debug("Copiou arquivo para [" + pathCompletoArquivo + "] com sucesso!");
							
							arquivosDoProcesso.add(arquivoDownload);
							//arquivosDoProcesso.add(arquivoDownload); //teste compilação de pdfs
							
							//excluir arquivo
							if(ultimoArquivoMod.delete()) {
								log.debug("Sucesso ao excluir arquivo ["+ultimoArquivoMod.getName() + "] da pasta TEMP!");
							} else {
								log.warn("FALHA ao excluir arquivo ["+ultimoArquivoMod.getName() + "] da pasta TEMP!");
							}
						}
					}
					
					//caso tenha mais de 1 pdf, compila todos em 1 só
					File arquivoAnexarPasta = Utils.concatenarArquivos(arquivosDoProcesso, atividade);
					if(arquivoAnexarPasta == null || !arquivoAnexarPasta.exists()) {
						log.error("Erro no arquivo!");
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro no arquivo!");
						return null;
					} else {
						return arquivoAnexarPasta;
					}
					
					//break;
				}
			}
		} catch (Exception e) {
			log.error("Erro ao baixar arquivos do processo ["+numeroProcesso+"]", e);
			try {
				File print = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
				String nomePrint = "erroBaixarDocumentos---" + sdf.format(new Date());
				String pathArquivoInteiro = CopiaIntimacaoConstants.PATH_DOCUMENTOS+"/"+numeroProcesso + "/"+nomePrint+".png";
				log.debug("Print salvo em [" + pathArquivoInteiro + "]");
				FileUtils.copyFile(print, new File(pathArquivoInteiro));
			} catch (Exception e2) {
				
			}
			CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao baixar arquivos do processo: [" + e.getMessage() + "]");
			return null;
		}
		return null;
	}

	/**
	 * Busca o último movimento 'CONFIRMADA A INTIMAÇÃO ELETRÔNICA' ou 'EXPEDIÇÃO DE INTIMAÇÃO' e retorna a referência. 
	 * Ex: ' [184]: EXPEDIÇÃO DE INTIMAÇÃO Para advogados/curador/defensor de BRASILSEG COMPANHIA DE SEGUROS com prazo de 10 
	 * 			dias úteis - Referente ao evento (seq. 181) CONCEDIDO O PEDIDO (19/08/2020) '
	 * Retorna o id da seq ( neste caso 181)
	 * 
	 * @param wdw1
	 * @param movimentacoes
	 * @param numeroProcesso
	 * @param logCI
	 * @return
	 */
	private static String buscaIntimacaoEReferencia(WebDriver driver, WebDriverWait wdw1, List<WebElement> movimentacoes, String numeroProcesso, String instancia, Atividade atividade, boolean ehProcuraOutraInstancia, LogCopiaIntimacao logCI) {
		try {
			String idSeq = null;
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date dataCompPubAtividade = sdf.parse(atividade.getDataPublicacao());
			
			log.debug("[" + ((int) (movimentacoes.size() / 2)) + "] movimentações encontradas!");
			
			boolean encontrouCopia = false;
			
			//verifica se tem mais de uma atividade de publicacao em andamento nessa pasta
			ArrayList<Integer> atividadesMesmaPasta = AtividadesBO.temMultiplasAtividades(atividade.getCodigo(), atividade.getPasta());
			ArrayList<String> sequenciasProibidas = new ArrayList<String>();
			if(atividadesMesmaPasta != null && atividadesMesmaPasta.size() > 0) {
				log.debug("A pasta [" + atividade.getPasta() + "] tem as atividades " + atividadesMesmaPasta + " além da [" + atividade.getCodigo() + "]");
				sequenciasProibidas = CopiaIntimacaoBO.buscaSequenciasProibidas(atividadesMesmaPasta);
				log.debug("Sequencias proibidas -> " + sequenciasProibidas);
			}
			
			log.debug("Movimentos:");
			int soma = 1;
			for(int i = 0; i < ((int) (movimentacoes.size() / 2)); i++) {
				
				//int indTr = ((i*2)+1); //linhas ímpares
				
				//a partir de 07/03/21 passou a ser a cada 3 linhas (ex: 1, 4, 7, 10, ...)
				int indTr = ((i*2)+soma);
				soma++;
				
				log.debug("indTr = [" + indTr + "]");
				
				
				String xpathNomeMovimento =        "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[4]/b";
				String xpathDataMovimento =		   "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[3]";
				String xpathNomeMovimentoStrike =  "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[4]/strike/b";
				String xpathDescricaoMovimento =   "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[4]";
				String xpathSeqMovimento =         "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[2]";
				String xpathMovPor = 			   "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr["+indTr+"]/td[5]";
				
				//secundário                 
				String xpathNomeMovimento2 =       "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[4]/b";
				String xpathDataMovimento2 =	   "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[3]";
				String xpathNomeMovimentoStrike2 = "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[4]/strike/b";
				String xpathDescricaoMovimento2 =  "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[4]";
				String xpathSeqMovimento2 =        "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[2]";
				String xpathMovPor2 =              "/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div[3]/div/div/table/tbody/tr["+indTr+"]/td[5]";
				
				WebElement nomeMov = null;
				WebElement dataMov = null;
				WebElement descricaoMov = null;
				WebElement seqMov = null;
				
				//busca seq do movimento com xpaths primário e secundário
				try {
					seqMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathSeqMovimento)));
				} catch (Exception esm) {
					log.debug("Não encontrou seq. do movimento com xpath padrão... Buscando com alternativo");
					try {
						seqMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathSeqMovimento2))); 
					} catch (Exception esm1) {
						log.error("Erro ao encontrar seq. do movimento com xpath primário e secundário!", esm1);
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a seq. de algum movimento!");
						return null;
					}
				}
				
				int sequenciaMovimento = -1;
				try{
					sequenciaMovimento = Integer.parseInt(seqMov.getText().trim());
				} catch (Exception csm) {
					log.debug("Não conseguiu converter encontrou seq. do movimento no xpath primário! Buscando com secundário...");
					try {
						sequenciaMovimento = Integer.parseInt(wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathSeqMovimento2))).getText().trim());
						log.debug("Sucesso ao encontrar seq. do movimento com xpath secundário!");
					} catch (Exception csm1) {
						log.error("Erro ao encontrar seq. do movimento com xpath primário e secundário!", csm1);
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a seq. de algum movimento!");
						return null;
					}
				}
				
				String dataHoraMovimento = "";
				String dataMovimento = "";
				try {
					dataMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathDataMovimento)));
					dataHoraMovimento = dataMov.getText().trim();
					if(dataHoraMovimento.equals("")) {
						throw new Exception("data do movimento vazia...");
					}
				} catch (Exception edm) {
					log.debug("Não encontrou data do movimento com xpath primário... Buscando com secundário...");
					try {
						dataMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathDataMovimento2)));
						dataHoraMovimento = dataMov.getText().trim();
						log.debug("Encontrou data do movimento com xpath secundário!");
					} catch (Exception edm1) {
						log.error("Erro ao pegar data do movimento!", edm1);
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a data de algum movimento!");
						return null;
					}
				}
				
				if(dataMov == null || dataHoraMovimento.equals("")) {
					log.error("Erro ao pegar data do movimento!");
					Thread.sleep(15000);
					CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a data de algum movimento!");
					return null;
				}
				
				
				log.debug("dataHoraMovimento: [" + dataHoraMovimento + "]");
				
				//PEGA APENAS A DATA DO MOVIMENTO NO FORMATO dd/mm/yyyy
				String[] dataHora = dataHoraMovimento.split(" ");
				if(dataHora.length < 2) {
					log.error("Erro ao pegar data do movimento!");
					CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a data de algum movimento!");
					return null;
				}
				
				dataMovimento = dataHora[0];
				
				String nomeMovimento = "";
				
				try {
					nomeMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathNomeMovimento)));
					nomeMovimento = nomeMov.getText();
				} catch (Exception enm) {
					log.debug("Não encontrou nome do movimento com xpath padrão... Buscando com alternativo...");
					try {
						nomeMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathNomeMovimento2)));
						nomeMovimento = nomeMov.getText();
					} catch (Exception enm1) {
						log.debug("Não encontrou nome do movimento com xpath secundário... Buscando com xpath STRIKE primário...");
						try {
							nomeMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathNomeMovimentoStrike)));
							nomeMovimento = nomeMov.getText();
							log.debug("["+sequenciaMovimento+"] - " + nomeMovimento);
							log.debug("Movimento com STRIKE, não considerando...");
							continue;
						} catch (Exception enms1) {
							log.debug("Não encontrou nome do movimento com xpath STRIKE primário... Buscando com alternativo...");
							try {
								nomeMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathNomeMovimentoStrike2)));
								nomeMovimento = nomeMov.getText();
								log.debug("["+sequenciaMovimento+"] - " + nomeMovimento);
								log.debug("Movimento com STRIKE, não considerando...");
								continue;
							} catch (Exception enms2) {
								log.error("Erro ao encontrar seq. do movimento com xpath primário e secundário!", enms2);
								CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar o nome de algum movimento!");
								return null;
							}
						}
					}
				}
					
				String descricaoMovimento = "";
				try {
					descricaoMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathDescricaoMovimento)));
					descricaoMovimento = descricaoMov.getText().trim();
					if(descricaoMovimento.equals("")) {
						throw new Exception("descricao do movimento vazia...");
					}
				} catch (Exception edm) {
					log.debug("Erro ao encontrar descrição do movimento com o xpath primário, buscando com secundário...");
					try {
						descricaoMov = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathDescricaoMovimento2)));
						descricaoMovimento = descricaoMov.getText().trim();
					} catch (Exception edm1) {
						log.error("Erro ao encontrar descrição do movimento com xpaths primário e secundário!", edm1);
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a descrição de algum movimento!");
						return null;
					}
				}
				
				descricaoMovimento = descricaoMovimento.replace("\n", "").trim();
				
				String movimentadoPor = "";
				try {
					movimentadoPor = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathMovPor))).getText(); 
				} catch (Exception emp) {
					log.debug("Erro ao encontrar movimentado por do movimento com o xpath primário, buscando com secundário...");
					try {
						movimentadoPor = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathMovPor2))).getText();
					} catch (Exception emp1) {
						log.error("Erro ao encontrar movimentado por do movimento com xpaths primário e secundário!", emp1);
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a movimentado por algum movimento!");
						return null;
					}
				}
				
				if(movimentadoPor != null) {
					movimentadoPor = movimentadoPor.trim();
				}
				
				fechaToastNotify(driver, wdw1);
					
				if(nomeMovimento.trim().equals("CONFIRMADA A INTIMAÇÃO ELETRÔNICA") || nomeMovimento.trim().equals("EXPEDIÇÃO DE INTIMAÇÃO")) {
					log.debug("[" + sequenciaMovimento + "] - [" + dataMovimento + "] - [" + nomeMovimento + "] - [" + descricaoMovimento + "] - [" + movimentadoPor + "]");
					log.debug("Encontrou uma confirmação/expedição de intimação! Verificando a data...");
					
					log.debug("Data movimento: [" + dataMovimento + "] - data atividade: [" + atividade.getDataPublicacao() + "]");
					
					if(!dataMovimento.equals(atividade.getDataPublicacao())) {
						
						log.debug("Não é a mesma data, buscando a data da confirmação na descrição do movimento...");
						if(!descricaoMovimento.equals("")) {
							int indexDe = descricaoMovimento.indexOf(" em ");
							if(indexDe != -1) {
								String dataDescricao = descricaoMovimento.substring(indexDe);
								dataDescricao = dataDescricao.trim().replace("em", "").trim();
								log.debug("data descrição (filtrando): [" + dataDescricao + "]");
								
								int indexPrimeiroEspacoPosData = dataDescricao.indexOf(" ");
								if(indexPrimeiroEspacoPosData != -1) {
									dataDescricao = dataDescricao.substring(0, indexPrimeiroEspacoPosData).trim();
									log.debug("data descrição (filtrando): [" + dataDescricao + "]");
									
									log.debug("Data na descrição do movimento: [" + dataDescricao + "]");
									
									if(!dataDescricao.equals(atividade.getDataPublicacao())) {
										log.debug("A data da descrição [" + dataDescricao +"] não é a mesma da atividade [" + atividade.getDataPublicacao() + "]! Continuando para próximo movimento...");
										
										Date dataMovimentoComparacao = sdf.parse(dataMovimento);
										//para a busca caso estiver estiver olhando para uma movimentacao com data anterior a data da publicação na atividade
										if(dataMovimentoComparacao.before(dataCompPubAtividade)) {
											log.error("Data do movimento na descrição ["+dataMovimento+"] é anterior à data da publicação na atividade ["+atividade.getDataPublicacao()+"]... Finalizando busca...");
											
											//CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a intimação: datas não compatíveis!");
											//return null;
											
											//procuraOutraInstancia = true;
											//break;
											
											return "outrainstancia";
										}
										
										continue;
									}
									
								} else {
									log.debug("Falha ao encontrar data na descrição do movimento...");
									continue;
								}
							} else {
								log.debug("Falha ao encontrar data na descrição do movimento...");
								continue;
							}
						} else {
							log.error("Erro ao encontrar descrição do movimento!");
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a descrição de algum movimento!");
							return null;
						}
						
						//log.debug("NÃO é a mesma data! Continuando para o próximo movimento...");
						//continue;
					}
					
					//A PARTIR DE 03/2022 A BUSCA É FEITA PELO 'JAIME OLIVEIRA PENTEADO' NA COLUNA 'MOVIMENTADO POR'
					log.debug("É a mesma data, verificando movimentado por...");
					if(!movimentadoPor.equals("")) {
						if(!movimentadoPor.toUpperCase().contains("JAIME OLIVEIRA PENTEADO")) {
							log.debug("O movimentador ñ é o Jaime! : ["+movimentadoPor+"]! Pulando...");
							continue;
						}
						log.debug("É o Jaime!");
					} else {
						log.error("Erro ao encontrar movimentado por do movimento!");
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar o movimentado por de algum movimento!");
						return null;
					}
					
					/*
					
					log.debug("É a mesma data, verificando cliente...");
					
					//verifica se tem o nome do cliente nessa descrição de movimento
					String nomeParte = null;
					String[] iniciosNomeParte = {"defensor de ", "Para Perito "};
					if(!descricaoMovimento.equals("")) {
						int indexDefensor = descricaoMovimento.indexOf(iniciosNomeParte[0]);
						
						int indINP = 1;
						
						while(indINP < iniciosNomeParte.length && indexDefensor == -1) {
							indexDefensor = descricaoMovimento.indexOf(iniciosNomeParte[indINP]);
							log.debug("Encontrou [" + iniciosNomeParte[indINP] + "] ...");
							indINP++;
						}
						
						
						if(indexDefensor == -1) {
							log.error("Falha ao encontrar parte do movimento na descrição!");
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a parte na descrição de algum movimento!");
							return null;
						}
						
						String stringDefensor = descricaoMovimento.substring(indexDefensor);
						for(int ir = 0; ir < iniciosNomeParte.length; ir++) {
							stringDefensor = stringDefensor.replace(iniciosNomeParte[ir], "");
						}
						
						
						String[] palavrasFimNomeParte = {" com ", " em ", " representado(a) ", " - "};
						
						int indP = 0, indexFimNomeParte = -1;
						
						while(indP < palavrasFimNomeParte.length) {
							int indexAtualPalavra =  stringDefensor.indexOf(palavrasFimNomeParte[indP]);
							if(indexAtualPalavra != -1 && indexFimNomeParte == -1) {
								indexFimNomeParte = indexAtualPalavra;
							} else if(indexAtualPalavra != -1 && indexAtualPalavra < indexFimNomeParte) {
								indexFimNomeParte = indexAtualPalavra;
							}
							
							indP++;
						}
						
						if(indexFimNomeParte == -1) {
							log.error("Não encontrou o final do nome da parte!");
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a parte na descrição de algum movimento!");
							return null;
						}
						
						stringDefensor = stringDefensor.substring(0, indexFimNomeParte);
						stringDefensor = stringDefensor.replace("(", "").replace(")", "").trim();
						
						
						nomeParte = stringDefensor;
						log.debug("Parte movimento: [" + nomeParte + "]");
						
						if(!Utils.nomeParteVerificado(nomeParte, atividade)) {
							log.error("Não encontrou o cliente na descrição!");
							continue;
						}
						
						Thread.sleep(5000);
					} else {
						log.error("Erro ao encontrar descrição do movimento!");
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a descrição de algum movimento!");
						return null;
					}
					*/
					
					
					log.debug("Encontrou intimação correta! Pegando descrição...");
					//String descricaoMovimento = wdw1.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathDescricaoMovimento))).getText();
					descricaoMovimento = descricaoMovimento.replace("CONFIRMADA A INTIMAÇÃO ELETRÔNICA", "").replace("EXPEDIÇÃO DE INTIMAÇÃO", "").trim();
					log.debug("Encontrou a última intimação na sequência ["+sequenciaMovimento+"]: " + descricaoMovimento);	
					
					//busca sequencia que essa confirmação se refere
					int indexReferente = descricaoMovimento.indexOf("*Referente");
					if(indexReferente == -1) {
						indexReferente = descricaoMovimento.indexOf("Referente ao evento");
						if(indexReferente == -1) {
							log.error("Erro ao buscar a referência da confirmação de intimação...");
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a referência da confirmação de intimação!");
							return null;
						}
					}
						
					String referenciaCompleta = descricaoMovimento.substring(indexReferente);
						
					int indexParentesesInicio = referenciaCompleta.indexOf("(");
					int indexParentesesFinal = referenciaCompleta.indexOf(")");
						
					if(indexParentesesInicio == -1 || indexParentesesFinal == -1) {
						log.error("Erro ao buscar a referência da confirmação de intimação...");
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a referência da confirmação de intimação!");
						return null;
					}
						
					String sequenciaCompleta = referenciaCompleta.substring(indexParentesesInicio, indexParentesesFinal);
					log.debug("Sequência completa: [" + sequenciaCompleta + "]");
						
					idSeq = sequenciaCompleta.replace("(", "").replace(")", "").replace("seq.","").trim();
					log.debug("idSeq: [" + idSeq + "]");
					
					//verifica se essa seq não é proibida
					boolean proibida = false;
					if(sequenciasProibidas != null && sequenciasProibidas.size() > 0) {
						for(String sequencia: sequenciasProibidas) {
							log.debug("Verificando a sequência encontrada [" + sequenciaMovimento + "] x [" + sequencia + "]!");
							if(sequencia != null && sequencia.trim().equals(String.valueOf(sequenciaMovimento))) {
								log.debug("Essa sequência é proibida, procurando outra...");
								proibida = true;
							}
						}
					}
						
					if(!proibida) {
						logCI.setReferencia(String.valueOf(sequenciaMovimento));
						encontrouCopia = true;
						break;
					}
					
				} else {
					log.debug("[" + sequenciaMovimento + "] - [" + dataMovimento + "] - [" + nomeMovimento + "]!");
					
					//para a busca se passou mais de 14 dias da data da publicação da atividade
					LocalDate ldAtividade = dataCompPubAtividade.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					Date dataMovimentoComparacao = sdf.parse(dataMovimento);
					LocalDate ldMovimento = dataMovimentoComparacao.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					
					if(ldMovimento.until(ldAtividade, ChronoUnit.DAYS) >= 14) {
						log.error("A data do movimento [" + ldMovimento + "] é muito antiga para a data da atividade [" + ldAtividade + "]! Parando busca...");
						
						
						//CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a intimação: datas não compatíveis!");
						//return null;
						
						//procuraOutraInstancia = true;
						//break;
						return "outrainstancia";
					}
				}
			}
			
			if(!encontrouCopia) {
				log.error("Falha ao encontrar intimação! Verifique se o movimento existe!");
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao encontrar intimação! Verifique se o movimento existe!");
				return null;
			}
				
			if(idSeq == null) {
				log.error("Falha ao encontrar copia! idSeq = null");
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao encontrar copia! idSeq = null");
				return null;
			}
			
			return idSeq;
		} catch (Exception e) {
			log.error("Erro ao buscar as movimentações do processo [" + numeroProcesso + "]", e);
			CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar as movimentações do processo: [" + e.getMessage() + "]");
			return null;
		}
	}

	private static String procurarNoProcessoOriginal(WebDriver driver, WebDriverWait wdw1, Atividade atividade, LogCopiaIntimacao logCI) {
		try {
			
			String instancia = atividade.getGrauProcesso();
			String numeroProcesso = atividade.getNumeroProcesso();
			
			log.debug("Procurando 'Árvore Processual'...");
			
			//pega todas as linhas e procura a 'Árvore Processual'
			List<WebElement> linhasRecurso = wdw1.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("/html/body/div[1]/div[2]/form/fieldset/table[1]/tbody/tr")));
			log.debug("Obteve [" + linhasRecurso.size() + "] linhas de dados do processo... Procurando 'Árvore Processual'");
			
			boolean entrouNoLink = false;
			for(WebElement linha: linhasRecurso) {
				if(linha.getText().trim().contains("Árvore Processual")){
					log.debug(linha.getText());
					List<WebElement> links = linha.findElements(By.tagName("a"));
					
					for(WebElement link: links) {
						log.debug("Link: [" + link.getText().trim() + "]");
						if(link.getText().trim().contains("Processo: ")) {
							log.debug("Encontrou processo! Clicando...");
							
							link.click();
							entrouNoLink = true;
							break;
						}
					}
					
					if(entrouNoLink) {
						break;
					}
				}
			}
			
			log.debug("Esperando abrir o processo...");
			Thread.sleep(3000);
			
			WebElement tituloMudouProcesso = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div[2]/form/h3")));
			
			if(!tituloMudouProcesso.getText().trim().contains("Processo")) {
				log.error("Falha ao abrir recurso! Não encontrou 'Processo' no título!");
				return null;
			}
			
			log.debug("Abriu processo! Pegando botões...");
																													 
			List<WebElement> botoesProcesso = wdw1.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[1]/td/div/ul/li")));
			log.debug("Encontrou [" + botoesProcesso.size() + "] botões! Procurando movimentações...");
			
			log.debug("Abriu movimentações...");
			
			//pega movimentações																					/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr[1]
			List<WebElement> movimentacoes = wdw1.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr")));
			if(movimentacoes.size() == 0) {
				log.error("Nenhuma movimentação encontrada!");
				//CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Nenhuma movimentação encontrada!");
				return null;
			} else {
				log.debug("Encontrou [" + movimentacoes.size() + "] movimentações!");
			}
			
			String idSeqProcesso = buscaIntimacaoEReferencia(driver, wdw1, movimentacoes, numeroProcesso, instancia, atividade, true, logCI);
			log.debug("idSeqProcesso: [" + idSeqProcesso + "]");
			if(idSeqProcesso == null || idSeqProcesso.equals("outrainstancia")) {
				return null;
			}
			
			return idSeqProcesso;
			
			
		} catch (Exception e) {
			log.error("Erro ao procurar intimação no processo original!", e);
			return null;
		}
	}

	private static String procurarNosRecursos(WebDriver driver, WebDriverWait wdw1, WebElement btMostrarRecursos, Atividade atividade, LogCopiaIntimacao logCI) {
		try {
			
			String instancia = atividade.getGrauProcesso();
			String numeroProcesso = atividade.getNumeroProcesso();
			
			btMostrarRecursos.click();
			log.debug("Clicou no botão mostrar recursos...");
			
			//pega todas as linhas e procura a 'Árvore Processual'
			List<WebElement> linhasRecurso = wdw1.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("/html/body/div[1]/div[2]/form/fieldset/table[1]/tbody/tr")));
			log.debug("Obteve [" + linhasRecurso.size() + "] linhas de dados do processo... Procurando 'Árvore Processual'");
			
			ArrayList<String> nomesRecursos = new ArrayList<String>();
			
			for(WebElement linha: linhasRecurso) {
				if(linha.getText().trim().contains("Árvore Processual")){
					log.debug(linha.getText());
					List<WebElement> links = linha.findElements(By.tagName("a"));
					
					for(WebElement link: links) {
						String nomeLink = link.getText().trim();
						log.debug("Link: [" + nomeLink + "]");
						if(nomeLink.contains("Recurso: ")) {
							nomesRecursos.add(nomeLink);
						}
					}
				}
			}
			
			for(String nomeRecurso: nomesRecursos) {
				WebElement linkRecurso = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(nomeRecurso)));
				log.debug("Encontrou recurso: ["+nomeRecurso+"]! Clicando...");
				
				linkRecurso.click();
				
				log.debug("Esperando abrir o recurso...");
				Thread.sleep(3000);
				
				WebElement tituloMudouRecurso = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div[2]/form/h3")));
				
				if(!tituloMudouRecurso.getText().trim().contains("Recurso")) {
					log.error("Falha ao abrir recurso! Não encontrou 'Recurso' no título!");
					return null;
				}
				
				log.debug("Abriu recurso ["+nomeRecurso+"]! Pegando botões...");
				
				List<WebElement> botoesRecurso = wdw1.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[1]/td/div/ul/li")));
				log.debug("Encontrou [" + botoesRecurso.size() + "] botões! Procurando movimentações...");
				
				boolean btMov = false;
				for(WebElement botao: botoesRecurso) {
					if(botao.getText().trim().equals("Movimentações")) {
						log.debug("Encontrou botão movimentações! Clicando...");
						btMov = true;
						botao.click();
					}
				}
				
				if(!btMov) {
					log.error("Falha ao encontrar botão movimentações...");
					return null;
				}
				
				Thread.sleep(3000);
				
				//pega movimentações
				List<WebElement> movimentacoes = wdw1.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/table/tbody/tr")));
				if(movimentacoes.size() == 0) {
					log.error("Nenhuma movimentação encontrada!");
					//CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Nenhuma movimentação encontrada!");
					return null;
				} 
				
				log.debug("Buscando intimação nesse recurso...");
				
				String intimacaoRecursoAtual = buscaIntimacaoEReferencia(driver, wdw1, movimentacoes, numeroProcesso, instancia, atividade, true, logCI);
				log.debug("intimacaoRecursoAtual: [" + intimacaoRecursoAtual + "]");
				if(intimacaoRecursoAtual != null && !intimacaoRecursoAtual.equals("outrainstancia")) {
					return intimacaoRecursoAtual;
				} else {
					log.debug("Falha ao encontrar intimação no recurso: [" + nomeRecurso + "]");
				}
			}
		} catch (Exception e) {
			log.error("Erro ao procurar intimação no recurso!", e);
			return null;
		}
		return null;
	}

	private static int pegaQuantidadeMovimentacoes(WebDriverWait wdw, WebDriverWait wdw1, String numeroProcesso, String instancia, LogCopiaIntimacao logCI) {
		try {
			WebElement mensagemQtdMovimentacoes = null;
			
			Thread.sleep(5000);
			
			try {
				mensagemQtdMovimentacoes = wdw.until(ExpectedConditions.presenceOfElementLocated(
						By.xpath("/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/div[2]/div[2]")));
				                  
			} catch (Exception emqm) {
				log.debug("Não encontrou mensagem com quantidade de movimentações no xpath primário, buscando com secundário...");
				try {
					mensagemQtdMovimentacoes = wdw.until(ExpectedConditions.presenceOfElementLocated(
							By.xpath("/html/body/div[1]/div[2]/form/fieldset/table[4]/tbody/tr[2]/td/div[3]/div/div/div[2]/div[2]")));
					
				} catch (Exception emqm1) {
					log.error("Erro ao recuperar quantidade de movimentações!", emqm1);
					CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao buscar a quantidade de movimentações do processo!");
					return -1;
				}
			}
			
			String strMensagemQtdMovimentacoes = mensagemQtdMovimentacoes.getText();
			log.debug("Mensagem quantidade de movimentações: " + strMensagemQtdMovimentacoes);
			
			
			int indexRegistros = strMensagemQtdMovimentacoes.indexOf("registro(s)");
			if(indexRegistros == -1) {
				log.debug("Falha ao recuperar quantidade de movimentações do processo [" + numeroProcesso + "]!");
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao pegar quantidade de movimentações: [não encontrou 'registro(s)']!");
				return -1;
			}
			
			String strQuantidadeRegistros = strMensagemQtdMovimentacoes.substring(0, indexRegistros);
			int quantidadeMovimentacoes = Integer.parseInt(strQuantidadeRegistros.trim());
			
			log.debug("Quantidade de movimentações encontradas: [" + quantidadeMovimentacoes + "]");
			
			/*
			if(quantidadeMovimentacoes > 1000) {
				log.error("Mais de 1000 movimentações!");
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Mais de 1000 movimentações!");
				return -1;
			}
			*/
			
			if(quantidadeMovimentacoes > 499) {
				log.debug("Mais de 499 movimentações encontradas, mudando select pra 1000 movimentações por página...");
				
				WebElement select = null;
				try {
					select = wdw1.until(ExpectedConditions.presenceOfElementLocated(By
							.xpath("/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[2]/td/div/div/div/div[2]/div[1]/select")));
				} catch (Exception es) {
					log.debug("Erro ao pegar select de quantidade de movimentações com xpath primário, tentando com secundário...");
					try {
						select = wdw1.until(ExpectedConditions.presenceOfElementLocated(By
								.xpath("/html/body/div[1]/div[2]/form/fieldset/table[4]/tbody/tr[2]/td/div[3]/div/div/div[2]/div[1]/select")));
					} catch (Exception es1) {
						log.error("Erro ao pegar select de quantidade de movimentações com xpath primário e secundário...", es1);
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao pegar movimentações");
						return -1;
					}
				}
				
				//clica no select pra ir para 1000 por pág
				Select selectQtdPag = new Select(select);
				selectQtdPag.selectByValue("1000");
				Thread.sleep(5000);
			}
			
			return quantidadeMovimentacoes;
		} catch (Exception eqm) {
			log.error("Erro ao encontrar quantidade de movimentações:", eqm);
			CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao pegar quantidade de movimentações: ["+eqm.getMessage()+"]!");
			return -1;
		}
	}

	private static boolean abreProcesso(WebDriver driver, WebDriverWait wdw, WebDriverWait wdw1, WebDriverWait wdw2, String numeroProcesso, String instancia, LogCopiaIntimacao logCI) {
		try {
			fechaToastNotify(driver, wdw1);
			WebElement botaoBuscas = wdw.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div/nav/ul/li[8]/a")));
			botaoBuscas.click();
			log.debug("Clicou no botão Buscas");
			
			//percurso 1º grau x 2º grau
			if(instancia.equals("1º Grau")) {
				WebElement busca1grau = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div/nav/ul/li[8]/ul/li[1]/a")));
				busca1grau.click();
				log.debug("Clicou botão busca 1º grau");
				
				//muda para userMainFrame
				wdw.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("/html/body/div[2]/iframe")));
				log.debug("Mudou para userMainFrame");
				frameAtual = "userMainFrame";
				
				WebElement btQualquerProcesso = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"buscaProcessosQualquerInstanciaForm\"]/fieldset/table/tbody/tr[2]/td[2]/input[2]")));
				if(!btQualquerProcesso.isSelected()) {
					btQualquerProcesso.click();
					log.debug("Clicou em qualquer processo");
				} else {
					log.debug("Qualquer processo já estava selecionado");
				}
				
				//preenche numeroProcesso
				WebElement campoNumeroProcesso = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"numeroProcesso\"]")));
				preencheCampo(campoNumeroProcesso, numeroProcesso);
				log.debug("Preenche campo nroProcesso: " + numeroProcesso);
				
			} else if (instancia.equals("2º Grau")) {
				WebElement busca2grau = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div/nav/ul/li[8]/ul/li[2]/a")));
				busca2grau.click();
				log.debug("Clicou botão busca 2º grau");
				
				//muda para userMainFrame
				wdw.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("/html/body/div[2]/iframe")));
				log.debug("Mudou para userMainFrame");
				frameAtual = "userMainFrame";
				
				WebElement btQualquerProcesso = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"buscaProcessosQualquerInstanciaForm\"]/fieldset/table/tbody/tr[1]/td[2]/input[2]")));
				if(!btQualquerProcesso.isSelected()) {
					btQualquerProcesso.click();
					log.debug("Clicou em qualquer processo");
				} else {
					log.debug("Qualquer processo já estava selecionado");
				}
				
				//preenche numeroRecurso
				WebElement campoNumeroRecurso = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"numeroRecurso\"]")));
				preencheCampo(campoNumeroRecurso, numeroProcesso);
				log.debug("Preenche campo nroRecurso: " + numeroProcesso);
			} else {
				log.error("instancia não correta: " + instancia);
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "A instância não é correta: ["+instancia+"]!");
				return false;
			}
			
			WebElement btPesquisar = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"pesquisar\"]")));
			btPesquisar.click();
			log.debug("Clicou no botão pesquisar!");
			
			//verifica se apareceu a div 'errorMessages'
			try {
				WebElement divErro = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"errorMessages\"]")));
				log.error("Div erro encontrada: [" + divErro.getText() + "]");
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, divErro.getText());
				return false;
			} catch (Exception e1) {
				log.debug("Div erro não encontrada!");
			}
			
			//tenta encontrar o numero do processo
			try {
				WebElement nroProcessoPagina = null;
				
				//se for em segundo grau procura de baixo pra cima 
				if (instancia.equals("2º Grau")) {
					log.debug("É 2º grau! Abrindo o último processo da lista...");
					
					List<WebElement> linhasProcessos = wdw1.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("/html/body/div[1]/div[2]/form/table[2]/tbody/tr")));
					log.debug("Encontrou [" + linhasProcessos.size() + "] linhas de processo! Abrindo o último...");
					
					String xpathUltimoLink = "/html/body/div[1]/div[2]/form/table[2]/tbody/tr["+linhasProcessos.size()+"]/td[2]/a";
					nroProcessoPagina = wdw2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathUltimoLink)));
				} else {
					//se for primeiro grau, abre o primeiro link
					nroProcessoPagina = wdw2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div[2]/form/table[2]/tbody/tr/td[2]/a")));
				}
				
				log.debug("Encontrou processo: " + nroProcessoPagina.getText());
				
				//entra no processo
				nroProcessoPagina.click();
				log.debug("Clique no processo");
				
				//espera aparecer botões do processo
				try {
					wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[1]/td/div/ul/li[1]")));
					log.debug("Carregou botões");		                               
					
					if (instancia.equals("2º Grau")) {
						log.debug("2º grau buscando aba movimentações...");
						//abre a aba de movimentações
						List<WebElement> botoes = wdw1.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("/html/body/div[1]/div[2]/form/fieldset/table[3]/tbody/tr[1]/td/div/ul/li")));
						for(WebElement bt: botoes) {
							if(bt.getText().trim().equals("Movimentações")) {
								bt.click();
								log.debug("Clicou na aba movimentações");
								break;
							}
						}
					}
				} catch (Exception e5) {
					log.debug("Falha ao encontrar botões, procurando outro xpath");
					wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div[2]/form/fieldset/table[4]/tbody/tr[1]/td/div/ul/li[1]")));
					if (instancia.equals("2º Grau")) {								  
						//abre a aba de movimentações
						List<WebElement> botoes = wdw1.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("/html/body/div[1]/div[2]/form/fieldset/table[4]/tbody/tr[1]/td/div/ul/li")));
						for(WebElement bt: botoes) {
							if(bt.getText().trim().equals("Movimentações")) {
								bt.click();
								log.debug("Clicou na aba movimentações");
								break;
							}
						}
					}
					log.debug("Carregou botões (tentativa 2)");
				}
				
			} catch (Exception e1) {
				//log.debug("Erro ao encontrar numero do processo: verificando se apareceu 'Nenhum registro encontrado' ");
				log.debug("Erro ao encontrar numero do processo: verificando se apareceu 'Segredo de justiça'...");
				//verifica se apareceu a mensagem 'Segredo de Justiça'
				try {
					WebElement segJus = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div[2]/form/table[2]/tbody/tr/td[3]/table/tbody/tr/td/ul/li")));
					
					if(segJus.getText().trim().contains("Segredo de Justiça")) {
						log.error("Processo em segredo de justiça! Continuando...");
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, segJus.getText().trim());
						return false;
					}
				} catch (Exception esj) {
					log.debug("Não encontrou segredo de justiça: verificando se apareceu 'Nenhum registro encontrado'");
				}
				
				try {
					WebElement msgProcessoNaoEncontrado = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div[2]/form/table[2]/tbody/tr/td")));
					if(msgProcessoNaoEncontrado.getText().equals("Nenhum registro encontrado")) {
						log.error("Mensagem 'Nenhum registro encontrado' encontrada!");
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Não encontrou processo:"+msgProcessoNaoEncontrado.getText());
						return false;
					} else {
						log.error("Mensagem 'Nenhum registro encontrado' NÃO encontrada! -> " + msgProcessoNaoEncontrado.getText());
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Não encontrou processo:"+msgProcessoNaoEncontrado.getText());
						return false;
					}
				} catch (Exception e2) {
					log.error("Mensagem 'Nenhum registro encontrado' NÃO encontrada!");
					CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Mensagem 'Nenhum registro encontrado' NÃO encontrada: [" + e2.getMessage() + "]");
					return false;
				}
			}
			
			fechaToastNotify(driver, wdw1);
			
			return true;
		} catch (Exception e) {
			log.debug("Erro ao abrir processo [" + numeroProcesso + "]", e);
			CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao abrir processo: [" + e.getMessage() + "]");
			return false;
		}
	}
	
	private static void fechaToastNotify(WebDriver driver, WebDriverWait wdw2) {
		try {
			WebDriverWait wdw1 = new WebDriverWait(driver, 2);
			
			//pega o nome do frame atual
			JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
			String currentFrame = (String) jsExecutor.executeScript("return self.name");
			log.debug("currentFrame: [" + currentFrame + "]");
			
			if(!currentFrame.equals("mainFrame")) {
				//volta para o mainFrame
				driver.switchTo().defaultContent();
				//vai pra frame mainFrame
				wdw1.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//*[@id=\"mainFrame\"]")));
				log.debug("Mudou para frame mainFrame!");
				//frameAtual = "mainFrame";
			}
			
			
			log.debug("Verificando se tem ToastyNotify...");
			List<WebElement> toastNotifys = driver.findElements(By.xpath("//*[contains(@class, 'toastnotify')]"));
			log.debug("[" + toastNotifys.size() + "] toastNotify's...");
			
			if(toastNotifys.size() == 0) {
				try {
					toastNotifys = wdw1.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className("toastnotify")));
					log.debug("[" + toastNotifys.size() + "] toastNotify's... Fechando...");
				} catch (Exception e1) {}
			}
			
			for(WebElement tn: toastNotifys) {
				log.debug("ToastNotify encontrado! Fechando...");
				tn.findElement(By.className("bBe")).click();
				try {
					wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@class='bBe']"))).click();
					log.debug("Clicou na 2...");
				} catch (Exception e1) {}
				log.debug("Fechado!");
				
			}
			
			if(!currentFrame.equals("mainFrame")) {
				if(frameAtual.equals("userMainFrame")) {
					//vai pra frame userMainFrame
					wdw1.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//*[@name=\"userMainFrame\"]")));
					log.debug("Mudou para frame userMainFrame !");
					frameAtual = "userMainFrame";
				} else {
					//vai pra frame userMainFrame
					wdw1.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//*[@name=\""+currentFrame+"\"]")));
					log.debug("Mudou para frame ["+currentFrame+"]!");
				}
				
			}
		} catch (Exception et) {
			log.debug("ToastNotify não encontrado!");
		}
	}

	public static void tirarPrint(WebDriver driver, Atividade atividade, WebElement we) {
		try {
			//colocar elemento no centro da tela
			String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                    + "var elementTop = arguments[0].getBoundingClientRect().top;"
                    + "window.scrollBy(0, elementTop-(viewPortHeight/2));";
			((JavascriptExecutor) driver).executeScript(scrollElementIntoMiddle, we);
			
			//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", we);
			File print = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
			String nomePrint = "printDocumentos---" + sdf.format(new Date());
			String pathArquivoInteiro = CopiaIntimacaoConstants.PATH_DOCUMENTOS+"/"+atividade.getCodigo() + "/"+nomePrint+".png";
			log.debug("Print salvo em [" + pathArquivoInteiro + "]");
			FileUtils.copyFile(print, new File(pathArquivoInteiro));
		} catch (Exception e) {
			log.error("Erro ao salvar print da atividade [" + atividade.getCodigo() + "]");
		}
	}
}
