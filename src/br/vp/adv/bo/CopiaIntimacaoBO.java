package br.vp.adv.bo;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.openqa.selenium.WebDriver;

import br.vp.adv.constants.CopiaIntimacaoConstants;
import br.vp.adv.dao.CopiaIntimacaoDAO;
import br.vp.adv.utils.Utils;
import br.vp.adv.vo.AnexoLS;
import br.vp.adv.vo.Atividade;
import br.vp.adv.vo.LogCopiaIntimacao;

public class CopiaIntimacaoBO {
	public static final Logger log = Logger.getLogger(CopiaIntimacaoBO.class);
	
	public static void baixarCopiasPROJUDI() {
		try {
			log.debug("=======================> INICIO CÓPIA INTIMAÇÃO PROJUDIPR");
			
			//pega as publicacoes em andamento de processos virtuais no PR
			ArrayList<Atividade> atividadesAptas = AtividadesBO.buscaPublicacoes(16, true, true);
			if(atividadesAptas == null) {
				return;
			}
			
			log.debug("Obteve [" + atividadesAptas.size() + "] publicações aptas para baixar intimação no PR.");
			
			if(atividadesAptas.size() == 0) {
				return;
			}
			
			log.debug("Realizando filtragem das atividades...");
			
			ArrayList<Atividade> atividades = new ArrayList<Atividade>();
			int contAt = 1;
			for(Atividade at: atividadesAptas) {
				if(atividades.size() >= CopiaIntimacaoConstants.LIMITE_POR_EXECUCAO) {
					log.debug("Completou o limite de [" + CopiaIntimacaoConstants.LIMITE_POR_EXECUCAO + "] atividades por execução! Iniciando busca...");
					break;
				}
				
				log.debug("Verificando atividade [" + at.getCodigo() + "] (" + contAt + " de " + atividadesAptas.size() + ")");
				contAt++;
				
				if(!verificaTentativasFrustadas(at.getNumeroProcesso(), at.getGrauProcesso(), at.getCodigo())) {
					//verifica se já tem arquivo anexado na atividade
					if(!temArquivoAnexadoAtividade(at.getCodigo(), at.getPasta())) {
						if(!temAtividadePublicacaoMesmaData(at)) {
							log.debug("O processo [" + at.getNumeroProcesso() + "] da atividade ["+at.getCodigo()+"] entrou para a fila!");
							atividades.add(at);
						}
					}
				}
			}
			
			log.debug("[" + atividades.size() + "] atividades na fila...");
			
			if(atividades.size() == 0) {
				return;
			}
			
			WebDriver driver = SeleniumBO.abrirNavegador();
			
			if(driver != null) {
				if(!SeleniumBO.loginProjudi(driver)) {
					driver.quit();
					return;
				}
				
				int cont = 1;
				for(Atividade atividade: atividades) {
					log.debug("======================================================================================================================================");
					log.debug("Buscando atividade [" + atividade.getCodigo() + "] (" + cont + " de " + atividades.size() + ")...");
					log.debug(atividade);
					SeleniumBO.buscaProcessoPROJUDIPR(driver, atividade);
					Utils.limparPastaTemp();
					cont++;
					log.debug("======================================================================================================================================");
				}
				
				driver.quit();
			} else {
				log.error("Erro ao abrir navegador!");
			}
			
			log.debug("=======================> FINAL CÓPIA INTIMAÇÃO PROJUDIPR");
		} catch (Exception e) {
			log.error("Falha na cópia de intimações do PROJUDI!", e);
			e.printStackTrace();
		}
	}
	
	private static boolean temAtividadePublicacaoMesmaData(Atividade at) {
		try {
			log.debug("Verificando se a pasta ["+at.getPasta()+"] possui duas (ou mais) publicaçoes no dia ["+at.getDataPublicacao()+"]...");
			
			ArrayList<Atividade> atividadesMesmaData = CopiaIntimacaoDAO.temAtividadePublicacaoMesmaData(at);
			if(atividadesMesmaData == null ) {
				log.error("Erro ao encontrar atividades mesma data!");
				return true;
			}
			
			if(atividadesMesmaData.size() == 0) {
				log.error("Não encontrou atividades na mesma data!");
				return false;
			}
			
			if(atividadesMesmaData.size() > 1) {
				log.debug("A pasta ["+at.getPasta()+"] possui as seguintes atividades com essa data de pub:");
				for(Atividade atividade: atividadesMesmaData) {
					log.debug(atividade);
				}
				
				log.debug("A atividade ["+at.getCodigo()+"] da pasta ["+at.getPasta()+"] tem mais de uma atividade com essa data, ñ entra!");
				return true;
			}
			
			log.debug(atividadesMesmaData.get(0));
			
			return false;
		} catch (Exception e) {
			log.error("Erro ao verificar se possui duas (ou mais) intimaçoes do dia ["+at.getDataPublicacao()+"] na pasta ["+at.getPasta()+"]!", e);
			return true;
		}
	}

	public static boolean temArquivoAnexadoAtividade(int codigo, int pasta) {
		try {
			boolean temArqAnexado = CopiaIntimacaoDAO.temArquivoAnexadoAtividade(codigo, pasta);
			if(temArqAnexado) {
				log.debug("A atividade ["+codigo+"] da pasta ["+pasta+"] já tem arquivo anexado, ñ entra!");
			}
			return temArqAnexado;
		} catch (Exception e) {
			log.error("Erro ao verificar se a atividade ["+codigo+"] da pasta ["+pasta+"] tem arquivo anexado!", e);
			return false;
		}
	}

	public static void insereLogCopiaIntimacao(int status, LogCopiaIntimacao logCI, String mensagem) {
		try {
			logCI.setStatus(status);
			if(mensagem != null) {
				String formatada = mensagem.replace("\n", "").trim();
				if(formatada.length() >= 200) {
					formatada = formatada.substring(0, 200);
				} else {
					if(logCI.getMensagem() != null && !logCI.getMensagem().equals("")) {
						String concatenado = logCI.getMensagem() + " - " + formatada;
						if(concatenado.length() < 200) {
							formatada = concatenado;
						}
					}
				}
				
				logCI.setMensagem(formatada);
				
			}
			
			if(logCI.getReferencia() != null && logCI.getReferencia().length() >= 100) {
				logCI.setReferencia(logCI.getReferencia().substring(0, 100));
			}
			
			//SE A ATIVIDADE BAIXOU VÁRIOS ANEXOS, SETA COMO STATUS 5 PARA NÃO SER FINALIZADA AUTOMATICAMENTE
			if(logCI.isMultiplosAnexos()) {
				logCI.setStatus(CopiaIntimacaoConstants.MULTIPLOS_ANEXOS);
			}
			
			CopiaIntimacaoDAO.insereLogCopiaIntimacao(logCI);
			log.debug("Log inserida: [" + logCI + "]");
		} catch (Exception e) {
			log.error("Erro ao inserir log " + logCI);
		}
	}
	
	/**
	 * Verifica se teve CopiaIntimacaoConstants.TENTATIVAS_POR_DIA ou mais tentativa frustradas no dia 
	 * ou se teve uma tentativa frustrada nas últimas CopiaIntimacaoConstants.INTERVALO_ENTRE_TENTATIVAS horas
	 * 
	 * @param numeroProcesso
	 * @param instancia
	 * @return
	 */
	public static boolean verificaTentativasFrustadas(String numeroProcesso, String instancia, int codigoAtividade) {
		try {
			if(instancia == null) {
				log.debug("A instância do processo [" + numeroProcesso + "] não foi setada! Não vai buscar agora!");
				return true;
			}
			int qtdTentativas = CopiaIntimacaoDAO.verificaTentativasFrustadas(numeroProcesso, instancia, codigoAtividade);
			if(qtdTentativas >= CopiaIntimacaoConstants.TENTATIVAS_POR_DIA) {
				log.debug("O processo ["+numeroProcesso+"] teve ["+CopiaIntimacaoConstants.TENTATIVAS_POR_DIA+"] ou mais tentativas no dia de hoje! Não vai buscar agora!");
				return true;
			} else {
				if(CopiaIntimacaoDAO.tentativaFrustradaTempo(numeroProcesso, instancia, codigoAtividade)) {
					log.debug("O processo ["+numeroProcesso+"] teve tentativa frustrada nas últimas ["+CopiaIntimacaoConstants.INTERVALO_ENTRE_TENTATIVAS+"] horas! Não vai buscar agora!");
					return true;
				}
				return false;
			}
		} catch (Exception e) {
			log.error("Erro ao verificar se a busca do processo [" + numeroProcesso + "] na instancia [" 
					+ instancia + "] teve " + CopiaIntimacaoConstants.TENTATIVAS_POR_DIA + " ou mais tentativas frustradas no dia de hoje!", e);
			return false;
		}
	}

	public static int obterIdAnexoPasta(int pasta) {
		try {
			return CopiaIntimacaoDAO.obterIdAnexoPasta(pasta);
		} catch (Exception e) {
			log.error("Erro ao obter o id do anexo mais recente da pasta [" + pasta + "]", e);
			e.printStackTrace();
			return -1;
		}
	}

	public static int incluirAnexoPasta(AnexoLS anexoLS) {
		try {
			return CopiaIntimacaoDAO.incluirAnexoPasta(anexoLS);
		} catch (Exception e) {
			log.error("Erro ao incluir o anexo [" + anexoLS + "]!", e);
			e.printStackTrace();
			return -1;
		}
	}

	public static ArrayList<String> buscaSequenciasProibidas(ArrayList<Integer> atividadesMesmaPasta) {
		try {
			String atividades = "";
			int cont = 0;
			for(Integer atividade: atividadesMesmaPasta) {
				if(cont > 0) {
					atividades += ",";
				}
				atividades += atividade;
				cont++;
			}
			return CopiaIntimacaoDAO.buscaSequenciasProibidas(atividades);
		} catch (Exception e) {
			log.error("Erro ao buscar sequencias proibidas para as atividade [" + atividadesMesmaPasta + "]!", e);
			return null;
		}
	}

	public static int verificaPublicacaoMesmaData(Atividade atividade) {
		try {
			return CopiaIntimacaoDAO.verificaPublicacaoMesmaData(atividade);
		} catch (Exception e) {
			log.error("Erro ao verificar se teve atividade com mesma data de publicação bem sucedida!", e);
			return -1;
		}
	}

	public static void limparTabela() {
		try {
			log.debug("Limpando logs de erro criadas a mais de 14 dias...");
			
			String timestamp14dias = LocalDateTime.now().minusDays(14).toString("yyyy-MM-dd");
			log.debug(timestamp14dias);
			
			String timestampInicio = timestamp14dias.concat(" 00:00:00");
			String timestampFinal = timestamp14dias.concat(" 23:59:59");
			
			CopiaIntimacaoDAO.deleteLogsErroPeriodo(timestampInicio, timestampFinal);
			
		} catch (Exception e) {
			log.error("Erro ao limpar tabela logCopiaIntimacao!", e);
		}
	}



	public static boolean verificaTentativasFrustadasTribunal(String numeroProcesso, String instancia, int codigoAtividade, String tribunal) {
		try {
			if(instancia == null) {
				log.debug("A instância do processo [" + numeroProcesso + "] não foi setada! Não vai buscar agora!");
				return true;
			}
			int qtdTentativas = CopiaIntimacaoDAO.verificaTentativasFrustadasTribunal(numeroProcesso, instancia, codigoAtividade, tribunal);
			if(qtdTentativas >= CopiaIntimacaoConstants.TENTATIVAS_POR_DIA) {
				log.debug("O processo ["+numeroProcesso+"] teve ["+CopiaIntimacaoConstants.TENTATIVAS_POR_DIA+"] ou mais tentativas no dia de hoje no ["+tribunal+"]! Não vai buscar agora!");
				return true;
			} else {
				if(CopiaIntimacaoDAO.tentativaFrustradaTempoTribunal(numeroProcesso, instancia, codigoAtividade, tribunal)) {
					log.debug("O processo ["+numeroProcesso+"] teve tentativa frustrada nas últimas ["+CopiaIntimacaoConstants.INTERVALO_ENTRE_TENTATIVAS+"] horas no ["+tribunal+"]! Não vai buscar agora!");
					return true;
				}
				return false;
			}
		} catch (Exception e) {
			log.error("Erro ao verificar se a busca do processo [" + numeroProcesso + "] na instancia [" 
					+ instancia + "] teve " + CopiaIntimacaoConstants.TENTATIVAS_POR_DIA + " ou mais tentativas frustradas no dia de hoje!", e);
			return false;
		}
	}



	public static boolean falhasTribunaisAnteriores(Atividade at) {
		try {
			int quantidadeFalhasPJEMG = obterQuantidadeFalhasTribunal(at, "PJEMG");
			int quantidadeFalhasPJERECURSALMG = obterQuantidadeFalhasTribunal(at, "PJERECURSALMG");
			
			if(quantidadeFalhasPJEMG == -1 || quantidadeFalhasPJERECURSALMG == -1) {
				log.error("Falha ao obter quantidade de falhas em algum tribunal!");
				return false;
			}
			
			log.info("Quantidade falhas PJEMG: [" + quantidadeFalhasPJEMG + "] - quantidade falhas PJERECURSALMG: [" + quantidadeFalhasPJERECURSALMG + "]");
			
			if(quantidadeFalhasPJEMG >= CopiaIntimacaoConstants.QTD_FALHAS_MG && quantidadeFalhasPJERECURSALMG >= CopiaIntimacaoConstants.QTD_FALHAS_MG) {
				return true;
			}
			
			return false;
		} catch (Exception e) {
			log.error("Erro ao verificar se a atividade teve 3 ou mais falhas no PJEMG e no PJERECURSALMG!");
			return false;
		}

	}

	private static int obterQuantidadeFalhasTribunal(Atividade at, String tribunal) {
		try {
			return CopiaIntimacaoDAO.obterQuantidadeFalhasTribunal(at, tribunal);
		} catch (Exception e) {
			log.error("Falha ao obter quantidade de falhas da atividade [" + at.getCodigo() + "] no [" + tribunal + "]", e);
			return -1;
		}
	}
}
