package br.vp.adv.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.vp.adv.bo.AtividadesBO;
import br.vp.adv.bo.CopiaIntimacaoBO;
import br.vp.adv.bo.SeleniumBO;
import br.vp.adv.constants.CopiaIntimacaoConstants;
import br.vp.adv.utils.Utils;
import br.vp.adv.utils.WasabiUtils;
import br.vp.adv.vo.Atividade;
import br.vp.adv.vo.LogCopiaIntimacao;

public class EPROCSC1G {
	
	private static final Logger log = Logger.getLogger(EPROCSC1G.class);
	
	public static void buscarIntimacoes() {
		try {
			log.debug("=======================> INICIO CÓPIA INTIMAÇÃO EPROCSC 1G");
			
			ArrayList<Atividade> atividadesAptas = AtividadesBO.buscaPublicacoes(24, true, false);
			
			if(atividadesAptas == null) {
				return;
			}
			
			log.debug("Obteve [" + atividadesAptas.size() + "] publicações aptas para baixar intimação em SC.");
			
			log.debug("Realizando filtragem das atividades...");
			
			ArrayList<Atividade> atividades = new ArrayList<Atividade>();
			int contAt = 1;
			for(Atividade at: atividadesAptas) {
				if(atividades.size() >= CopiaIntimacaoConstants.LIMITE_POR_EXECUCAO) {
					log.debug("Completou o limite de [" + CopiaIntimacaoConstants.LIMITE_POR_EXECUCAO + "] atividades por execução! Iniciando busca...");
					break;
				}
				
				log.debug("Verificando atividade [" + at.getCodigo() + "] - ["+at.getGrauProcesso()+"] (" + contAt + " de " + atividadesAptas.size() + ")");
				contAt++;
				
				if(!CopiaIntimacaoBO.verificaTentativasFrustadas(at.getNumeroProcesso(), at.getGrauProcesso(), at.getCodigo())) {
					//verifica se já tem arquivo anexado na atividade
					if(!CopiaIntimacaoBO.temArquivoAnexadoAtividade(at.getCodigo(), at.getPasta())) {
						log.debug("O processo [" + at.getNumeroProcesso() + "] da atividade ["+at.getCodigo()+"] entrou para a fila!");
						atividades.add(at);
					}
				}
			}
			
			log.debug("[" + atividades.size() + "] atividades na fila...");
			
			if(atividades.size() == 0) {
				return;
			}
			
			//abre navegador
			WebDriver driver = SeleniumBO.abrirNavegador();
			
			int contSucesso = 0;
			if(driver != null) {
				if(!loginEPROCSC(driver)) {
					driver.quit();
					return;
				}
				
				int cont = 1;
				
				for(Atividade atividade: atividades) {
					log.debug("======================================================================================================================================");
					log.debug("Buscando atividade [" + atividade.getCodigo() + "] (" + cont + " de " + atividades.size() + ")...");
					log.debug(atividade);
					
					if(buscaIntimacaoProcessoEPROCSC1G(driver, atividade)) {
						contSucesso++;
					}
					
					Utils.limparPastaTemp();
					cont++;
					log.debug("======================================================================================================================================");
				}
				
				driver.quit();
			} else {
				log.error("Erro ao abrir navegador!");
			}
			
			log.debug("=======================> FINAL CÓPIA INTIMAÇÃO EPROCSC 1G! Sucesso: [" + contSucesso + " de " + atividades.size() + "]");
		} catch (Exception e) {
			log.debug("Falha ao buscar intimações no EPROC/SC 1G!", e);
		}
	}

	private static boolean buscaIntimacaoProcessoEPROCSC1G(WebDriver driver, Atividade atividade) {
		try {
			String numeroProcesso = atividade.getNumeroProcesso();
			int pasta = atividade.getPasta();
			
			LogCopiaIntimacao logCI = new LogCopiaIntimacao();
			logCI.setCodigoAtividade(atividade.getCodigo());
			logCI.setPasta(pasta);
			logCI.setNumeroProcesso(numeroProcesso);
			logCI.setInstancia(atividade.getGrauProcesso());
			logCI.setDataPublicacaoAtividade(atividade.getDataPublicacao());
			logCI.setTribunal("EPROCSC1G");
			
			WebDriverWait wdw = new WebDriverWait(driver, 30);
			
			//verifica se já teve uma publicação com mesma data e mesma pasta
			log.debug("Verificando se já teve uma publicação da pasta [" + atividade.getPasta() + "] da data [" + atividade.getDataPublicacao() + "] com status sucesso...");
			int pubMesmoDocs = CopiaIntimacaoBO.verificaPublicacaoMesmaData(atividade);
			if(pubMesmoDocs > 0) {
				log.debug("Já teve uma atividade de publicação com a data [" + atividade.getDataPublicacao() + "] com sucesso: ["+pubMesmoDocs+"]! Não vai rodar essa atividade!");
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.DOCUMENTOS_ATIVIDADE_MESMO_DIA, logCI, "A atividade ["+pubMesmoDocs+"] é do mesmo dia e tem status sucesso! Não roda para evitar duplicidade de documentos!");
				return true;
			}
			
			fechaAbas(driver);
			
			if(!abrirProcesso(atividade, driver, wdw, logCI)) {
				return false;
			}
			
			if(!buscaEventosIntimacoes(atividade, driver, wdw, logCI)) {
				return false;
			}
			
			return true;
		} catch (Exception e) {
			log.error("Erro ao buscar intimações da atividade [" + atividade.getCodigo() + "]", e);
			return false;
		}
	}

	private static boolean buscaEventosIntimacoes(Atividade atividade, WebDriver driver, WebDriverWait wdw, LogCopiaIntimacao logCI) {
		try {
			//pegando os eventos
			List<WebElement> eventos = wdw.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"tblEventos\"]/tbody/tr")));
			log.debug("Obteve [" + eventos.size() + "] eventos...");
			
			log.debug("Buscando confirmação de intimação com data [" + atividade.getDataPublicacao() + "]");
			
			ArrayList<String> referenciasIntermediarias = buscaReferenciasIntermediarias(atividade, wdw, logCI, eventos);
			
			if(referenciasIntermediarias == null || referenciasIntermediarias.size() == 0) {
				log.error("referenciasIntermediarias = null! Buscando 'Processos relacionados'...");
				referenciasIntermediarias = buscaRefIntermediariasProcRelacionados(atividade, driver, wdw, logCI);
				
				if(referenciasIntermediarias == null || referenciasIntermediarias.size() == 0) {
					log.error("Falha ao buscar referências intermediárias nos processos relacionados...");
					CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao buscar em processos relacionados!");
					return false;
				}
				
				//pegando os eventos (novamente pois mudou a aba)
				eventos = wdw.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"tblEventos\"]/tbody/tr")));
			}
			
			log.debug("Referências intermediárias: " + referenciasIntermediarias);
			
			//busca eventos finais
			ArrayList<String> referenciasFinais = buscaReferenciasFinais(atividade, wdw, logCI, eventos, referenciasIntermediarias);
			
			if(referenciasFinais == null || referenciasFinais.size() == 0) {
				log.error("referenciasFinais = null");
				return false;
			}
			
			log.debug("Referências finais: " + referenciasFinais);
			
			if(!baixarDocumentosEventos(atividade, driver, wdw, logCI, eventos, referenciasFinais)) {
				return false;
			}
			
			log.debug("Sucesso ao buscar documentos da atividade [" + atividade.getCodigo() + "]!");
			return true;
		} catch (Exception e) {
			log.error("Erro ao buscar eventos para baixar...", e);
			return false;
		}
	}
	
	public static void tirarPrint(WebDriver driver, Atividade atividade) {
		try {
			File print = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
			String nomePrint = "printDocumentos---" + sdf.format(new Date());
			String pathArquivoInteiro = CopiaIntimacaoConstants.PATH_DOCUMENTOS+"/"+atividade.getCodigo() + "/"+nomePrint+".png";
			log.debug("Print salvo em [" + pathArquivoInteiro + "]");
			FileUtils.copyFile(print, new File(pathArquivoInteiro));
		} catch (Exception e) {
			log.error("Erro ao salvar print da atividade [" + atividade.getCodigo() + "]");
		}
	}

	private static boolean baixarDocumentosEventos(Atividade atividade, WebDriver driver, WebDriverWait wdw, LogCopiaIntimacao logCI, List<WebElement> eventos, ArrayList<String> referenciasFinais) {
		try {
			log.debug("Buscando documentos para baixar nas referencias finais...");
			
			ArrayList<File> arquivos = new ArrayList<File>();
			
			int qtdEventosFinaisPercorridos = 0;
			
			for(WebElement evento: eventos) {
				String idEvento = evento.findElement(By.xpath("./td[1]")).getText().trim().replace("\n", "");
				String descricao = evento.findElement(By.xpath("./td[3]")).getText().trim().replace("\n", " ");
				
				log.debug("Verificando evento [" + idEvento + "] - [" + descricao + "]");
				
				//se já procurou todas as referencias finais finaliza
				if(qtdEventosFinaisPercorridos == referenciasFinais.size()) {
					log.debug("Procurou em todas as referências finais!");
					break;
				}
				
				//verifica se esse evento ta na lista de referencias finais
				boolean ehFinal = false;
				for(String refFinal: referenciasFinais) {
					if(refFinal.equals(idEvento)) {
						log.debug("O evento [" + idEvento + "] está na lista de referências finais!");
						ehFinal = true;
						qtdEventosFinaisPercorridos++;
						break;
					}
				}
				
				if(!ehFinal) {
					continue;
				}
				
				//add referencia
				if(logCI.getReferencia() != null && !logCI.getReferencia().equals("")) {
					logCI.setReferencia(logCI.getReferencia() + " // " + idEvento);
				} else {
					logCI.setReferencia(idEvento);
				}
				
				log.debug("Buscando documentos no evento [" + idEvento + "]");
				
				WebElement tdDocumentos = evento.findElement(By.xpath("./td[5]"));
				
				String textoTdDocumentos = tdDocumentos.getText().replace("\n", " ").trim();
				
				log.debug("textoTdDocumentos: [" + textoTdDocumentos + "]");
				
				if(textoTdDocumentos.contains("Evento não gerou documento")) {
					if((referenciasFinais.size() == 1 || (qtdEventosFinaisPercorridos == referenciasFinais.size())) && arquivos.size() == 0) {
						CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.NAO_GEROU_DOCUMENTO, logCI, textoTdDocumentos);
						return false;
					}
					
					log.debug("Evento não gerou documento porém já existe documento pra subir! Continua...");
				}
				
				//procura links dentro do evento
				//List<WebElement> links = tdDocumentos.findElements(By.className("infraLinkDocumento"));
				List<WebElement> links = tdDocumentos.findElements(By.xpath("./a[contains(@class,'infraLinkDocumento')]"));
				log.debug("Encontrou [" + links.size() + "] links no evento!");
				
				for(WebElement link: links) {
					log.debug("Baixando link [" + link.getText() + "] - href [" + link.getAttribute("href") + "]");
					
					//link.click();
					JavascriptExecutor executor = (JavascriptExecutor)driver;
					executor.executeScript("arguments[0].click();", link);
					
					Thread.sleep(5000);
					
					//pega nova guia
					Set<String> handlesSet = driver.getWindowHandles();
			        List<String> handlesList = new ArrayList<String>(handlesSet);
			        if(handlesList.size() > 1) {
			        	driver.switchTo().window(handlesList.get(handlesList.size()-1));
				        log.debug("Mudou para aba " + driver.getTitle() + "!");
				        
				        //tira print da tela
				        tirarPrint(driver, atividade);
				        
				        File arquivo = baixarDocumento(atividade, driver, wdw);
				        
				        if(arquivo == null) {
				        	log.warn("arquivo = null");
				        	return false;
				        }
				        
				        log.debug("Adicionando [" + arquivo.getPath() + "] na lista de arquivos para anexar ao LS");
				        
				        arquivos.add(arquivo);
				        
			        } else {
			        	log.error("Erro ao encontrar e mudar de abas!");
			        	return false;
			        }
			        
			        //fechaAbas(driver);
			        fechaUltimaAbaAberta(driver);
				}
			}
			
			fechaAbas(driver);
			
			if(arquivos.size() == 0) {
				log.debug("arquivos.size() = 0");
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Nenhum arquivo baixado!");
				return false;
			}
			
			log.debug("[" + arquivos.size() + "] arquivos para anexar ao LS! Anexando...");
			
			for(File arquivo: arquivos) {
				log.debug("Anexando arquivo [" + arquivo.getName() + "]");
				if(!WasabiUtils.anexarArquivoLS(atividade.getPasta(), atividade.getCodigo(), arquivo.getName(), arquivo, logCI, true)) {
					log.error("Erro ao subir [" + arquivo.getName() + "] para o LS!");
					CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao anexar arquivo no LS!");
					return false;
				}
			}
			
			CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.SUCESSO_BUSCA_DOCUMENTOS, logCI, "Sucesso ao baixar documentos!");
			
			return true;
		} catch (Exception e) {
			log.error("Erro ao baixar documentos das referências finais!", e);
			CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao baixar documentos das referências finais:" + e.getMessage());
			return false;
		}
	}
	
	private static void fechaUltimaAbaAberta(WebDriver driver) {
		try {
			log.debug("Fechando última aba aberta...");
			Set<String> handlesSet = driver.getWindowHandles();
	        List<String> handlesList = new ArrayList<String>(handlesSet);
	        if(handlesList.size() > 1) {
	        	driver.switchTo().window(handlesList.get(handlesList.size()-1));
	        	if(!driver.getTitle().contains(":: eproc - - Consulta Processual - Detalhes do Processo")) {
	        		driver.close();
	        	} else {
	        		log.debug("Não fechou aba!");
	        		return;
	        	}
	        } else {
	        	log.debug("Apenas uma aba aberta, não vai fechar...");
	        	driver.switchTo().window(handlesList.get(0));
	        	return;
	        }
	        driver.switchTo().window(handlesList.get(handlesList.size()-2));
	        log.debug("Mudou para aba " + driver.getTitle() + "!");
		} catch (Exception e) {
			log.error("Erro ao fechar última aba aberta!", e);
		}
		
	}

	private static void fechaAbas(WebDriver driver) {
		try {
			log.debug("Fechando abas abertas...");
			//volta para aba principal
			Set<String> handlesSet = driver.getWindowHandles();
	        List<String> handlesList = new ArrayList<String>(handlesSet);
	        String handleMain = handlesList.get(0);
	        if(handlesList.size() > 1) {
	        	for(int i = 1; i < handlesList.size(); i++){
	        		driver.switchTo().window(handlesList.get(i));
	        		driver.close();
	        	}
	        }
	        driver.switchTo().window(handleMain);
	        log.debug("Mudou para aba " + driver.getTitle() + "!");
		} catch (Exception e) {
			log.error("Erro ao fechar abas!", e);
		}
	}

	private static File baixarDocumento(Atividade atividade, WebDriver driver, WebDriverWait wdw) {
		try {
			String numeroProcesso = atividade.getNumeroProcesso();
			int codigoAtividade = atividade.getCodigo();
			WebDriverWait wdw1 = new WebDriverWait(driver, 10);
			
			String nomeArquivo = atividade.getCodigo() + "-arquivo-"+numeroProcesso.replace(".", "-")+".pdf";
			
			String pathFolderCodigoAtividade = CopiaIntimacaoConstants.PATH_DOCUMENTOS+"/"+codigoAtividade;	
			String pathCompletoArquivo = pathFolderCodigoAtividade + "/" + nomeArquivo;
			
			File arq = new File(pathCompletoArquivo);
			if(arq.exists()) {
				log.debug("O arquivo [" + nomeArquivo + "] já existe! Mudando nome...");
				int cont = 1;
				while(arq.exists()) {
					nomeArquivo = atividade.getCodigo() + "-arquivo-"+cont+"-"+numeroProcesso.replace(".", "-")+".pdf";
					log.debug("Nome do arquivo: [" + nomeArquivo + "]");
					
					pathCompletoArquivo = pathFolderCodigoAtividade + "/" + nomeArquivo;
					
					arq = new File(pathCompletoArquivo);
					
					cont++;
				}
			}
			log.debug("Nome do arquivo OFICIAL: [" + nomeArquivo + "]");
			
			//verifica se existe um folder pra esse processo
			File folderAtividade = new File(pathFolderCodigoAtividade);
			if(!folderAtividade.exists() || !folderAtividade.isDirectory()) {
				folderAtividade.mkdir();
			}
			
			//espera aparecer botão imprimir
			try {
				WebElement btImprimir = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"btnImprimir\"]")));
				log.debug("Encontrou botão imprimir!");
				btImprimir.click();
			} catch (Exception ebi) {
				log.debug("Não encontrou botão imprimir, buscando botão abrir...");
				
				log.debug("Esperando iframe conteudoIframe");
				wdw.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//*[@id=\"conteudoIframe\"]")));
				log.debug("Trocou pro iFrame! Buscando botão de abrir...");
				WebElement btAbrir = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"open-button\"]")));
				log.debug("Encontrou botão abrir!");
				btAbrir.click();
			}
			
			log.debug("Esperando download...");
			Thread.sleep(30000);
			
			log.debug("Baixou arquivo!");
			
			//pega o ultimo arquivo na pasta
			File[] arquivosTemp = new File(CopiaIntimacaoConstants.PATH_DOCUMENTOS_TEMP).listFiles();
			log.debug("Encontrou [" + arquivosTemp.length + "] arquivos na pasta TEMP!");
			File ultimoArquivoMod = null;
			Date ultimoMod = null;
			for(int indArqTempo = 0; indArqTempo < arquivosTemp.length; indArqTempo++) {
				Date dataUltimaMod = new Date(arquivosTemp[indArqTempo].lastModified());
				if(ultimoMod == null || dataUltimaMod.after(ultimoMod)) {
					ultimoArquivoMod = arquivosTemp[indArqTempo];
					ultimoMod = dataUltimaMod;
				}
			}
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			if(ultimoArquivoMod == null || ultimoMod == null) {
				log.error("Erro ao pegar arquivo baixado!");
				//CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao pegar algum arquivo baixado!");
				return null;
			} else {
				String nomeArquivoPastaTemp = ultimoArquivoMod.getName();
				
				log.debug("Último arquivo modificado: ["+nomeArquivoPastaTemp+"] - [" + sdf.format(ultimoMod) + "]");
				log.debug("Tamanho: [" + ultimoArquivoMod.length() + "]");
				
				File arquivoDownload = new File(pathCompletoArquivo);
				
				log.debug("Copiando arquivo para [" + pathCompletoArquivo + "]");
				//copia pra essa pasta
				FileUtils.copyFile(ultimoArquivoMod, arquivoDownload);
				log.debug("Copiou arquivo para [" + pathCompletoArquivo + "] com sucesso!");
				
				//arquivosDoProcesso.add(arquivoDownload);
				
				//excluir arquivo
				if(ultimoArquivoMod.delete()) {
					log.debug("Sucesso ao excluir arquivo ["+ultimoArquivoMod.getName() + "] da pasta TEMP!");
				} else {
					log.warn("FALHA ao excluir arquivo ["+ultimoArquivoMod.getName() + "] da pasta TEMP!");
				}
				
				return arquivoDownload;
				
				/*
				//subir no LS
				if(!WasabiUtils.anexarArquivoLS(atividade.getPasta(), atividade.getCodigo(), arquivoDownload.getName(), arquivoDownload, logCI)) {
					log.error("Erro ao subir [" + arquivoDownload.getName() + "] para o LS!");
					CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao anexar arquivo no LS!");
					return false;
				}
				*/
			}
		} catch (Exception e) {
			log.error("Falha ao baixar documento!", e);
			return null;
		}
	}

	private static ArrayList<String> buscaReferenciasFinais(Atividade atividade, WebDriverWait wdw, LogCopiaIntimacao logCI, List<WebElement> eventos, ArrayList<String> referenciasIntermediarias) {
		try {
			log.debug("Procurando referências finais para cada cada referência intermediária...");
			
			ArrayList<String> referenciasFinais = new ArrayList<String>();
			
			int refProcuradas = 0;
			
			for(WebElement evento: eventos) {
				String idEvento = evento.findElement(By.xpath("./td[1]")).getText().trim().replace("\n", "");
				//String dataHora = evento.findElement(By.xpath("./td[2]")).getText().trim().replace("\n", "");
				String descricao = evento.findElement(By.xpath("./td[3]")).getText().trim().replace("\n", " ");
				
				log.debug("Verificando evento [" + idEvento + "]");
				
				//se já procurou todas as referencias intermediária finaliza
				if(refProcuradas == referenciasIntermediarias.size()) {
					log.debug("Procurou em todas as referências intermediárias!");
					break;
				}
				
				//verifica se esse evento ta na lsita de referencias intermediárias
				boolean ehIntermediaria = false;
				for(String refIntermediaria: referenciasIntermediarias) {
					if(refIntermediaria.equals(idEvento)) {
						log.debug("O evento [" + idEvento + "] está na lista de referências intermediárias!");
						ehIntermediaria = true;
						refProcuradas++;
						break;
					}
				}
				
				if(!ehIntermediaria) {
					continue;
				}
				
				log.debug("Buscando referência(s) final(is) na descricao do evento [" + idEvento + "] - [" + descricao +"]");
				
				String palavraChave = "Refer. ao Evento: ";
				String palavraChave2 = "Refer. ao Evento ";
				String palavraChaveDoisEventos = "Refer. aos Eventos ";
				
				boolean multiplosEventos = false;
				
				int indexInicioRef = descricao.indexOf(palavraChave);
				int indexInicioRef2 = descricao.indexOf(palavraChave2);
				
				log.debug("indexInicioRef: ["+indexInicioRef+"] - indexInicioRef2: ["+indexInicioRef2+"]");
				
				if(indexInicioRef == -1) {
					indexInicioRef = indexInicioRef2;
				}
				
				if(indexInicioRef == -1) {
					log.warn("Não encontrou [" + palavraChave + "] na descrição do evento! Procurando por [" + palavraChaveDoisEventos + "]...");
					
					indexInicioRef = descricao.indexOf(palavraChaveDoisEventos);
					if(indexInicioRef == -1) {
						log.warn("Não encontrou [" + palavraChave + "]!");
						logCI.setMensagem("Não encontrou ref. final no evento " + idEvento + "!");
						continue;
					} else {
						log.debug("Encontrou '" +  palavraChaveDoisEventos + "'");
						multiplosEventos = true;
					}
				} else {
					log.debug("Encontrou frase no singular! Verificando se tem múltiplos eventos...");
					
					String referenciaCompleta = descricao.substring(indexInicioRef);
					String refFinalSuja = referenciaCompleta.replace(palavraChave, "");
					refFinalSuja = referenciaCompleta.replace(palavraChave2, "");
					refFinalSuja = refFinalSuja.replace(palavraChaveDoisEventos, "");
					
					if(refFinalSuja.indexOf(" (") != -1) {
						refFinalSuja = refFinalSuja.substring(0, refFinalSuja.indexOf(" ("));
					}
					
					log.debug("refFinalSuja: ["+refFinalSuja+"]");
					
					String referenciasFiltradas = refFinalSuja.replace(",", "").replace(" e ", " ").trim();
					
					log.debug("referenciasFiltradas: ["+referenciasFiltradas+"]");
					
					int indexEspaco = referenciasFiltradas.indexOf(" ");
					log.debug("indexEspaco: ["+indexEspaco+"]");
					
					if(indexEspaco != -1) {
						log.debug("Como encontrou espaço vai marcar como múltiplos eventos...");
						multiplosEventos = true;
					}
				}
				
				String referenciaCompleta = descricao.substring(indexInicioRef);
				
				String refFinalSuja = referenciaCompleta.replace(palavraChave, "");
				refFinalSuja = referenciaCompleta.replace(palavraChave2, "");
				refFinalSuja = refFinalSuja.replace(palavraChaveDoisEventos, "");
				
				String[] camposRefFinal = null;
				
				if(multiplosEventos) {
					//testes
					//Utils.enviaEmailThread("CopiaIntimacao - Ref. a múltiplos eventos EPROC!", "O processo ["+atividade.getNumeroProcesso()+"] teve evento com múltiplas referências! Verifique!", Utils.arrayEmailBruno, null, null);
					
					String referenciasFiltradas = refFinalSuja.replace(",", "").replace(" e ", " ").trim();
					
					if(referenciasFiltradas.indexOf(" (") != -1) {
						referenciasFiltradas = referenciasFiltradas.substring(0, referenciasFiltradas.indexOf(" ("));
					}
					
					log.debug("referenciasFiltradas = " + referenciasFiltradas);
					
					camposRefFinal = referenciasFiltradas.split(" ");
					
					for(int i = 0; i < camposRefFinal.length; i++) {
						String refInter = camposRefFinal[i].trim();
						
						if(refInter.contains(" ")) {
							String[] limpezaRef2 = refInter.split(" ");
							refInter = limpezaRef2[0];
						}
						
						if(referenciasFinais.contains(refInter)) {
							log.debug("Referência intermediária [" + refInter + "] já está na lista!");
						} else {
							referenciasFinais.add(refInter);
							log.debug("Adicionou [" + refInter + "]");
						}
					}
					
					
					
				} else {
					camposRefFinal = refFinalSuja.split(" ");
					String refFinal =  camposRefFinal[0];
					
					log.debug("Encontrou referência final [" + refFinal + "] na referência intermediária [" + idEvento + "]! Adicionando na lista de referências finais...");
					
					if(referenciasFinais.contains(refFinal)) {
						log.debug("Referência final [" + refFinal + "] já está na lista!");
					} else {
						referenciasFinais.add(refFinal);
						log.debug("Adicionou [" + refFinal + "] na lista!");
					}
				}
			}
			
			return referenciasFinais;
		} catch (Exception e) {
			log.error("Erro na busca das referências finais!", e);
			CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha na busca de referências finais:" + e.getMessage());
			return null;
		}
	}

	private static ArrayList<String> buscaReferenciasIntermediarias(Atividade atividade, WebDriverWait wdw, LogCopiaIntimacao logCI, List<WebElement> eventos) {
		try {
			ArrayList<String> referenciasIntermediarias = new ArrayList<String>();
			for(WebElement evento: eventos) {
				String idEvento = evento.findElement(By.xpath("./td[1]")).getText().trim().replace("\n", "");
				String dataHora = evento.findElement(By.xpath("./td[2]")).getText().trim().replace("\n", "");
				String descricao = evento.findElement(By.xpath("./td[3]")).getText().trim().replace("\n", " ");
				String usuario = evento.findElement(By.xpath("./td[4]")).getText().trim().replace("\n", " ").toUpperCase();
				
				log.debug("[" + idEvento + "] - [" + dataHora + "] - [" + descricao + "] - [" + usuario + "]");
				
				String[] camposDataHora = dataHora.split(" ");
				String data = camposDataHora[0];
				
				log.debug("Apenas data -> [" + data + "]");
				
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
				LocalDate ldtDataPubAt = LocalDate.parse(atividade.getDataPublicacao(), formatter);
				LocalDate ldtDataEvento = LocalDate.parse(data, formatter);
				
				//se a data for anterior e já encontrou referência intermediária -> para a busca
				if((ldtDataEvento.until(ldtDataPubAt, ChronoUnit.DAYS) >= 1) && referenciasIntermediarias.size() > 0) {
					log.debug("Já encontrou referência intermediária e data do evento [" + data + "] é anterior! Parando busca...");
					break;
				}
				
				//se a data for anterior a 14 dias da data da publicação -> para a busca
				if(ldtDataEvento.until(ldtDataPubAt, ChronoUnit.DAYS) > 14) {
					log.debug("A data do evento [" + data + "] é muito anterior a data da publicação na atividade [" + atividade.getDataPublicacao() + "]! Parando busca...");
					
					if(referenciasIntermediarias.size() == 0) {
						//CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao encontrar data!");
						return null;
					}
				}
				
				if(!usuario.equals("SC017282") && !usuario.equals("PR020835") && !usuario.equals("SC009603") && !usuario.equals("RS064803A") && !usuario.equals("MG102044")) {
					log.debug("Evento não é do usuário SC017282 | PR020835 | SC009603 | RS064803A | MG102044! Pulando...");
					continue;
				}
				
				if(descricao.contains("Confirmada a intimação eletrônica ")) {
					log.debug("Encontrou uma confirmação de intimação! Verificando a data...");
					log.debug("Data do evento: [" + data + "] - data da publicação atividade: [" + atividade.getDataPublicacao() + "]");
					if(data.equals(atividade.getDataPublicacao())) {
						log.debug("As datas são iguais! Procurando referência intermediária...");
						
						String palavraChave = "Refer. ao Evento: ";
						String palavraChave2 = "Refer. ao Evento ";
						String palavraChaveDoisEventos = "Refer. aos Eventos: ";
						
						boolean multiplosEventos = false;
						
						int indexInicioRef = descricao.indexOf(palavraChave);
						int indexInicioRef2 = descricao.indexOf(palavraChave2);
						
						log.debug("indexInicioRef: ["+indexInicioRef+"] - indexInicioRef2: ["+indexInicioRef2+"]");
						
						if(indexInicioRef == -1) {
							indexInicioRef = indexInicioRef2;
						}
						
						if(indexInicioRef == -1) {
							log.error("Falha ao encontrar '" + palavraChave + "'! Procurando '" + palavraChaveDoisEventos + "'");
							indexInicioRef = descricao.indexOf(palavraChaveDoisEventos);
							if(indexInicioRef == -1) {
								log.error("Falha ao encontrar '" + palavraChaveDoisEventos + "'!");
								if(referenciasIntermediarias.size() == 0) {
									CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao encontrar referência intermediária do evento!");
									return null;
								}
							} else {
								log.debug("Encontrou '" +  palavraChaveDoisEventos + "'");
								multiplosEventos = true;
							}
						} else {
							log.debug("Encontrou frase no singular! Verificando se tem múltiplos eventos...");
							
							String referenciaCompleta = descricao.substring(indexInicioRef);
							String refFinalSuja = referenciaCompleta.replace(palavraChave, "");
							refFinalSuja = referenciaCompleta.replace(palavraChave2, "");
							refFinalSuja = refFinalSuja.replace(palavraChaveDoisEventos, "");
							
							if(refFinalSuja.indexOf(" (") != -1) {
								refFinalSuja = refFinalSuja.substring(0, refFinalSuja.indexOf(" ("));
							}
							
							log.debug("refFinalSuja: ["+refFinalSuja+"]");
							
							String referenciasFiltradas = refFinalSuja.replace(",", "").replace(" e ", " ").trim();
							
							log.debug("referenciasFiltradas: ["+referenciasFiltradas+"]");
							
							int indexEspaco = referenciasFiltradas.indexOf(" ");
							log.debug("indexEspaco: ["+indexEspaco+"]");
							
							if(indexEspaco != -1) {
								log.debug("Como encontrou espaço vai marcar como múltiplos eventos...");
								multiplosEventos = true;
							}
						}
						
						String referenciaCompleta = descricao.substring(indexInicioRef);
						
						String referenciaIntermediaria = referenciaCompleta.replace(palavraChave, "").trim();
						referenciaIntermediaria = referenciaIntermediaria.replace(palavraChave2, "").trim();
						referenciaIntermediaria = referenciaIntermediaria.replace(palavraChaveDoisEventos, "").trim();
						
						log.debug("Encontrou referência(s) intermediária(s): [" + referenciaIntermediaria + "]! Adicionando na lista de referências intermediárias...");
						
						if(multiplosEventos) {
							//testes
							//Utils.enviaEmailThread("CopiaIntimacao - Ref. a múltiplos eventos EPROC!", "O processo ["+atividade.getNumeroProcesso()+"] teve evento com múltiplas referências! Verifique!", Utils.arrayEmailBruno, null, null);
							
							String referenciasFiltradas = referenciaIntermediaria.replace(",", "").replace(" e ", " ").trim();
							
							if(referenciasFiltradas.indexOf(" (") != -1) {
								referenciasFiltradas = referenciasFiltradas.substring(0, referenciasFiltradas.indexOf(" ("));
							}
							
							log.debug("referenciasFiltradas = " + referenciasFiltradas);
							
							String[] multReferencias = referenciasFiltradas.split(" ");
							
							for(int i = 0; i < multReferencias.length; i++) {
								String refInter = multReferencias[i].trim();
								
								if(refInter.contains(" ")) {
									String[] limpezaRef2 = refInter.split(" ");
									refInter = limpezaRef2[0];
								}
								
								if(referenciasIntermediarias.contains(refInter)) {
									log.debug("Referência intermediária [" + refInter + "] já está na lista!");
								} else {
									referenciasIntermediarias.add(refInter);
									log.debug("Adicionou [" + refInter + "]");
								}
							}
						} else {
							if(referenciasIntermediarias.contains(referenciaIntermediaria)) {
								log.debug("Referência intermediária [" + referenciaIntermediaria + "] já está na lista!");
							} else {
								referenciasIntermediarias.add(referenciaIntermediaria);
								log.debug("Adicionou [" + referenciaIntermediaria + "]");
							}
						}
						
						
					}
				}
			}
			return referenciasIntermediarias;
		} catch (Exception e) {
			log.error("Erro na busca das referências intermediárias!", e);
			CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha na busca de referências intermediárias:" + e.getMessage());
			return null;
		}
	}
	
	private static ArrayList<String> buscaRefIntermediariasProcRelacionados(Atividade atividade, WebDriver driver, WebDriverWait wdw, LogCopiaIntimacao logCI) {
		try {
			//ArrayList<String> referenciasIntermediarias = new ArrayList<String>();
			
			//busca os processos relacionados
			WebElement tabelaProcessosRelacionados = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"tableRelacionado\"]")));
			log.debug("Encontrou tabela de processos relacionados! Procurando links no interior...");
			
			List<WebElement> linksProcessosRelacionados = tabelaProcessosRelacionados.findElements(By.tagName("a"));
			log.debug("Obteve [" + linksProcessosRelacionados.size() + "] links dentro da tabela de processos relacionados! Abrindo...");
			
			for(WebElement link: linksProcessosRelacionados) {
				String textoLink = link.getText();
				log.debug("Abrindo link [" + textoLink + "]...");
				String hrefLink = link.getAttribute("href");
				log.debug("href = [" + hrefLink + "]");
				
				//abre link em nova aba
				String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL, Keys.RETURN);
				link.sendKeys(selectLinkOpeninNewTab);
				
				//pega nova guia
				Set<String> handlesSet = driver.getWindowHandles();
		        List<String> handlesList = new ArrayList<String>(handlesSet);
		        if(handlesList.size() > 1) {
		        	driver.switchTo().window(handlesList.get(1));
			        log.debug("Mudou para aba " + driver.getTitle() + "!");
		        } else {
		        	log.error("Erro ao encontrar e mudar de abas!");
		        	return null;
		        }
		        
		        String processoAberto = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"txtNumProcesso\"]"))).getText().trim();
		        log.debug("Abriu processo [" + processoAberto + "]");
		        
		        //pegando os eventos
				List<WebElement> eventos = wdw.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"tblEventos\"]/tbody/tr")));
				log.debug("Obteve [" + eventos.size() + "] eventos...");
				
				log.debug("Buscando confirmação de intimação com data [" + atividade.getDataPublicacao() + "]");
		        
				//pega as referencias intermediárias nesse processo relacionado
				ArrayList<String> referenciasIntermediariasPR = buscaReferenciasIntermediarias(atividade, wdw, logCI, eventos);
				
				log.debug("Encontrou as referencias intermediarias " + referenciasIntermediariasPR + " no processo relacionado [" + textoLink + "]...");
				
				if(referenciasIntermediariasPR != null && referenciasIntermediariasPR.size() != 0) {
					return referenciasIntermediariasPR;
				}
				
				log.debug("Falha ao encontrar referências intermediárias nesse processo relacionado! Fechando abas e procurando em outro (se tiver)...");
				fechaAbas(driver);
			}
			
			return null;
		} catch (Exception e) {
			log.error("Erro ao buscar referências intermediárias nos processos relacionados...", e);
			return null;
		}
	}

	private static boolean abrirProcesso(Atividade atividade, WebDriver driver, WebDriverWait wdw, LogCopiaIntimacao logCI) {
		try {
			WebDriverWait wdw1 = new WebDriverWait(driver, 10);
			
			log.debug("Abrindo processo [" + atividade.getNumeroProcesso() + "]");
			
			WebElement campoNroProcesso = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@title,'Pesquisar pelo número do processo')]")));
			campoNroProcesso.sendKeys(atividade.getNumeroProcesso());
			campoNroProcesso.sendKeys(Keys.RETURN);
			
			esperaCarregar(wdw);
			
			try {
				//verifica se abriu processo correto
				String processoAberto = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"txtNumProcesso\"]"))).getText().trim();
				
				if(!processoAberto.equals(atividade.getNumeroProcesso())) {
					log.error("Erro ao abrir processo: Não abriu o processo correto...");
					CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao abrir processo!");
				}
			} catch (Exception e1) {
				log.debug("Não encontrou numero do processo aberto, procurando 'Processo não encontrado'...");
				
				String exc = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"divInfraExcecao\"]"))).getText().trim();
				if(exc.contains("Processo não encontrado")) {
					CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, exc);
					return false;
				}
			}
			
			log.debug("Processo aberto!");
			return true;
		} catch (Exception e) {
			log.error("Erro ao abrir processo [" + atividade.getCodigo() + "]", e);
			CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao abrir processo: " + e.getMessage());
			return false;
		}
	}

	public static boolean loginEPROCSC(WebDriver driver) {
		try {
			log.debug("Abrindo EPROCSC...");
			WebDriverWait wdw = new WebDriverWait(driver, 30);
			
			driver.get(CopiaIntimacaoConstants.URL_EPROCSC1G);
			
			WebElement campoUsuario = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"txtUsuario\"]")));
			WebElement campoSenha = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"pwdSenha\"]")));
			
			campoUsuario.sendKeys(CopiaIntimacaoConstants.LOGIN_EPROCSC1G_JAIME);
			campoSenha.sendKeys(CopiaIntimacaoConstants.SENHA_EPROCSC1G_JAIME);
			log.debug("Preencheu dados de login...");
			
			WebElement btEntrar = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"sbmEntrar\"]")));
			btEntrar.click();
			log.debug("Clicou botão entrar...");
			
			//espera o carregamento
			esperaCarregar(wdw);
			
			//espera aparecer nome usuario
			WebElement labelUsuario = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"divInfraAreaDadosDinamica\"]/div[1]/span[1]")));
			log.debug(labelUsuario.getText().trim());
			
			log.debug("Escolhendo usuário/lotação...");
			
			//pega as opções
			List<WebElement> opcoesUsuario = wdw.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("/html/body/div[1]/div[3]/div/div/div[1]/div[2]/div/form/div/fieldset/div/button")));
			log.debug("Obteve [" + opcoesUsuario.size() + "] opções de usuário...");
			
			boolean selecionouUsuario = false;
			for(WebElement opcao: opcoesUsuario) {
				String textoOpcao = opcao.findElement(By.xpath("./div[1]")).getText().trim();
				log.debug(textoOpcao);
				if(textoOpcao.equals(CopiaIntimacaoConstants.USUARIO_SIGLA)) {
					log.debug("Entrando no usuário [" + CopiaIntimacaoConstants.USUARIO_SIGLA + "]...");
					opcao.click();
					selecionouUsuario = true;
					break;
				}
			}
			
			if(!selecionouUsuario) {
				log.error("Falha ao selecionar usuário no EPROCSC!");
				return false;
			}
			
			//espera aparecer campo de procura de nro de processo
			wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@title,'Pesquisar pelo número do processo')]")));
			log.debug("Entrou EPROCSC...");
			
			return true;
		} catch (Exception e) {
			log.error("Erro a logar no EPROCSC!", e);
			return false;
		}
	}

	private static void esperaCarregar(WebDriverWait wdw) {
		try {
			log.debug("Esperando o carregamento...");
			
			wdw.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@id=\"divInfraAvisoFundo\"]")));
			
			log.debug("Finalizou carregamento...");
		} catch (Exception e) {
		}
		
	}

}
