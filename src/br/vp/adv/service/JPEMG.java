package br.vp.adv.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.vp.adv.bo.AtividadesBO;
import br.vp.adv.bo.CopiaIntimacaoBO;
import br.vp.adv.bo.SeleniumBO;
import br.vp.adv.constants.CopiaIntimacaoConstants;
import br.vp.adv.utils.Utils;
import br.vp.adv.utils.WasabiUtils;
import br.vp.adv.vo.Atividade;
import br.vp.adv.vo.LogCopiaIntimacao;

public class JPEMG {
	
	public static final Logger log = Logger.getLogger(JPEMG.class);
	
	public static void buscaIntimacoes() {
		try {
			log.debug("=======================> INÍCIO CÓPIA INTIMAÇÃO JPEMG");
			
			//pega as publicacoes que tiveram erro do PJE Recursal MG
			ArrayList<Atividade> atividadesAptas = AtividadesBO.buscaPublicacoesErroTribunal("PJERECURSALMG");
			
			if(atividadesAptas == null) {
				return;
			}
			
			log.debug("Obteve [" + atividadesAptas.size() + "] publicações com erro do PJERecursal MG.");
			if(atividadesAptas.size() == 0) {
				return;
			}
			
			log.debug("Realizando filtragem das atividades...");
			
			ArrayList<Atividade> atividades = new ArrayList<Atividade>();
			int contAt = 1;
			int qtdSucesso = 0;
			for(Atividade at: atividadesAptas) {
				if(atividades.size() >= CopiaIntimacaoConstants.LIMITE_POR_EXECUCAO) {
					log.debug("Completou o limite de [" + CopiaIntimacaoConstants.LIMITE_POR_EXECUCAO + "] atividades por execução! Iniciando busca...");
					break;
				}
				
				log.debug("Verificando atividade [" + at.getCodigo() + "] (" + contAt + " de " + atividadesAptas.size() + ")");
				contAt++;
				
				if(!CopiaIntimacaoBO.verificaTentativasFrustadasTribunal(at.getNumeroProcesso(), at.getGrauProcesso(), at.getCodigo(), "JPEMG")) {
					
					//verifica se teve determinada quantidade de falhas no PJEMG e no PJERECURSALMG
					if(CopiaIntimacaoBO.falhasTribunaisAnteriores(at)) {
						//verifica se já tem arquivo anexado na atividade
						if(!CopiaIntimacaoBO.temArquivoAnexadoAtividade(at.getCodigo(), at.getPasta())) {
							log.debug("O processo [" + at.getNumeroProcesso() + "] da atividade ["+at.getCodigo()+"] entrou para a fila!");
							atividades.add(at);
						}
					}
				}
			}
			
			log.debug("[" + atividades.size() + "] atividades na fila...");
			
			if(atividades.size() == 0) {
				return;
			}
			
			WebDriver driver = SeleniumBO.abrirNavegador();
			
			if(driver != null) {
				if(!loginJPE(driver)) {
					driver.quit();
					return;
				}
				
				int cont = 1;
				for(Atividade atividade: atividades) {
					log.debug("======================================================================================================================================");
					log.debug("Buscando atividade [" + atividade.getCodigo() + "] (" + cont + " de " + atividades.size() + ")...");
					log.debug(atividade);
					
					if(buscaProcessoJPEMG(driver, atividade)) {
						qtdSucesso++;
					}
					
					fecharPopup(driver);
					Utils.limparPastaTemp();
					cont++;
					log.debug("======================================================================================================================================");
				}
				
				driver.quit();
			} else {
				log.error("Erro ao abrir navegador!");
			}
			
			log.debug("=======================> FINAL CÓPIA INTIMAÇÃO JPEMG - Sucesso: [" + qtdSucesso + " de " + atividades.size() + "]!");
			
		} catch (Exception e) {
			log.error("Erro ao buscar intimações no PJERecursal!", e);
		}
	}

	private static boolean buscaProcessoJPEMG(WebDriver driver, Atividade atividade) {
		try {
			String numeroProcesso = atividade.getNumeroProcesso();
			int pasta = atividade.getPasta();
			
			LogCopiaIntimacao logCI = new LogCopiaIntimacao();
			logCI.setCodigoAtividade(atividade.getCodigo());
			logCI.setPasta(pasta);
			logCI.setNumeroProcesso(numeroProcesso);
			logCI.setInstancia(atividade.getGrauProcesso());
			logCI.setDataPublicacaoAtividade(atividade.getDataPublicacao());
			logCI.setTribunal("JPEMG");
			
			WebDriverWait wdw = new WebDriverWait(driver, 30);
			WebDriverWait wdw1 = new WebDriverWait(driver, 10);
			
			//verifica se já teve uma publicação com mesma data e mesma pasta
			log.debug("Verificando se já teve uma publicação da pasta [" + atividade.getPasta() + "] da data [" + atividade.getDataPublicacao() + "] com status sucesso...");
			int pubMesmoDocs = CopiaIntimacaoBO.verificaPublicacaoMesmaData(atividade);
			if(pubMesmoDocs > 0) {
				log.debug("Já teve uma atividade de publicação com a data [" + atividade.getDataPublicacao() + "] com sucesso: ["+pubMesmoDocs+"]! Não vai rodar essa atividade!");
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.DOCUMENTOS_ATIVIDADE_MESMO_DIA, logCI, "A atividade ["+pubMesmoDocs+"] é do mesmo dia e tem status sucesso! Não roda para evitar duplicidade de documentos!");
				return false;
			}
			
			log.debug("Abrindo processo " + numeroProcesso);
			
			if(!abreProcessoJPEMG(driver, wdw, wdw1, numeroProcesso)) {
				fecharPopup(driver);
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao abrir processo!");
				return false;
			}
			
			if(!buscaIntimacaoJPEMG(driver, wdw, wdw1, atividade, logCI)) {
				fecharPopup(driver);
				return false;
			}
			
			fecharPopup(driver);
			CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.SUCESSO_BUSCA_DOCUMENTOS, logCI, "Sucesso ao baixar documentos!");
			return true;
		} catch (Exception e) {
			log.error("Falha ao buscar intimações no JPEMG!", e);
			return false;
		}
	}

	private static boolean loginJPE(WebDriver driver) {
		try {
			log.debug("Abrindo JPEMG...");
			WebDriverWait wdw = new WebDriverWait(driver, 30);
			
			driver.get(CopiaIntimacaoConstants.URL_JPE_MG);
			
			WebElement campoUsuario = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"login\"]")));
			WebElement campoSenha = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"senha\"]")));
			
			log.debug("Realizando login...");
			
			SeleniumBO.preencheCampo(campoUsuario, CopiaIntimacaoConstants.LOGIN_JPE_JAIME);
			SeleniumBO.preencheCampo(campoSenha, CopiaIntimacaoConstants.SENHA_JPE_JAIME);
			
			//clica no botão entrar
			wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"entrar\"]"))).click();
			log.debug("Clicou no botão entrar...");
			
			try {
				//espera se aparece popup mensagens
				WebDriverWait wdw1 = new WebDriverWait(driver, 60);
				wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"popupMensagemPlantaoCDiv\"]")));
				log.debug("Encontrou popup 'Aviso aos usuários'! Fechando...");
				
				WebElement btFecharAviso = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"imgFecharPopuppopupMensagemPlantao\"]")));
				btFecharAviso.click();
				log.debug("Clicou botão fechar aviso!");
				
			} catch (Exception e1) {
				log.info("Erro ao encontrar o popup 'Aviso aos usuários'!");
			}
			
			//espera carregar botões do menu
			List<WebElement> botoesMenu = wdw.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"menuSistema\"]/ul/li")));
			log.debug("Obteve [" + botoesMenu.size() + "] botões no menu!");
			
			return true;
		} catch (Exception e) {
			log.error("Erro ao realizar login no PJERecursal!", e);
			return false;
		}
	}
	
	private static boolean abreProcessoJPEMG(WebDriver driver, WebDriverWait wdw, WebDriverWait wdw1, String numeroProcesso) {
		try {
			WebDriverWait wdw2 = new WebDriverWait(driver, 5);
			
			//espera carregar botões do menu
			List<WebElement> botoesMenu = wdw.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"menuSistema\"]/ul/li")));
			log.debug("Obteve [" + botoesMenu.size() + "] botões no menu!");
			
			//procura o botão consulta
			boolean clicouConsulta = false;
			WebElement btConsulta = null;
			for(WebElement btMenu: botoesMenu) {
				log.debug(btMenu.getText());
				if(btMenu.getText().trim().equals("Consulta")) {
					log.debug("Encontrou botão consulta! Clicando...");
					btConsulta = btMenu;
					btMenu.click();
					clicouConsulta = true;
					break;
				}
			}
			
			if(!clicouConsulta || btConsulta == null) {
				log.error("Não encontrou botão 'Consulta'!");
				return false;
			}
			
			//procura todos os links dentro
			List<WebElement> linksConsulta = btConsulta.findElements(By.tagName("a"));
			log.debug("Encontrou [" + linksConsulta.size() + "] links dentro do pai do botão 'Consulta'!");
			
			//procura o botão processo
			boolean clicouProcesso = false;
			WebElement btProcesso = null;
			for(WebElement link: linksConsulta) {
				log.debug(link.getText());
				if(link.getText().trim().equals("Processo")) {
					log.debug("Encontrou botão 'Processo'! Clicando...");
					btProcesso = link;
					link.click();
					clicouProcesso = true;
					break;
				}
			}
			
			if(!clicouProcesso || btProcesso == null) {
				log.error("Não encontrou botão 'Processo'!");
				return false;
			}
			
			//pega o pai do botão processos
			WebElement paiBtProcesso = btProcesso.findElement(By.xpath("./.."));
			log.debug(paiBtProcesso.getTagName());
			
			//procura todos os links dentro
			List<WebElement> linksProcesso = paiBtProcesso.findElements(By.tagName("a"));
			log.debug("Encontrou [" + linksProcesso.size() + "] links dentro do pai do botão 'Processo'!");
			
			//procura o botão Todos
			boolean clicouTodos = false;
			WebElement btTodosProcesso = null;
			for(WebElement link: linksProcesso) {
				log.debug(link.getText());
				if(link.getText().trim().equals("Todos")) {
					log.debug("Encontrou botão 'Todos'! Clicando...");
					btTodosProcesso = link;
					link.click();
					clicouTodos = true;
					break;
				}
			}
			
			if(!clicouTodos || btTodosProcesso == null) {
				log.error("Não encontrou botão 'Todos'!");
				return false;
			}
			
			//espera aparecer campo para procura do processo do lado esquerdo (processo)
			WebElement campoProcessoEsquerda = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"formFiltrosfiltrosProcessos:processo\"]")));
			log.debug("Encontrou campo de busca de processo do lado esquerdo!");
			campoProcessoEsquerda.clear();
			
			//espera aparecer campo para procura do processo do lado direito (origem)
			WebElement campoProcessoDireita = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"formFiltrosfiltrosProcessos:origem\"]")));
			log.debug("Encontrou campo de busca de processo do lado direito!");
			campoProcessoDireita.clear();
			
			WebElement btPesquisar = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"formFiltrosfiltrosProcessos:pesquisar\"]")));
			
			//pesquisa primeiro no processo
			//procura numero do processo
			log.debug("Procurando pelo lado esquerdo...");
			SeleniumBO.preencheCampo(campoProcessoEsquerda, numeroProcesso);
			
			//rola até o final da página
			JavascriptExecutor js = ((JavascriptExecutor) driver);
			js.executeScript("arguments[0].scrollIntoView(false);", driver.findElement(By.xpath("//*[@id=\"formTabelaProcessos:dataTabletabelaProcessos:tb\"]")));
			
			btPesquisar.click();
			
			//verifica se apareceu processo
			List<WebElement> linhasProcesso = null;
			try {
				log.debug("Verifcando se encontrou processo...");
				linhasProcesso = wdw2.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"formTabelaProcessos:dataTabletabelaProcessos:tb\"]/tr")));
			} catch (Exception ebpp) {
				log.debug("Não encontrou processo pelo lado da esquerda! Procurando no lado direito (origem)...");
				
				campoProcessoEsquerda.clear();
				
				SeleniumBO.preencheCampo(campoProcessoDireita, numeroProcesso);
				
				//rola até o final da página
				js.executeScript("arguments[0].scrollIntoView(false);", driver.findElement(By.xpath("//*[@id=\"formTabelaProcessos:dataTabletabelaProcessos:tb\"]")));
				
				btPesquisar.click();
				
				try {
					log.debug("Verifcando se encontrou processo...");
					linhasProcesso = wdw2.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"formTabelaProcessos:dataTabletabelaProcessos:tb\"]/tr")));
				} catch (Exception ebpd) {
					log.error("Erro ao encontrar processo no JPE!");
					return false;
				}
			}
			
			log.debug("Encontrou [" + linhasProcesso.size() + "] processos!");
			
			if(linhasProcesso.size() == 0) {
				return false;
			}
			
			//scroll pro final da página
			((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
			 
			//abre primeiro processo
			//WebElement primeiraLinha = linhasProcesso.get(0);
			
			//abre último processo
			WebElement ultimaLinha = linhasProcesso.get(linhasProcesso.size()-1);
			
			WebElement lupa = null;
			try {
				lupa = ultimaLinha.findElement(By.xpath("./img[@title='Visualizar']"));
			} catch (Exception febvisu) {
				log.warn("Falha ao encontrar botão 'Visualizar' pelo title! Buscando todos os <img> ...");
				
				List<WebElement> imgs = ultimaLinha.findElements(By.tagName("img"));
				log.debug("Encontrou [" + imgs.size() + "] imgs! Procurando com title ou alt...");
				
				for(WebElement img: imgs) {
					String title = img.getAttribute("title");
					String alt = img.getAttribute("alt");
					
					if(title.equals("Visualizar") || alt.equals("Visualizar")) {
						log.debug("Encontrou botão 'Visualizar'!");
						lupa = img;
						break;
					}
				}
			}
			
			if(lupa == null) {
				log.error("Falha ao encotrar botão de visualizar!");
				return false;
			}
			
			lupa.click();
			log.debug("Clicou para abrir processo!");
			
			//espera aparecer popup do processo
			wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"popupDadosProcessoCDiv\"]")));
			log.debug("Abriu popup do processo!");
			
			WebElement headerPopup = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"popupDadosProcessoHeader\"]")));
			String tituloPopup = headerPopup.getText().trim();
			
			String nome = "Dados Processo - " + numeroProcesso;
			log.debug(nome + " - " + tituloPopup);
			
			/*
			if(tituloPopup.equals(nome)) {
				log.debug("Abriu processo correto!");
				return true;
			}
			*/
			
			return true;
		} catch (Exception e) {
			log.error("Erro ao abrir processo [" + numeroProcesso + "] no JPEMG!", e);
			return false;
		}
	}
	
	private static boolean buscaIntimacaoJPEMG(WebDriver driver, WebDriverWait wdw, WebDriverWait wdw1, Atividade atividade, LogCopiaIntimacao logCI) {
		try {
			
			WebElement popupProcesso = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"popupDadosProcessoCDiv\"]")));

			WebElement btPetDocs = popupProcesso.findElement(By.xpath("//*[@id=\"formDadosProcesso:dadosPecas_lbl\"]"));
			log.debug("Encontrou botão 'Petições e Documentos'! Clicando...");
			btPetDocs.click();
			
			//espera carregar linhas dos documentos
			List<WebElement> linhasDocumentos = wdw.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"formDadosProcesso:dataTabletabelaPecasProcessoprocessoPrincipal:tb\"]/tr")));
			log.debug("Obteve [" + linhasDocumentos.size() + "] documentos! Pegando o primeiro!");
			
			if(linhasDocumentos.size() == 0) {
				log.error("Nenhum documento encontrado!");
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha na busca da intimação! Nenhum documento encontrado!");
				return false;
			}
			
			//percorre todas as linhas e pega o primeiro que estiver em vermelho
			for(WebElement linha: linhasDocumentos) {
				log.debug(linha.getText().replace("\n", " / ").trim());
				
				String numeroMovimento = linha.findElement(By.xpath("./td[1]")).getText().trim();
				WebElement spanTipoMovimento = linha.findElement(By.xpath("./td[2]/span"));
				String tipoMovimento = spanTipoMovimento.getText().trim();
				
				String classeSpan = spanTipoMovimento.getAttribute("class");
				if(classeSpan.contains("destaque")) {
					log.debug("Possui destaque, vai baixar!");
					
					SeleniumBO.tirarPrint(driver, atividade, linha);
					
					String refDoc = numeroMovimento + " - " + tipoMovimento;
					log.debug(refDoc);
					
					if(logCI.getReferencia() == null || logCI.getReferencia().equals("")) {
			        	logCI.setReferencia(refDoc);
			        } else {
			        	logCI.setReferencia(logCI.getReferencia() + " // " + refDoc);
			        }
					
					//pega link para documento
					WebElement tdLinkDocumento = linha.findElement(By.xpath("./td[3]"));
					log.debug("Pegou td documento(s)!");
					
					List<WebElement> linksDocumentos = tdLinkDocumento.findElements(By.tagName("a"));
					log.debug("Encontrou [" + linksDocumentos.size() + "] documentos para baixar nessa linha!");
					
					for(WebElement link: linksDocumentos) {
						String nomeLink = link.getText().trim();
						log.debug(nomeLink);
						
						log.debug("Clicando para baixar...");
						link.click();
						
						if(!salvarArquivoJPEMG(driver, wdw1, atividade, nomeLink, logCI)) {
							log.error("Erro ao salvar arquivo(s)!");
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha na busca da intimação! Erro ao salvar arquivo(s)!");
							return false;
						}
					}
					
					return true;
				} else {
					log.debug("Não possui destaque!");
				}
				
				
			}
			/*
			WebElement linhaPrimeiroDocumento = linhasDocumentos.get(0);
			log.debug(linhaPrimeiroDocumento.getText().replace("\n", " / ").trim());
			
			SeleniumBO.tirarPrint(driver, atividade, linhaPrimeiroDocumento);
			
			String numeroMovimento = linhaPrimeiroDocumento.findElement(By.xpath("./td[1]")).getText().trim();
			String tipoMovimento = linhaPrimeiroDocumento.findElement(By.xpath("./td[2]")).getText().trim();
			
			String refDoc = numeroMovimento + " - " + tipoMovimento;
			log.debug(refDoc);
			
			if(logCI.getReferencia() == null || logCI.getReferencia().equals("")) {
	        	logCI.setReferencia(refDoc);
	        } else {
	        	logCI.setReferencia(logCI.getReferencia() + " // " + refDoc);
	        }
			
			//pega link para documento
			WebElement tdLinkDocumento = linhaPrimeiroDocumento.findElement(By.xpath("./td[3]"));
			log.debug("Pegou td documento(s)!");
			
			List<WebElement> linksDocumentos = tdLinkDocumento.findElements(By.tagName("a"));
			log.debug("Encontrou [" + linksDocumentos.size() + "] documentos para baixar nessa linha!");
			
			for(WebElement link: linksDocumentos) {
				String nomeLink = link.getText().trim();
				log.debug(nomeLink);
				
				log.debug("Clicando para baixar...");
				link.click();
				
				if(!salvarArquivoJPEMG(driver, wdw1, atividade, nomeLink, logCI)) {
					log.error("Erro ao salvar arquivo(s)!");
					CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha na busca da intimação! Erro ao salvar arquivo(s)!");
					return false;
				}
			}
			
			return true;
			*/
			
			CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha na busca da intimação! Erro ao encontrar destaque!");
			return false;
		} catch (Exception e) {
			log.error("Erro ao buscar intimação no JPEMG!", e);
			return false;
		}
	}
	
	private static boolean salvarArquivoJPEMG(WebDriver driver, WebDriverWait wdw1, Atividade atividade, String nomeLink, LogCopiaIntimacao logCI) {
		try {
			String numeroProcesso = atividade.getNumeroProcesso();
			int codigoAtividade = atividade.getCodigo();
			
			String nomeArquivo = atividade.getCodigo() + "-arquivo-"+numeroProcesso.replace(".", "-")+".pdf";
			
			String pathFolderCodigoAtividade = CopiaIntimacaoConstants.PATH_DOCUMENTOS+"/"+codigoAtividade;	
			String pathCompletoArquivo = pathFolderCodigoAtividade + "/" + nomeArquivo;
			
			//verificando se esse arquivo já não existe
			File arq = new File(pathCompletoArquivo);
			if(arq.exists()) {
				log.debug("O arquivo [" + nomeArquivo + "] já existe! Mudando nome...");
				int cont = 1;
				while(arq.exists()) {
					nomeArquivo = atividade.getCodigo() + "-arquivo-"+cont+"-"+numeroProcesso.replace(".", "-")+".pdf";
					log.debug("Nome do arquivo: [" + nomeArquivo + "]");
					
					pathCompletoArquivo = pathFolderCodigoAtividade + "/" + nomeArquivo;
					
					arq = new File(pathCompletoArquivo);
					
					cont++;
				}
			}
			log.debug("Nome do arquivo OFICIAL: [" + nomeArquivo + "]");
			
			//verifica se existe um folder pra esse processo
			File folderAtividade = new File(pathFolderCodigoAtividade);
			if(!folderAtividade.exists() || !folderAtividade.isDirectory()) {
				folderAtividade.mkdir();
			}
			
			log.debug("Esperando 15s para baixar...");
			
			Thread.sleep(15000);
			
			log.debug("Baixou arquivo!");
			
			//pega o ultimo arquivo na pasta
			File[] arquivosTemp = new File(CopiaIntimacaoConstants.PATH_DOCUMENTOS_TEMP).listFiles();
			log.debug("Encontrou [" + arquivosTemp.length + "] arquivos na pasta TEMP!");
			File ultimoArquivoMod = null;
			Date ultimoMod = null;
			for(int indArqTempo = 0; indArqTempo < arquivosTemp.length; indArqTempo++) {
				Date dataUltimaMod = new Date(arquivosTemp[indArqTempo].lastModified());
				if(ultimoMod == null || dataUltimaMod.after(ultimoMod)) {
					ultimoArquivoMod = arquivosTemp[indArqTempo];
					ultimoMod = dataUltimaMod;
				}
			}
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			if(ultimoArquivoMod == null || ultimoMod == null) {
				log.error("Erro ao pegar arquivo baixado!");
				//CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao pegar algum arquivo baixado!");
				return false;
			} else {
				String nomeArquivoPastaTemp = ultimoArquivoMod.getName();
				
				log.debug("Último arquivo modificado: ["+nomeArquivoPastaTemp+"] - [" + sdf.format(ultimoMod) + "]");
				log.debug("Tamanho: [" + ultimoArquivoMod.length() + "]");
				
				//verifica se o arquivo é o mesmo
				if(!nomeArquivoPastaTemp.equals(nomeLink.replace(" ", "_"))) {
					log.warn("O nome do arquivo não é o mesmo do link no site!");
					return false;
				} else {
					log.debug("O nome do arquivo é o mesmo do link no site! [" + nomeArquivoPastaTemp + "]! Continuando...");
				}
				
				File arquivoDownload = new File(pathCompletoArquivo);
				
				log.debug("Copiando arquivo para [" + pathCompletoArquivo + "]");
				//copia pra essa pasta
				FileUtils.copyFile(ultimoArquivoMod, arquivoDownload);
				log.debug("Copiou arquivo para [" + pathCompletoArquivo + "] com sucesso!");
				
				//excluir arquivo
				if(ultimoArquivoMod.delete()) {
					log.debug("Sucesso ao excluir arquivo ["+ultimoArquivoMod.getName() + "] da pasta TEMP!");
				} else {
					log.warn("FALHA ao excluir arquivo ["+ultimoArquivoMod.getName() + "] da pasta TEMP!");
				}
				
				//subir no LS
				if(!WasabiUtils.anexarArquivoLS(atividade.getPasta(), atividade.getCodigo(), arquivoDownload.getName(), arquivoDownload, logCI, true)) {
					log.error("Erro ao subir [" + arquivoDownload.getName() + "] para o LS!");
					CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao anexar arquivo no LS!");
					return false;
				}
			}
			
			return true;
		} catch (Exception e) {
			log.error("Erro ao salvar arquivo(s)!", e);
			return false;
		}
	}

	private static void fecharPopup(WebDriver driver) {
		try {
			WebDriverWait wdw1 = new WebDriverWait(driver, 5);
			
			log.debug("Fechando popup aberto (se tiver)...");
			
			wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"popupDadosProcessoCDiv\"]")));
			
			WebElement headerPopup = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"popupDadosProcessoHeader\"]")));
			String tituloPopup = headerPopup.getText().trim();
			
			WebElement btFecharPopup = wdw1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"imgFecharPopuppopupDadosProcesso\"]")));
			btFecharPopup.click();
			log.debug("Clicou para fechar popup [" + tituloPopup + "]");
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

}
