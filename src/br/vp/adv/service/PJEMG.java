package br.vp.adv.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.twocaptcha.TwoCaptcha;
import com.twocaptcha.captcha.ReCaptcha;

import br.vp.adv.bo.AtividadesBO;
import br.vp.adv.bo.CopiaIntimacaoBO;
import br.vp.adv.bo.SeleniumBO;
import br.vp.adv.constants.CopiaIntimacaoConstants;
import br.vp.adv.utils.Utils;
import br.vp.adv.utils.WasabiUtils;
import br.vp.adv.vo.Atividade;
import br.vp.adv.vo.LogCopiaIntimacao;
import br.vp.adv.vo.PartesNroProcessoPJE;

public class PJEMG {

	public static final Logger log = Logger.getLogger(PJEMG.class);
	
	public static void baixarCopiasPJE() {
		try {
			log.debug("=======================> INÍCIO CÓPIA INTIMAÇÃO PJEMG");
			
			//pega as publicacoes em andamento de processos virtuais em MG
			ArrayList<Atividade> atividadesAptas = AtividadesBO.buscaPublicacoes(13, true, true);
			if(atividadesAptas == null) {
				return;
			}
			
			log.debug("Obteve [" + atividadesAptas.size() + "] publicações aptas para baixar intimação em MG.");
			
			if(atividadesAptas.size() == 0) {
				return;
			}
			
			log.debug("Realizando filtragem das atividades...");
			
			ArrayList<Atividade> atividades = new ArrayList<Atividade>();
			int contAt = 1;
			int qtdSucesso = 0;
			for(Atividade at: atividadesAptas) {
				if(atividades.size() >= CopiaIntimacaoConstants.LIMITE_POR_EXECUCAO) {
					log.debug("Completou o limite de [" + CopiaIntimacaoConstants.LIMITE_POR_EXECUCAO + "] atividades por execução! Iniciando busca...");
					break;
				}
				
				log.debug("Verificando atividade [" + at.getCodigo() + "] (" + contAt + " de " + atividadesAptas.size() + ")");
				contAt++;
				
				if(!CopiaIntimacaoBO.verificaTentativasFrustadas(at.getNumeroProcesso(), at.getGrauProcesso(), at.getCodigo())) {
					//verifica se já tem arquivo anexado na atividade
					if(!CopiaIntimacaoBO.temArquivoAnexadoAtividade(at.getCodigo(), at.getPasta())) {
						log.debug("O processo [" + at.getNumeroProcesso() + "] da atividade ["+at.getCodigo()+"] entrou para a fila!");
						atividades.add(at);
					}
					
				}
			}
			
			log.debug("[" + atividades.size() + "] atividades na fila...");
			
			if(atividades.size() == 0) {
				return;
			}
			
			WebDriver driver = SeleniumBO.abrirNavegador();
			
			if(driver != null) {
				if(!loginPJE(driver)) {
					driver.quit();
					return;
				}
				
				int cont = 1;
				for(Atividade atividade: atividades) {
					log.debug("======================================================================================================================================");
					log.debug("Buscando atividade [" + atividade.getCodigo() + "] (" + cont + " de " + atividades.size() + ")...");
					log.debug(atividade);
					
					if(buscaProcessoPJEMG(driver, atividade)) {
						qtdSucesso++;
					}
					
					//fecha abas e volta pra principal do PJE
					fecharAbas(driver);
					Utils.limparPastaTemp();
					cont++;
					log.debug("======================================================================================================================================");
				}
				
				driver.quit();
			} else {
				log.error("Erro ao abrir navegador!");
			}
			
			log.debug("=======================> FINAL CÓPIA INTIMAÇÃO PJEMG - Sucesso: [" + qtdSucesso + " de " + atividades.size() + "]!");
		} catch (Exception e) {
			log.error("Falha na cópia de intimações do PJE!", e);
			e.printStackTrace();
		}
	}
	
	public static boolean buscaProcessoPJEMG(WebDriver driver, Atividade atividade) {
		try {
			String numeroProcesso = atividade.getNumeroProcesso();
			int pasta = atividade.getPasta();
			
			LogCopiaIntimacao logCI = new LogCopiaIntimacao();
			logCI.setCodigoAtividade(atividade.getCodigo());
			logCI.setPasta(pasta);
			logCI.setNumeroProcesso(numeroProcesso);
			logCI.setInstancia(atividade.getGrauProcesso());
			logCI.setDataPublicacaoAtividade(atividade.getDataPublicacao());
			logCI.setTribunal("PJEMG");
			
			WebDriverWait wdw = new WebDriverWait(driver, 30);
			
			//verifica se já teve uma publicação com mesma data e mesma pasta
			log.debug("Verificando se já teve uma publicação da pasta [" + atividade.getPasta() + "] da data [" + atividade.getDataPublicacao() + "] com status sucesso ou múltiplos anexos...");
			int pubMesmoDocs = CopiaIntimacaoBO.verificaPublicacaoMesmaData(atividade);
			if(pubMesmoDocs > 0) {
				log.debug("Já teve uma atividade de publicação com a data [" + atividade.getDataPublicacao() + "] com sucesso: ["+pubMesmoDocs+"]! Não vai rodar essa atividade!");
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.DOCUMENTOS_ATIVIDADE_MESMO_DIA, logCI, "A atividade ["+pubMesmoDocs+"] é do mesmo dia e tem status sucesso! Não roda para evitar duplicidade de documentos!");
				return false;
			}
			
			log.debug("Abrindo processo " + numeroProcesso);
			
			if(!abreProcessoPJEMG(driver, wdw, numeroProcesso)) {
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao abrir processo!");
				return false;
			}
			
			if(!buscaIntimacaoPJEMG(driver, wdw, atividade, logCI)) {
				return false;
			}
			
			CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.SUCESSO_BUSCA_DOCUMENTOS, logCI, "Sucesso ao baixar documentos!");
			return true;
		} catch (Exception e) {
			log.error("Falha ao buscar copia da atividade [" + atividade.getCodigo() + "] no PJEMG!", e);
			return false;
		}
	}
	
	private static boolean abreProcessoPJEMG(WebDriver driver, WebDriverWait wdw, String numeroProcesso) {
		try {
			//if(!driver.getCurrentUrl().equals(CopiaIntimacaoConstants.URL_PJE_BUSCA)) {
				//abre pagina de consulta de processo
				driver.get(CopiaIntimacaoConstants.URL_PJE_BUSCA);
			//}
			
			//separa nro do processo em partes
			PartesNroProcessoPJE partesNroProcessoPJE = Utils.separaNumeroProcessoPJE(numeroProcesso);
			if(partesNroProcessoPJE == null) {
				return false;
			}
			
			log.debug("Preenchendo numero do processo...");
			SeleniumBO.preencheCampo(wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"fPP:numeroProcesso:numeroSequencial\"]"))), partesNroProcessoPJE.getNumeroSequencial());
			SeleniumBO.preencheCampo(wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"fPP:numeroProcesso:numeroDigitoVerificador\"]"))), partesNroProcessoPJE.getNumeroDigitoVerificador());
			SeleniumBO.preencheCampo(wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"fPP:numeroProcesso:Ano\"]"))), partesNroProcessoPJE.getAno());
			//preencheCampo(wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"fPP:numeroProcesso:ramoJustica\"]"))), partesNroProcessoPJE.getRamoJustica());
			//preencheCampo(wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"fPP:numeroProcesso:respectivoTribunal\"]"))), partesNroProcessoPJE.getRespectivoTribunal());
			SeleniumBO.preencheCampo(wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"fPP:numeroProcesso:NumeroOrgaoJustica\"]"))), partesNroProcessoPJE.getNumeroOrgaoJustica());
			
			//clica botao pesquisar
			wdw.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"fPP:searchProcessos\"]"))).click();
			log.debug("Clicou botão pesquisar...");
			
			log.debug("Espera parar de carregar");
			wdw.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@id=\"_viewRoot:status.start\"]")));
			log.debug("Parou de carregar!");
			
			WebDriverWait wdw1 = new WebDriverWait(driver, 10);
			
			//verifica se retornou resultados
			List<WebElement> listaProcessos = null;
			
			try {
				listaProcessos = wdw1.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"fPP:processosTable:tb\"]/tr")));
				log.debug("Encontrou [" + listaProcessos.size() + "] linhas de processos! Abrindo o correto...");
			} catch (Exception eelp) {
				log.debug("Não encontrou linhas de processo! Verificando captcha...");
				
				resolverCaptchaSeNecessario(driver, wdw1);
				
				log.debug("Procurando novamente os processos:");
				listaProcessos = wdw1.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"fPP:processosTable:tb\"]/tr")));
				log.debug("Encontrou [" + listaProcessos.size() + "] linhas de processos! Abrindo o correto...");
			}
			
			
			//clica para entrar no processo
			boolean entrouProcesso = false;
			for(WebElement linha: listaProcessos) {
				//WebElement linkProcesso = linha.findElement(By.tagName("a"));
				WebElement linkProcesso = linha.findElement(By.xpath(".//td[2]/a"));
				if(linkProcesso.getText().trim().equals(numeroProcesso)) {
					log.debug("Encontrou link para o processo [" + linkProcesso.getText().trim() + "]! Abrindo processo...");
					linkProcesso.click();
					entrouProcesso = true;
					break;
				}
			}
			
			if(!entrouProcesso) {
				log.error("Falha ao encontrar processo!");
				return false;
			}
			
			try {
				WebDriverWait wdw2 = new WebDriverWait(driver, 5);
				//verifica se surgiu popup
				Alert alert = wdw2.until(ExpectedConditions.alertIsPresent());
				log.debug("Alerta apareceu: '" + alert.getText() + " Aceitando...");
				alert.accept();
			} catch (Exception e1) {
				log.debug("Alerta não apareceu! TUDO OK!");
			}
			
			log.debug("Espera parar de carregar");
			wdw.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@id=\"_viewRoot:status.start\"]")));
			log.debug("Parou de carregar!");
			
			try {
				WebDriverWait wdw2 = new WebDriverWait(driver, 5);
				//verifica se surgiu popup
				Alert alert = wdw2.until(ExpectedConditions.alertIsPresent());
				log.debug("Alerta apareceu: '" + alert.getText() + " Aceitando...");
				alert.accept();
			} catch (Exception e1) {
				log.debug("Alerta não apareceu! TUDO OK!");
			}
			
			//verifica se abriu o processo em outra aba
			log.debug("Verificando se o processo abriu corretamente...");
			
			//muda para nova aba aberta pelo PJE
			Set<String> handlesSet = driver.getWindowHandles();
	        List<String> handlesList = new ArrayList<String>(handlesSet);
	        if(handlesList.size() > 1) {
	        	driver.switchTo().window(handlesList.get(1));
		        log.debug("Mudou para aba " + driver.getTitle() + "!");
		        
		        //verifica se apareceu botão menu na direita
		        //wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"navbar:ajaxPanelAlerts\"]/ul[2]/li[6]/a")));
		        wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(@class, 'btn-menu-abas dropdown-toggle') and @title='Menu']")));
		        log.debug("Encontrou menu na direita! Processo aberto com sucesso!");
		        
		        return true;
	        } else {
	        	log.error("Erro ao encontrar e mudar de abas!");
	        	return false;
	        }
		} catch (Exception e) {
			log.error("Falha ao abrir processo [" + numeroProcesso + "] no PJEMG!", e);
			return false;
		}
	}
	
	private static boolean buscaIntimacaoPJEMG(WebDriver driver, WebDriverWait wdw, Atividade atividade, LogCopiaIntimacao logCI) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			//clica no botão menu da direita
			//WebElement menuDireita = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"navbar:ajaxPanelAlerts\"]/ul[2]/li[7]/a")));
			WebElement menuDireita = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(@class, 'btn-menu-abas dropdown-toggle') and @title='Menu']")));
			menuDireita.click();
			
			Thread.sleep(1000);

			WebElement btExpedientes = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"navbar:linkAbaExpedientes\"]")));
			btExpedientes.click();
			
			log.debug("Abrindo expedientes...");
			
			log.debug("Espera parar de carregar");
			wdw.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@id=\"_viewRoot:status.start\"]")));
			log.debug("Parou de carregar!");
			
			String dataPublicacaoAtividade = atividade.getDataPublicacao();
			Date datePubAtividade = sdf.parse(atividade.getDataPublicacao());
			
			//procura os atos de comunicação
			List<WebElement> atos = wdw.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[@id=\"processoParteExpedienteMenuGridList:tb\"]/tr")));
			log.debug("Encontrou [" + atos.size() + "] atos de comunicação! Procurando o do dia [" + dataPublicacaoAtividade + "]");
			
			boolean encontrouAto = false;
			boolean subiuAlgumArquivo = false;
			String dataCiente = null;
			int contAtos = 1;
			for(WebElement ato: atos) {
				dataCiente = null;
				
				log.debug(ato.getText().replace("\n", " / ").trim());
				
				WebElement h6 = ato.findElement(By.tagName("h6"));
				String textoH6 = h6.getText().trim();
				log.debug("Ato nº "+contAtos+" - H6: [" + textoH6 + "]");
				contAtos++;
				
				//pega o parent desse h6
				WebElement spanParent = h6.findElement(By.xpath("./.."));
				
				if(spanParent.getTagName().equals("span")) {
					//pega primeira div e verifica se aparece intimação
					WebElement div1 = spanParent.findElement(By.xpath("./div[1]"));
					String textoDiv1 = div1.getText().trim();
					log.debug("div1: [" + textoDiv1 + "]");
					
					if(!textoDiv1.contains("Intimação") && !textoDiv1.contains("Petição") && 
							!textoDiv1.contains("Decisão - Jesp") && !textoDiv1.contains("Sentença") &&
							!textoDiv1.contains("Despacho")) {
						log.debug("Não possui 'intimação' ou 'petição'! Pulando esse ato...");
						continue;
					}
				} else {
					log.error("Erro ao verificar se tem 'intimação' ou 'petição'!");
					CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha na busca da intimação!");
					return false;
				}
				
				log.debug("Buscando data de ciência...");
					
				try {
					WebElement divData = spanParent.findElement(By.xpath("./*[contains (text(), ' registrou ciência em ')]"));
					String textoDiv = divData.getText().trim();
					//log.debug("Encontrou div da data: [" + textoDiv + "] do tipo [" + divData.getTagName() + "]");
					log.debug(textoDiv);
				} catch (Exception e1) {
					log.debug("Não encontrou data de ciência GERAL");
				}
					
				try {
					WebElement divData = spanParent.findElement(By.xpath("./*[contains (text(), 'Você registrou ciência em ')]"));
					String textoDiv = divData.getText().trim();
					log.debug("POSSÍVEL INTIMAÇÃO: Encontrou div da data: [" + textoDiv + "] do tipo [" + divData.getTagName() + "]");
					
					String dataEHora = textoDiv.replace("Você registrou ciência em ", "").trim();
					log.debug("dataEHora: [" + dataEHora + "]");
					String[] dataSeparada = dataEHora.split(" ");
					dataCiente = dataSeparada[0].trim();
					
					log.debug("Data de ciência do ato: [" + dataCiente + "]");
				} catch (Exception e1) {
					log.debug("Não encontrou data de ciência 'Você registrou...'! Procurando por ERICA FERNANDES TONELOTO...");
					
					try {
						WebElement divData = spanParent.findElement(By.xpath("./*[contains (text(), 'ERICA FERNANDES TONELOTO registrou ciência em ')]"));
						log.debug("Encontrou com o nome da ERICA!");
						String textoDiv = divData.getText().trim();
						log.debug("POSSÍVEL INTIMAÇÃO: Encontrou div da data: [" + textoDiv + "] do tipo [" + divData.getTagName() + "]");
						
						String dataEHora = textoDiv.replace("ERICA FERNANDES TONELOTO registrou ciência em ", "").trim();
						log.debug("dataEHora: [" + dataEHora + "]");
						String[] dataSeparada = dataEHora.split(" ");
						dataCiente = dataSeparada[0].trim();
						
						log.debug("Data de ciência do ato: [" + dataCiente + "]");
					} catch (Exception eneft) {
						log.debug("Não encontrou ERICA...");
					}
				}
					
				if(dataCiente != null && !dataCiente.equals("")) {
					//verificando se as datas batem
					log.debug("Verificando a data do ato [" + dataCiente + "] com a data da atividade [" + dataPublicacaoAtividade + "]");
					if(dataCiente.equals(dataPublicacaoAtividade)) {
						log.debug("As datas batem! Baixando arquivos...");
						
						SeleniumBO.tirarPrint(driver, atividade, ato);
						
						WebElement spanLinkDocumentos = null;
						
						boolean encontrouPeloFiltoTitle = false;
						
						try {
							spanLinkDocumentos = ato.findElement(By.xpath("./*[text()='Visualizar ato']"));
						} catch (Exception e1) {
							log.debug("Falha ao encontrar link com 'Visualizar ato'! Procurando maiúsculo...");
							try {
								spanLinkDocumentos = ato.findElement(By.xpath("./*[text()='VISUALIZAR ATO']"));
							} catch (Exception e2) {
								log.debug("Falha ao encontrar link 'VISUALIZAR ATO'! Procurando todos os links e filtrando pelo título...");
								try {
									List<WebElement> linksDoAto = ato.findElements(By.tagName("a"));
									log.debug("Encontrou [" + linksDoAto.size() + "] links dentro desse ato! Procurando pelo título 'Visualizar ato");
									
									for(WebElement link: linksDoAto) {
										if(link.getAttribute("title").equals("Visualizar ato")) {
											spanLinkDocumentos = link;
											encontrouPeloFiltoTitle = true;
										}
									}
									
									//spanLinkDocumentos = ato.findElement(By.linkText("Visualizar ato"));
								} catch (Exception e3) {
									log.error("Falha ao obter link para baixar arquivo!", e3);
									CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao baixar arquivo da intimação!");
									return false;
								}
							}
						}
						
						if(spanLinkDocumentos == null) {
							log.error("Falha ao obter link para baixar arquivo!");
							CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao baixar arquivo da intimação!");
							return false;
						}
						
						//pega link
						//WebElement spanLinkDocumentos = ato.findElement(By.xpath("./*[text()='Visualizar ato']"));
						log.debug("Encontrou botão para ver arquivos: [" + spanLinkDocumentos.getText() + "]");
						
						WebElement linkAbrirDocumentos = null;
						
						//pega o elemento pai <a></a>
						if(!encontrouPeloFiltoTitle) {
							linkAbrirDocumentos = spanLinkDocumentos.findElement(By.xpath("./.."));
							log.debug("Encontrou link: [" + linkAbrirDocumentos.getTagName() + "]");
						} else {
							linkAbrirDocumentos = spanLinkDocumentos;
						}
						
						
						if(linkAbrirDocumentos.getTagName().equals("a")) {
							linkAbrirDocumentos.click();
							log.debug("Clicou para abrir documentos!");
							
							//verifica se documentos abriram em outra aba
							//muda para nova aba aberta pelo PJE
							Set<String> handlesSet = driver.getWindowHandles();
					        List<String> handlesList = new ArrayList<String>(handlesSet);
					        if(handlesList.size() > 2) {
					        	driver.switchTo().window(handlesList.get(2));
						        log.debug("Mudou para aba " + driver.getTitle() + "!");
						        
						        String refDoc = "---";
						        try {
						        	//pega titulo/referência
							        refDoc = driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div[2]/div/div/div/form/div/div/div/div/div/h4/span")).getText().trim();
						        } catch (Exception erd) {
								}
						        
						        if(logCI.getReferencia() == null || logCI.getReferencia().equals("")) {
						        	logCI.setReferencia(refDoc);
						        } else {
						        	logCI.setReferencia(logCI.getReferencia() + " // " + refDoc);
						        }
						        
						        log.debug(refDoc);
						        
						        //verifica se apareceu botão de download
						        WebElement spanLinkDownload = driver.findElement(By.xpath("//*[@id=\"detalhesDocumento:download\"]/span"));
								log.debug("Encontrou botão para baixar arquivo: [" + spanLinkDownload.getText() + "]");
								
								//pega o elemento pai <a></a>
								WebElement linkDownloadDocumentos = spanLinkDownload.findElement(By.xpath("./.."));
								log.debug("Encontrou link: [" + linkDownloadDocumentos.getTagName() + "]");
								
								if(linkDownloadDocumentos.getTagName().equals("a")) {
									log.debug("Baixando documentos... href: [" +linkDownloadDocumentos.getAttribute("href") + "]");
									
									linkDownloadDocumentos.click();
									
									//tratar alerta
									try {
										WebDriverWait wdw1 = new WebDriverWait(driver, 5);
										//verifica se surgiu popup
										Alert alert = wdw1.until(ExpectedConditions.alertIsPresent());
										log.debug("Alerta apareceu: '" + alert.getText() + " Aceitando...");
										alert.accept();
									} catch (Exception e1) {
										log.debug("Alerta não apareceu! TUDO OK!");
									}
									
									if(!salvarArquivoPJE(driver, wdw, atividade, logCI)) {
										if(!subiuAlgumArquivo) {
											CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao salvar arquivos!");
											return false;
										} else {
											if(logCI.getMensagem() != null && !logCI.getMensagem().equals("")) {
												logCI.setMensagem(logCI.getMensagem() + " - Falha ao baixar " + refDoc);
											} else {
												logCI.setMensagem("Falha ao baixar " + refDoc);
											}
											
										}
										
									} else {
										subiuAlgumArquivo = true;
									}
									
									log.debug("Salvou arquivos! Fechando aba atual");
									String handleAbaAtos = handlesList.get(1);
									driver.close();
									
									driver.switchTo().window(handleAbaAtos);
									log.debug("Mudou para aba " + driver.getTitle() + "!");
									log.debug("Procurando mais atos desse dia...");
								}
								
						        //return true;
					        } else {
					        	log.error("Erro ao encontrar e mudar de abas!");
					        	return false;
					        }
							
						}
						
						//marca se encontrou algum ato
						encontrouAto = true;
						//break;
					} else {
						//verifica se a data é anterior a data da atividade
						Date dateAto = sdf.parse(dataCiente);
						if(dateAto.before(datePubAtividade)) {
							log.error("A data do ato [" + dataCiente + "] é anterior a data da atividade [" + dataPublicacaoAtividade + "]! Parando busca...");
							if(!encontrouAto) {
								CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao buscar a intimação! Datas não compatíveis!");
								return false;
							} else {
								return true;
							}
							
						}
					}
				}
			}
			
			/*
			if(dataCiente == null || dataCiente.equals("")) {
				log.error("Falha ao pegar a data do ato!");
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao pegar a data de algum ato de comunicação!");
				return false;
			}*/
			
			if(!encontrouAto) {
				log.error("Falha ao encontrar ato de comunicação para essa atividade!");
				CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao encontrar ato de comunicação!");
				return false;
			}
				
			return true;
		} catch (Exception e) {
			log.error("Erro ao procurar intimação da atividade [" + atividade.getCodigo() + "]", e);
			CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao buscar intimação: " + e.getMessage());
			return false;
		}
	}
	
	private static boolean salvarArquivoPJE(WebDriver driver, WebDriverWait wdw, Atividade atividade, LogCopiaIntimacao logCI) {
		try {
			String numeroProcesso = atividade.getNumeroProcesso();
			int codigoAtividade = atividade.getCodigo();
			
			log.debug("Esperando download...");
			Thread.sleep(45000);
			
			String nomeArquivo = atividade.getCodigo() + "-arquivo-"+numeroProcesso.replace(".", "-")+".pdf";
			
			String pathFolderCodigoAtividade = CopiaIntimacaoConstants.PATH_DOCUMENTOS+"/"+codigoAtividade;	
			String pathCompletoArquivo = pathFolderCodigoAtividade + "/" + nomeArquivo;
			
			File arq = new File(pathCompletoArquivo);
			if(arq.exists()) {
				log.debug("O arquivo [" + nomeArquivo + "] já existe! Mudando nome...");
				int cont = 1;
				while(arq.exists()) {
					nomeArquivo = atividade.getCodigo() + "-arquivo-"+cont+"-"+numeroProcesso.replace(".", "-")+".pdf";
					log.debug("Nome do arquivo: [" + nomeArquivo + "]");
					
					pathCompletoArquivo = pathFolderCodigoAtividade + "/" + nomeArquivo;
					
					arq = new File(pathCompletoArquivo);
					
					cont++;
				}
			}
			log.debug("Nome do arquivo OFICIAL: [" + nomeArquivo + "]");
			
			//verifica se existe um folder pra esse processo
			File folderAtividade = new File(pathFolderCodigoAtividade);
			if(!folderAtividade.exists() || !folderAtividade.isDirectory()) {
				folderAtividade.mkdir();
			}
			
			log.debug("Baixou arquivo!");
			
			//pega o ultimo arquivo na pasta
			File[] arquivosTemp = new File(CopiaIntimacaoConstants.PATH_DOCUMENTOS_TEMP).listFiles();
			log.debug("Encontrou [" + arquivosTemp.length + "] arquivos na pasta TEMP!");
			File ultimoArquivoMod = null;
			Date ultimoMod = null;
			for(int indArqTempo = 0; indArqTempo < arquivosTemp.length; indArqTempo++) {
				Date dataUltimaMod = new Date(arquivosTemp[indArqTempo].lastModified());
				if(ultimoMod == null || dataUltimaMod.after(ultimoMod)) {
					ultimoArquivoMod = arquivosTemp[indArqTempo];
					ultimoMod = dataUltimaMod;
				}
			}
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			if(ultimoArquivoMod == null || ultimoMod == null) {
				log.error("Erro ao pegar arquivo baixado!");
				//CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Erro ao pegar algum arquivo baixado!");
				return false;
			} else {
				String nomeArquivoPastaTemp = ultimoArquivoMod.getName();
				
				log.debug("Último arquivo modificado: ["+nomeArquivoPastaTemp+"] - [" + sdf.format(ultimoMod) + "]");
				log.debug("Tamanho: [" + ultimoArquivoMod.length() + "]");
				
				//verifica se o arquivo começa com o n do processo
				if(!nomeArquivoPastaTemp.contains(numeroProcesso)) {
					log.warn("O nome do arquivo não possui o número do processo!");
					return false;
				} else {
					log.debug("O nome do arquivo contém o número do processo! Continuando...");
				}
				
				File arquivoDownload = new File(pathCompletoArquivo);
				
				log.debug("Copiando arquivo para [" + pathCompletoArquivo + "]");
				//copia pra essa pasta
				FileUtils.copyFile(ultimoArquivoMod, arquivoDownload);
				log.debug("Copiou arquivo para [" + pathCompletoArquivo + "] com sucesso!");
				
				//arquivosDoProcesso.add(arquivoDownload);
				
				//excluir arquivo
				if(ultimoArquivoMod.delete()) {
					log.debug("Sucesso ao excluir arquivo ["+ultimoArquivoMod.getName() + "] da pasta TEMP!");
				} else {
					log.warn("FALHA ao excluir arquivo ["+ultimoArquivoMod.getName() + "] da pasta TEMP!");
				}
				
				//subir no LS
				if(!WasabiUtils.anexarArquivoLS(atividade.getPasta(), atividade.getCodigo(), arquivoDownload.getName(), arquivoDownload, logCI, true)) {
					log.error("Erro ao subir [" + arquivoDownload.getName() + "] para o LS!");
					CopiaIntimacaoBO.insereLogCopiaIntimacao(CopiaIntimacaoConstants.ERRO_BUSCA_DOCUMENTOS, logCI, "Falha ao anexar arquivo no LS!");
					return false;
				}
			}
			
			return true;
		} catch (Exception e) {
			log.error("Erro ao salvar arquivos da atividade [" + atividade.getCodigo() + "]", e);
			return false;
		}
	}
	
	public static void fecharAbas(WebDriver driver) {
		try {
			//pega as janelas(abas)
			Set<String> handlesSet = driver.getWindowHandles();
	        List<String> handlesList = new ArrayList<String>(handlesSet);
	        
	        if(handlesList.size() > 1) {
	        	for(int i = (handlesList.size() - 1); i >= 1; i--) {
	        		String handle = handlesList.get(i);
	        		driver.switchTo().window(handle);
	        		log.debug("Fechando aba de handle " + handle + " de título " + driver.getTitle());
	        		driver.close();
	        	}
	        }
	        
	        driver.switchTo().window(handlesList.get(0));
	        log.debug("Mudou para aba " + driver.getTitle());
		} catch (Exception e) {
			log.error("Erro ao fechar as abas e retornar para a principal do PJE!", e);
		}
	}
	
	public static boolean loginPJE(WebDriver driver) {
		try {
			log.debug("Abrindo PJEMG...");
			WebDriverWait wdw = new WebDriverWait(driver, 30);
			
			driver.get(CopiaIntimacaoConstants.URL_PJE);
			
			if(driver.getPageSource().contains("Sistema indisponível no momento.")) {
				log.error("Sistema indisponível!");
				return false;
			}
			
			boolean mudouFrame = false;
			try {
				log.debug("Tentando mudar para frmae 'ssoFrame'...");
				wdw.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("ssoFrame")));
				log.debug("Trocou para iFrame ssoFrame!");
				mudouFrame = true;
			} catch (Exception e) {
				log.debug("Não mudou para frame 'ssoFrame'!");
			}
			
			WebElement campoCPFUsuario = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"username\"]")));
			WebElement campoSenhaUsuario = wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"password\"]")));
			
			log.debug("Realizando login...");
			
			SeleniumBO.preencheCampo(campoCPFUsuario, CopiaIntimacaoConstants.LOGIN_PJE_JAIME);
			SeleniumBO.preencheCampo(campoSenhaUsuario, CopiaIntimacaoConstants.SENHA_PJE_JAIME);
			
			//clica no botão entrar
			if(mudouFrame) {
				wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"kc-login\"]"))).click();
			} else {
				wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"btnEntrar\"]"))).click();
			}
			
//			if(!resolverCaptchaSeNecessario(driver, new WebDriverWait(driver, 10))) {
//				return false;
//			}
			
			driver.switchTo().defaultContent();
			log.debug("Voltou para default content!");
			
			Utils.sleepComLog(5);
			log.debug("Dando Ctrl+F5...");
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("location.reload(true);");
			log.debug("Deu Ctrl+F5!");
			Utils.sleepComLog(10);
			
			try {
				//espera alguns segundos pra ver se o menu 'Abrir menu' aparece
				WebDriverWait wdw1 = new WebDriverWait(driver, 60);
				//wdw1.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/nav/div/div[1]/ul/li/a")));
				wdw1.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"barraSuperiorPrincipal\"]/div/div[1]/ul/li/a")));
				log.debug("Encontrou botão 'Abrir menu'! Login realizado com sucesso!");
			} catch (Exception e1) {
				log.error("Erro ao encontrar o 'Abrir menu'!");
				return false;
			}
			return true;
		} catch (Exception e) {
			log.error("Erro ao realizar login no PJE!", e);
			return false;
		}
	}

	private static boolean resolverCaptchaSeNecessario(WebDriver driver, WebDriverWait wdw) {
		//verifica se aparece capthca
		try {
			log.debug("Procurando frame do captcha...");
			WebElement iframeCaptcha = wdw.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//iframe[contains(@src,'https://www.google.com/recaptcha/api2')]")));
			log.debug("Encontrou frame do captcha...");
			
			Thread.sleep(5000);
			
			//verifica se popup do captcha apareceu
			log.debug("Procurando popup do captcha...");
			//WebElement popupCaptcha = wdw.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[6]")));
			WebElement paiIframe = iframeCaptcha.findElement(By.xpath("./.."));
			WebElement divVoIframe = paiIframe.findElement(By.xpath("./.."));
			
			if(!divVoIframe.isDisplayed()) {
				log.debug("Popup do captcha NÃO encontrado!");
				return true;
			}
			
			log.debug("Popup do captcha encontrado!");
			
			TwoCaptcha solver = new TwoCaptcha(CopiaIntimacaoConstants.TWOCAPTCHA_API_KEY);
			
			ReCaptcha captcha = new ReCaptcha();
			
			WebElement divCaptcha = wdw.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@data-sitekey]")));
			
			String dataSiteKey = divCaptcha.getAttribute("data-sitekey");
			String url = driver.getCurrentUrl();
			
			log.debug("sitekey = [" + dataSiteKey + "]");
			log.debug("url = [" + url + "]");
			
			captcha.setSiteKey(dataSiteKey);
			captcha.setUrl(url);
			captcha.setInvisible(true);
			captcha.setAction("verify");
			
			try {
				log.debug("Aguardando resolução do captcha...");
				solver.solve(captcha);
				
				log.info("Captcha resolvido! Code: [" + captcha.getCode() + "]");
				
				((JavascriptExecutor)driver).executeScript("document.getElementById('g-recaptcha-response').innerHTML = '" + captcha.getCode() + "';");
				log.debug("Inseriu code no g-recaptcha-response!");
				Thread.sleep(5000);
				 
				//calback
				((JavascriptExecutor)driver).executeScript("onSubmit();");
				log.debug("Fez callback!");
				
				return true;
			} catch (Exception e1) {
				log.error("Erro ao resolver captcha!", e1);
				return false;
			}
		} catch (Exception e) {
			log.error("Captcha não encontrado!");
		}
		return true;
	}
	
	
}
