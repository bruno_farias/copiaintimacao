package br.vp.adv.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.vp.adv.bo.CopiaIntimacaoBO;

public class LimpezaTabelaJob implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		CopiaIntimacaoBO.limparTabela();
	}

}
