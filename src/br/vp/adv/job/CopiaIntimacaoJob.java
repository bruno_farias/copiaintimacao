package br.vp.adv.job;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.vp.adv.bo.CopiaIntimacaoBO;
import br.vp.adv.service.EPROCSC1G;
import br.vp.adv.service.JPEMG;
import br.vp.adv.service.PJEMG;
import br.vp.adv.service.PJERecursal;
import br.vp.adv.utils.Utils;

@DisallowConcurrentExecution
public class CopiaIntimacaoJob implements Job {
	
	public static void main(String[] args) {
		todasExecucoes();
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		todasExecucoes();
	}
	
	private static void todasExecucoes() {
		//CopiaIntimacaoBO.baixarCopiasPROJUDI(); //desativado temporariamente para tratamento 
		PJEMG.baixarCopiasPJE();
		EPROCSC1G.buscarIntimacoes();
		PJERecursal.buscaIntimacoes();
		JPEMG.buscaIntimacoes();
		Utils.limpezaArquivos();
	}
}
