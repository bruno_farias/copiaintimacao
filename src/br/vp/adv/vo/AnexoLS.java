package br.vp.adv.vo;

public class AnexoLS {
	private int codigoAtividade;
	private String nomeAnexo;
	private int pasta;
	private int tipAnx;
	private int codUsuIncluir;
	private String url;
	private int idAnx;
	
	public AnexoLS() {
		
	}

	public AnexoLS(int codigoAtividade, String nomeAnexo, int pasta, int tipAnx, int codUsuIncluir, String url,
			int idAnx) {
		super();
		this.codigoAtividade = codigoAtividade;
		this.nomeAnexo = nomeAnexo;
		this.pasta = pasta;
		this.tipAnx = tipAnx;
		this.codUsuIncluir = codUsuIncluir;
		this.url = url;
		this.idAnx = idAnx;
	}

	public int getCodigoAtividade() {
		return codigoAtividade;
	}

	public void setCodigoAtividade(int codigoAtividade) {
		this.codigoAtividade = codigoAtividade;
	}

	public String getNomeAnexo() {
		return nomeAnexo;
	}

	public void setNomeAnexo(String nomeAnexo) {
		this.nomeAnexo = nomeAnexo;
	}

	public int getPasta() {
		return pasta;
	}

	public void setPasta(int pasta) {
		this.pasta = pasta;
	}

	public int getTipAnx() {
		return tipAnx;
	}

	public void setTipAnx(int tipAnx) {
		this.tipAnx = tipAnx;
	}

	public int getCodUsuIncluir() {
		return codUsuIncluir;
	}

	public void setCodUsuIncluir(int codUsuIncluir) {
		this.codUsuIncluir = codUsuIncluir;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getIdAnx() {
		return idAnx;
	}

	public void setIdAnx(int idAnx) {
		this.idAnx = idAnx;
	}

	@Override
	public String toString() {
		return "AnexoLS [codigoAtividade=" + codigoAtividade + ", nomeAnexo=" + nomeAnexo + ", pasta=" + pasta
				+ ", tipAnx=" + tipAnx + ", codUsuIncluir=" + codUsuIncluir + ", url=" + url + ", idAnx=" + idAnx + "]";
	}
}
