package br.vp.adv.vo;

public class PartesNroProcessoPJE {
	private String numeroSequencial;
	private String numeroDigitoVerificador;
	private String ano;
	private String ramoJustica;
	private String respectivoTribunal;
	private String numeroOrgaoJustica;
	
	public String getNumeroSequencial() {
		return numeroSequencial;
	}
	public void setNumeroSequencial(String numeroSequencial) {
		this.numeroSequencial = numeroSequencial;
	}
	public String getNumeroDigitoVerificador() {
		return numeroDigitoVerificador;
	}
	public void setNumeroDigitoVerificador(String numeroDigitoVerificador) {
		this.numeroDigitoVerificador = numeroDigitoVerificador;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public String getRamoJustica() {
		return ramoJustica;
	}
	public void setRamoJustica(String ramoJustica) {
		this.ramoJustica = ramoJustica;
	}
	public String getRespectivoTribunal() {
		return respectivoTribunal;
	}
	public void setRespectivoTribunal(String respectivoTribunal) {
		this.respectivoTribunal = respectivoTribunal;
	}
	public String getNumeroOrgaoJustica() {
		return numeroOrgaoJustica;
	}
	public void setNumeroOrgaoJustica(String numeroOrgaoJustica) {
		this.numeroOrgaoJustica = numeroOrgaoJustica;
	}
	@Override
	public String toString() {
		return "PartesNroProcessoPJE [numeroSequencial=" + numeroSequencial + ", numeroDigitoVerificador="
				+ numeroDigitoVerificador + ", ano=" + ano + ", ramoJustica=" + ramoJustica + ", respectivoTribunal="
				+ respectivoTribunal + ", numeroOrgaoJustica=" + numeroOrgaoJustica + "]";
	}
}
