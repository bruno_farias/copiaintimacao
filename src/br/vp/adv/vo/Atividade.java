package br.vp.adv.vo;

public class Atividade {
	private int codigo;
	private int tipo;
	private int pasta;
	private String numeroProcesso;
	private String grauProcesso;
	private String dataPublicacao;
	private String clientePasta;
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public int getPasta() {
		return pasta;
	}
	public void setPasta(int pasta) {
		this.pasta = pasta;
	}
	public String getNumeroProcesso() {
		return numeroProcesso;
	}
	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}
	public String getGrauProcesso() {
		return grauProcesso;
	}
	public void setGrauProcesso(String grauProcesso) {
		this.grauProcesso = grauProcesso;
	}
	public String getDataPublicacao() {
		return dataPublicacao;
	}
	public void setDataPublicacao(String dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
	}
	public String getClientePasta() {
		return clientePasta;
	}
	public void setClientePasta(String clientePasta) {
		this.clientePasta = clientePasta;
	}
	@Override
	public String toString() {
		return "Atividade [codigo=" + codigo + ", tipo=" + tipo + ", pasta=" + pasta + ", numeroProcesso="
				+ numeroProcesso + ", grauProcesso=" + grauProcesso + ", dataPublicacao=" + dataPublicacao
				+ ", clientePasta=" + clientePasta + "]";
	}
}
