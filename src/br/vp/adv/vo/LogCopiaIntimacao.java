package br.vp.adv.vo;

public class LogCopiaIntimacao {
	private int codigoAtividade;
	private int pasta;
	private String data;
	private String numeroProcesso;
	private String mensagem;
	private String instancia;
	private int status;
	private String url;
	private String dataPublicacaoAtividade;
	private String referencia;
	private String tribunal;
	private String anexosLS;
	boolean multiplosAnexos;
	
	public int getCodigoAtividade() {
		return codigoAtividade;
	}
	public void setCodigoAtividade(int codigoAtividade) {
		this.codigoAtividade = codigoAtividade;
	}
	public int getPasta() {
		return pasta;
	}
	public void setPasta(int pasta) {
		this.pasta = pasta;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getNumeroProcesso() {
		return numeroProcesso;
	}
	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public String getInstancia() {
		return instancia;
	}
	public void setInstancia(String instancia) {
		this.instancia = instancia;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDataPublicacaoAtividade() {
		return dataPublicacaoAtividade;
	}
	public void setDataPublicacaoAtividade(String dataPublicacaoAtividade) {
		this.dataPublicacaoAtividade = dataPublicacaoAtividade;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getTribunal() {
		return tribunal;
	}
	public void setTribunal(String tribunal) {
		this.tribunal = tribunal;
	}
	public String getAnexosLS() {
		return anexosLS;
	}
	public void setAnexosLS(String anexosLS) {
		this.anexosLS = anexosLS;
	}
	public boolean isMultiplosAnexos() {
		return multiplosAnexos;
	}
	public void setMultiplosAnexos(boolean multiplosAnexos) {
		this.multiplosAnexos = multiplosAnexos;
	}
	@Override
	public String toString() {
		return "LogCopiaIntimacao [codigoAtividade=" + codigoAtividade + ", pasta=" + pasta + ", data=" + data
				+ ", numeroProcesso=" + numeroProcesso + ", mensagem=" + mensagem + ", instancia=" + instancia
				+ ", status=" + status + ", url=" + url + ", dataPublicacaoAtividade=" + dataPublicacaoAtividade
				+ ", referencia=" + referencia + ", tribunal=" + tribunal + ", anexosLS=" + anexosLS
				+ ", multiplosAnexos=" + multiplosAnexos + "]";
	}
}
